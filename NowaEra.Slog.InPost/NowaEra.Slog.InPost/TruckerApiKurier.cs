﻿using System;
using System.Collections.Generic;
using System.Linq;



namespace NowaEra.Slog.InPost
{
    public static class TruckerApiKurier
    {
        public static string RemoveAccent(string txt)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }

        public static bool Create(IEnumerable<dynamic> paczki, dynamic senderAddress, string login, string pass, string url)
        {
            try
            {
                var paczka = paczki.FirstOrDefault();
                if (paczka == null)
                    return false;

                var binding = new System.ServiceModel.BasicHttpBinding();
                binding.MaxReceivedMessageSize = 2147483647;

                var endpoint = new System.ServiceModel.EndpointAddress(url);

                InPostService.AuthToken token = new InPostService.AuthToken() { UserName = login, Password = pass };

                List<InPostService.AdditionalService> d_AdditionalServices = new List<InPostService.AdditionalService>();
                //d_AdditionalServices.Add(new InPostService.AdditionalService() { Code = "SMS" });
                //d_AdditionalServices.Add(new InPostService.AdditionalService() { Code = "ROD" });

                InPostService.COD cod = null;

                var _InsuranceAmount = 0;

                if (paczka.UslugiDodatkowe != null)
                {
                    foreach (string u in paczka.UslugiDodatkowe)
                    {
                        switch (u)
                        {
                            case "COD":
                                if (paczka.WartoscZwrotuPobrania > 0)
                                {
                                    cod = new InPostService.COD();
                                    cod.Amount = (decimal)paczka.WartoscZwrotuPobrania;
                                }
                                break;
                            case "UBEZP":
                                _InsuranceAmount = 0;//?
                                break;
                            default:
                                break;
                        }
                    }
                }

                List<InPostService.Parcel> d_parcels = new List<InPostService.Parcel>();

                foreach (var p in paczki)
                {
                    d_parcels.Add(new InPostService.Parcel
                    {
                        D = ((p.Dlugosc ?? 0) == 0) ? 0 : (decimal)p.Dlugosc,
                        S = ((p.Szerokosc ?? 0) == 0) ? 0 : (decimal)p.Szerokosc,
                        W = ((p.Wysokosc ?? 0) == 0) ? 0 : (decimal)p.Wysokosc,
                        Type = InPostService.ParcelType.Package,
                        Weight = ((p.Waga ?? 0) == 0) ? 0 : (decimal)p.Waga
                    });
                }

                var locationFrom = new InPostService.Location()
                {
                    Address = senderAddress.NadawcaUlica + " " + senderAddress.NadawcaNrBudynku + " / " + senderAddress.NadawcaNrLokalu,
                    City = senderAddress.NadawcaMiejscowosc,
                    Contact = "801 88 10 10",
                    CountryCode = "PL",
                    Email = "cok@nowaera.pl",
                    Name = senderAddress.NadawcaNazwa,
                    Person = senderAddress.NadawcaNazwa,
                    PostCode = senderAddress.NadawcaKodPocztowy
                };

                string OdbAdres = paczka.OdbiorcaUlica;
                
                if( string.IsNullOrEmpty(paczka.OdbiorcaOsoba))
                {
                    paczka.OdbiorcaOsoba = "-";
                }

                var locationTo = new InPostService.Location()
                {
                    Name = RemoveAccent(paczka.OdbiorcaNazwa),
                    Address = RemoveAccent(OdbAdres),
                    City = RemoveAccent(paczka.OdbiorcaMiejscowosc),
                    PostCode = paczka.OdbiorcaKodPocztowy,
                    CountryCode = "PL",
                    IsPrivatePerson = true,
                    //PointId = "0", 
                    Contact = paczka.OdbiorcaTelefon,
                    Person = RemoveAccent(paczka.OdbiorcaOsoba),
                    Email = paczka.OdbiorcaEmail
                };

                string referencja = string.Format("{0}/{1}", paczka.NrRef, paczka.NrPaczki);

                var requestCreate = new InPostService.ShipmentRequest()
                {
                    InsuranceAmount = _InsuranceAmount,
                    COD = cod,
                    AdditionalServices = d_AdditionalServices.ToArray(),
                    Parcels = d_parcels.ToArray(),
                    ShipFrom = locationFrom,
                    ShipTo = locationTo,
                    ContentDescription = "Ksiazki. Nr ref: " + referencja,                   
                    LabelFormat = InPostService.LabelImageFormat.ZPL,
                    MPK = "4000",
                    ServiceId = 38,                    
                    ReferenceNumber = paczka.NrRef
                };

                InPostService.ServiceObjectSoapClient client = new InPostService.ServiceObjectSoapClient(binding, endpoint);
                client.Open();

                var response = client.CreateShipment(token, requestCreate);


                if (response.PackageNo == null)
                {
                    foreach (var p in paczki)
                    {
                        p.UstawOpisBledu("[Spedytor] " + "[" + response.responseCode + "] " + response.responseDescription, 2);
                    }
                    client.Close();
                    return false;
                }

                int number = 0;
                foreach (var p in paczki)
                {
                    p.NrListu = response.ParcelData[number].ParcelID;
                    string result = System.Text.Encoding.UTF8.GetString(response.ParcelData[number].MimeData);
                    result = result.Replace("FT648,952^A0N,28,28^FH", "FT648,952^A0N,21,21^FH");
                    p.EtykietaInPost = result;
                    p.IDPrzesyłkiInpost = response.PackageNo;
                    p.AktualizujDaneSpedycyjneInpost();
                    number += 1;
                }
                client.Close();
            }
            catch (Exception e)
            {
                foreach (var p in paczki)
                {
                    p.UstawOpisBledu("[Exception] " + e.Message, 5);
                }
                return false;
            }


            return true;
        }
    }
}
