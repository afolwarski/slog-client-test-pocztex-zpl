﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NowaEra.Slog.InPost
{
    public struct body
    {
        public receiver receiver;
        public List<parcel> parcels;
        public custom_attributes custom_attributes;
    }

    public struct custom_attributes
    {
        public string shipment_type { get; set; }
    }

    public struct receiver
    {
        public string company_name { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public address address { get; set; }
    }

    public struct address
    {
        public string line1 { get; set; }
        public string line2 { get; set; }
        public string city { get; set; }
        public string post_code { get; set; }
        public string country_code { get; set; }
    }

    public struct parcel
    {
        public dimensions dimensions { get; set; }
        public weight weight { get; set; }
    }

    public struct dimensions
    {
        public string length { get; set; }
        public string width { get; set; }
        public string height { get; set; }
        public string unit { get; set; }
    }

    public struct weight
    {
        public string amount { get; set; }
        public string unit { get; set; }
    }
}
