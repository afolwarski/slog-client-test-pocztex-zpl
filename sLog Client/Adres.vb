﻿Public Class Adres
    Public Property Ulica As String = ""
    Public Property NrBudynku As String = ","
    Public Property NrLokalu As String = ""

    Private Const Ulica_MaxLen As Integer = 22
    Private Const NrBudynku_MaxLen As Integer = 7
    Private Const NrLokalu_MaxLen As Integer = 7

    Public Sub New(adres As String)
        If Not PodzielAdresLadnie(adres) AndAlso Not PodzielAdresBrzydko(adres) Then
            Throw New Exception("Nie udało się podzielić adresu.")
        End If
    End Sub

    Private Function PodzielAdresLadnie(adres As String) As Boolean
        Try
            Ulica = PobierzSubAdres(adres, Ulica_MaxLen)

            If Not String.IsNullOrEmpty(adres) Then
                NrBudynku = PobierzSubAdres(adres, NrBudynku_MaxLen)
            End If

            If Not String.IsNullOrEmpty(adres) Then
                If adres.Length <= NrLokalu_MaxLen Then
                    NrLokalu = adres
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
            Return False
        End Try

        Return True
    End Function

    Private Function PobierzSubAdres(ByRef adres As String, ByRef maxLen As Integer) As String
        Dim adresParts As String() = adres.Split(New String() {" "}, StringSplitOptions.RemoveEmptyEntries)
        Dim i As Integer = adresParts.Length + 1
        Dim subAdres As String

        Do
            i = i - 1
            subAdres = String.Join(" ", adresParts.Take(i))
        Loop Until i <= 0 OrElse subAdres.Length <= maxLen

        If i <= 0 Then Throw New Exception()

        If i > 0 Then
            adres = String.Join(" ", adresParts.Where(Function(part, index) index >= i))
        Else
            adres = Nothing
        End If

        Return subAdres
    End Function

    Private Function PodzielAdresBrzydko(adres As String) As Boolean
        Try

        
        If adres.Length <= Ulica_MaxLen Then
            Ulica = adres
        Else
            Ulica = adres.Substring(0, Ulica_MaxLen)
            adres = adres.Substring(Ulica_MaxLen)

            If adres.Length <= NrBudynku_MaxLen Then
                NrBudynku = adres
            Else
                NrBudynku = adres.Substring(0, NrBudynku_MaxLen)
                adres = adres.Substring(NrBudynku_MaxLen)

                If adres.Length <= NrLokalu_MaxLen Then
                    NrLokalu = adres
                Else
                    Return False
                End If
            End If
        End If
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
End Class
