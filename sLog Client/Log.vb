﻿Imports System.Data.SqlClient

Public Class Log
    Public Shared Sub WriteError(msg As String)
        'EventLog.WriteEntry("sLog Client", msg, EventLogEntryType.Error)
    End Sub

    Public Shared Sub WriteInformation(msg As String)
        'EventLog.WriteEntry("sLog Client", msg, EventLogEntryType.Information)
    End Sub

    Public Shared Sub WriteToDB(paczka As PaczkaExtended, status As Integer, msg As String)
        Try
            Using conn As New SqlConnection(My.Settings.SLOGConnectionString)
                conn.Open()

                Using cmd As New SqlCommand()
                    cmd.Connection = conn
                    cmd.CommandText = "INSERT INTO log (data, paczkaId, status, nrRef, nrListu, nrPaczki, blad) VALUES (@data, @paczkaId, @status, @nrRef, @nrListu, @nrPaczki, @blad)"
                    cmd.Parameters.Add("@data", SqlDbType.DateTime).Value = DateTime.Now
                    cmd.Parameters.Add("@status", SqlDbType.Int).Value = status
                    cmd.Parameters.Add("@blad", SqlDbType.VarChar).Value = msg

                    If Not paczka Is Nothing Then
                        cmd.Parameters.Add("@paczkaId", SqlDbType.Int).Value = paczka.Id

                        Dim param As SqlParameter

                        param = cmd.Parameters.Add("@nrRef", SqlDbType.VarChar)
                        If Not String.IsNullOrEmpty(paczka.NrRef) Then param.Value = paczka.NrRef Else param.Value = DBNull.Value

                        param = cmd.Parameters.Add("@nrListu", SqlDbType.VarChar)
                        If Not String.IsNullOrEmpty(paczka.NrListu) Then param.Value = paczka.NrListu Else param.Value = DBNull.Value

                        param = cmd.Parameters.Add("@nrPaczki", SqlDbType.Int)
                        If Not String.IsNullOrEmpty(paczka.NrPaczki) Then param.Value = Integer.Parse(paczka.NrPaczki) Else param.Value = DBNull.Value
                    Else
                        cmd.Parameters.Add("@paczkaId", SqlDbType.Int).Value = DBNull.Value
                        cmd.Parameters.Add("@nrRef", SqlDbType.VarChar).Value = DBNull.Value
                        cmd.Parameters.Add("@nrListu", SqlDbType.VarChar).Value = DBNull.Value
                        cmd.Parameters.Add("@nrPaczki", SqlDbType.Int).Value = DBNull.Value
                    End If

                    cmd.ExecuteNonQuery()
                End Using
            End Using
        Catch ex As Exception
            WriteError(ex.ToString())
        End Try
    End Sub
End Class
