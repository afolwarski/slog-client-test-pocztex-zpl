﻿Imports System.Data.SqlClient

Public Class Paczka
    Public Property Id As Integer
    Public Property EtykietaInPost As String = ""
    Public Property IDPrzesyłkiInpost As String = ""
    Public Property IDPrzesyłkiString As String = ""
    Public Property NrRef As String = Nothing
    Public Property NrPaczki As String = Nothing
    Public Property NrPaczkiWiodacej As String = Nothing
    Public Property NrListu As String = Nothing
    Public Property Etykieta As Etykieta = Nothing
    Public Property Spedytor As String = ""
    
    Public Property OdbNazwa As String = ""
    Public Property OdbOsoba As String = ""
    Public Property OdbMiasto As String = ""
    Public Property OdbAdres As String = ""
    Public Property OdbKodPocztowy As String = ""
    Public Property DataNadania As DateTime? = Nothing

    Public Property EtykietaNiestandardowa As String = ""

    Public Sub PobierzEtykieteZBazy()
        Using connection As New SqlConnection(My.Settings.SLOGConnectionString)
            connection.Open()

            Using cmd As New SqlCommand()
                cmd.Connection = connection
                cmd.CommandText = "select Etykieta from paczki where id = " & Id

                Using reader As SqlDataReader = cmd.ExecuteReader()
                    If reader.Read() Then
                        If Not IsDBNull(reader("Etykieta")) Then Etykieta = Etykieta.FromStrictContent(CType(reader("Etykieta"), String))
                    End If
                End Using
            End Using
        End Using
    End Sub

    Public Sub OznaczJakoAnulowane()
        SQL_UpdateQuery(String.Format("UPDATE paczki SET ZadanieAnulowania = 0, Anulowane = 1, Status = 3, DataAnulowania = '{0}', PIStatus = NULL, PIData = NULL, OpisBledu = NULL WHERE ID = {1}",
                                      DateTime.Today.ToString("yyyy-MM-dd"), Id))
    End Sub

    Public Sub UstawOpisBledu(msg As String, status As Integer)
        If status = 2 Then
            SQL_UpdateQuery(String.Format("UPDATE paczki SET OpisBledu = '{0}', Status = {1}, PIStatus = NULL, PIData = NULL WHERE ID = {2}", msg, status, Id))
        Else
            SQL_UpdateQuery(String.Format("UPDATE paczki SET OpisBledu = '{0}', Status = {1} WHERE ID = {2}", msg, status, Id))
        End If

        Log.WriteToDB(Me, status, msg)
    End Sub

    Public Sub ZapiszEtykieteWBazie()
        SQL_UpdateQuery(String.Format("UPDATE paczki SET Etykieta = '{0}' WHERE ID = {1}", Etykieta.ToString().Replace("'", "''"), Id))
    End Sub

    Protected Function SQL_UpdateQuery(query As String) As Boolean
        Try
            Using connection As New SqlConnection(My.Settings.SLOGConnectionString)
                connection.Open()

                Using cmd As New SqlCommand()
                    cmd.Connection = connection
                    cmd.CommandText = query
                    cmd.ExecuteNonQuery()
                End Using
            End Using
        Catch ex As Exception
            Log.WriteError(ex.ToString())
            Log.WriteToDB(Me, 5, ex.ToString())
            Return False
        End Try

        Return True
    End Function
    Public Sub AktualizujDaneSpedycyjneInpost()
        DataNadania = Now()
        SQL_UpdateQuery(String.Format("UPDATE paczki SET DataNadaniaReal = '{0}', NrListu = '{1}', OpisBledu = NULL, Etykieta = '{2}',IDPrzesyłki = '{4}' WHERE ID = {3}",
                                      DataNadania.Value.ToString("yyyy-MM-dd"), NrListu, EtykietaInPost.ToString().Replace("'", "''"), Id, IDPrzesyłkiInpost))
    End Sub
    Public Sub ZatrzymajPrzetwazanie(msg As String)

        SQL_UpdateQuery(String.Format("UPDATE paczki SET Zrealizowane = 1, OpisBledu = '{0}' WHERE ID = {1}", msg, Id))

    End Sub

    Public Function Check_PP_Log_exist() As Boolean

        Try
            Using connection As New SqlConnection(My.Settings.SLOGConnectionString)

                connection.Open()

                Using cmd As New SqlCommand()
                    cmd.Connection = connection

                    cmd.CommandText = "SELECT * from PP_log WHERE Date = '" + Date.Now.ToShortDateString + "'"

                    Using reader As SqlDataReader = cmd.ExecuteReader()
                        If reader.HasRows Then
                            Return True
                        End If
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Log.WriteError(ex.ToString())
            Log.WriteToDB(Nothing, 5, ex.ToString())
        End Try

        Return False

    End Function

    Public Sub Insert_PP_log(_CreateDateTime As String, _Shipment_guid As String, _Envelope_sent As Integer, _Envelope_cleared As Integer, _Date As String)

        SQL_UpdateQuery(String.Format("INSERT INTO PP_log (CreateDateTime,Shipment_guid, Envelope_cleared, Envelope_sent, Date ) VALUES ('{0}', '{1}', {2}, {3}, '{4}')", _CreateDateTime, _Shipment_guid, _Envelope_cleared, _Envelope_sent, _Date))

    End Sub


    Public Sub AktualizujDaneSpedycyjneEtykietaNiestandardowa()
        SQL_UpdateQuery(String.Format("UPDATE paczki SET DataNadaniaReal = '{0}', NrListu = '{1}', OpisBledu = NULL, Etykieta = '{2}',IDPrzesyłki = '{4}' WHERE ID = {3}",
                                      DataNadania.Value.ToString("yyyy-MM-dd"), NrListu, EtykietaNiestandardowa.ToString().Replace("'", "''"), Id, IDPrzesyłkiString))
    End Sub

End Class
