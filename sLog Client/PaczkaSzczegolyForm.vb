﻿Imports System.Data.SqlClient

Public Class PaczkaSzczegolyForm
    Private id As Integer
    Private inicjalizacja As Boolean = True
    Private dataNadaniaReal As DateTime = DateTime.MaxValue

    Public Sub New(id As Integer)
        InitializeComponent()
        DialogResult = Windows.Forms.DialogResult.None

        Me.id = id
        ZaladujSzczegoly()

        inicjalizacja = False

        If (Not String.IsNullOrEmpty(slogStatusTextBox.Text) AndAlso slogStatusTextBox.Text = "3") OrElse dataNadaniaReal.Date < DateTime.Today Then
            MakeReadOnly(False)
        ElseIf Not String.IsNullOrEmpty(slogStatusTextBox.Text) AndAlso slogStatusTextBox.Text = "10" Then
            MakeReadOnly(True)
        End If
    End Sub

    Private Sub ZaladujSzczegoly()
        Using connection As New SqlConnection(My.Settings.SLOGConnectionString)
            connection.Open()

            Using cmd As New SqlCommand()
                cmd.Connection = connection
                cmd.CommandText = "SELECT * FROM paczki WHERE id = " & id

                Using reader As SqlDataReader = cmd.ExecuteReader()
                    If reader.Read() Then
                        slogIdTextBox.Text = GetString(reader("ID"))
                        slogZrealizowaneTextBox.Text = GetString(reader("Zrealizowane"))
                        slogStatusTextBox.Text = GetString(reader("Status"))
                        slogSpedytorTextBox.Text = GetString(reader("Spedytor"))
                        slogStanowiskoTextBox.Text = GetString(reader("Stanowisko"))
                        slogDataNadaniaWgSAPTextBox.Text = GetString(reader("DataNadaniaWgSAP"))
                        slogDataUtworzeniaRekorduTextBox.Text = GetString(reader("DataUtworzeniaRekordu"))
                        slogDataNadaniaRealnieTextBox.Text = GetString(reader("DataNadaniaReal"))

                        If Not IsDBNull(reader("DataNadaniaReal")) Then dataNadaniaReal = CType(reader("DataNadaniaReal"), DateTime)

                        slogTypUslugiTextBox.Text = GetString(reader("TypUslugi"))
                        slogKodyUslugDodatkowychTextBox1.Text = GetString(reader("KodyUslugDod"))
                        slogPobranieTextBox.Text = GetString(reader("Pobranie"))
                        slogKwotaPobraniaTextBox.Text = GetString(reader("KwotaPobrania"))
                        slogRodzajPaczkiTextBox.Text = GetString(reader("RodzajPaczki"))
                        slogRodzajPaletyTextBox.Text = GetString(reader("RodzajPalety"))
                        slogPaletaDlugoscTextBox.Text = GetString(reader("PaletaDl"))
                        slogPaletaSzerokoscTextBox.Text = GetString(reader("PaletaSzer"))
                        slogPaletaWysokoscTextBox.Text = GetString(reader("PaletaWys"))
                        slogPaczkaNiestandardowaTextBox.Text = GetString(reader("PaczkaNiestandardowa"))
                        slogWagaTextBox.Text = GetString(reader("Waga"))
                        slogGabarytTextBox.Text = GetString(reader("Gabaryt"))
                        slogNrListuTextBox.Text = GetString(reader("NrListu"))
                        slogNrReferencyjnyTextBox.Text = GetString(reader("NrRef"))
                        slogNrPaczkiTextBox.Text = GetString(reader("NrPaczki"))
                        slogNrPaczkiWiodacejTextBox.Text = GetString(reader("NrPaczkiWiodacej"))
                        slogOdbiorcaNazwaTextBox.Text = GetString(reader("OdbNazwa"))
                        slogOdbiorcaOsobaTextBox.Text = GetString(reader("OdbOsoba"))
                        slogOdbiorcaAdresTextBox.Text = GetString(reader("OdbAdres"))
                        slogOdbiorcaMiastoTextBox.Text = GetString(reader("OdbMiasto"))
                        slogOdbiorcaKodPocztowyTextBox.Text = GetString(reader("OdbKodPocztowy"))
                        slogOdbiorcaTelefonTextBox.Text = GetString(reader("OdbTelefon"))
                        slogOdbiorcaMailTextBox.Text = GetString(reader("OdbMail"))

                        If Not reader("ZadanieAnulowania") Is Nothing AndAlso Not IsDBNull(reader("ZadanieAnulowania")) Then slogZadanieAnulowaniaCheckBox.Checked = CType(reader("ZadanieAnulowania"), Boolean)

                        slogAnulowaneTextBox.Text = GetString(reader("Anulowane"))
                        slogDataAnulowaniaTextBox.Text = GetString(reader("DataAnulowania"))
                        slogPlatnoscZaPrzesylkeTextBox.Text = GetString(reader("PlatnoscZaPrzesylke"))
                        slogNrKlientaPlacTextBox.Text = GetString(reader("NrKlientaPlac"))
                        slogPIStatusTextBox.Text = GetString(reader("PIStatus"))
                        slogPIDataTextBox.Text = GetString(reader("PIData"))
                        slogOpisBleduTextBox.Text = GetString(reader("OpisBledu"))
                        slogPotwierdzenieWyjsciaZMagTextBox.Text = GetString(reader("PotwierdzenieWyjsciaZMag"))
                        slogPotwierdzenieOsobaTextBox.Text = GetString(reader("PotwierdzenieOsoba"))
                        slogPotwierdzenieDataTextBox.Text = GetString(reader("PotwierdzenieData"))
                        slogModyfikacjaOsobaTextBox.Text = GetString(reader("ModyfikacjaOsoba"))
                        slogModyfikacjaDataTextBox.Text = GetString(reader("ModyfikacjaData"))
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Function GetString(obj As Object) As String
        If Not obj Is Nothing AndAlso Not IsDBNull(obj) Then
            Return obj.ToString()
        Else
            Return ""
        End If
    End Function

    Private Sub slogZamknijButton_Click(sender As System.Object, e As System.EventArgs) Handles slogZamknijButton.Click
        Me.Close()
    End Sub

    Private Function WalidujDane() As Boolean
        Dim result As Boolean = True

        Dim polaPusteNazwy As String() = SprawdzNiepustoscPol()
        Dim polaNieprawidloweFormaty As String() = SprawdzFormatDanych()

        If polaPusteNazwy.Length > 0 OrElse polaNieprawidloweFormaty.Length > 0 Then
            Dim separator As String = vbCrLf & "    - "
            Dim msg As String = ""

            If polaPusteNazwy.Length > 0 Then
                msg = "Proszę o wypełnienie następujących pól:" & separator & String.Join(separator, polaPusteNazwy)
            End If

            If polaNieprawidloweFormaty.Length > 0 Then
                If polaPusteNazwy.Length > 0 Then
                    msg = msg & vbCrLf & vbCrLf & "Proszę również poprawić następujące błędy formatu danych: "
                Else
                    msg = "Proszę poprawić następujące błędy formatu danych: "
                End If

                msg = msg & separator & String.Join(separator, polaNieprawidloweFormaty)
            End If

            MessageBox.Show(msg, "Nieprawidłowe dane", MessageBoxButtons.OK, MessageBoxIcon.Warning)

            result = False
        End If

        Return result
    End Function

    Private Function SprawdzNiepustoscPol() As String()
        Dim polaPusteNazwy As New List(Of String)

        If String.IsNullOrEmpty(slogWagaTextBox.Text) Then polaPusteNazwy.Add("Waga")
        If String.IsNullOrEmpty(slogOdbiorcaNazwaTextBox.Text) Then polaPusteNazwy.Add("Odbiorca - Nazwa")
        If String.IsNullOrEmpty(slogOdbiorcaOsobaTextBox.Text) Then polaPusteNazwy.Add("Odbiorca - Osoba")
        If String.IsNullOrEmpty(slogOdbiorcaAdresTextBox.Text) Then polaPusteNazwy.Add("Odbiorca - Adres")
        If String.IsNullOrEmpty(slogOdbiorcaMiastoTextBox.Text) Then polaPusteNazwy.Add("Odbiorca - Miasto")
        If String.IsNullOrEmpty(slogOdbiorcaKodPocztowyTextBox.Text) Then polaPusteNazwy.Add("Odbiorca - Kod pocztowy")

        Return polaPusteNazwy.ToArray()
    End Function

    Private Function SprawdzFormatDanych() As String()
        Dim nieprawidloweFormatyMsgs As New List(Of String)
        Dim liczba As Double

        If Not String.IsNullOrEmpty(slogWagaTextBox.Text) AndAlso Not Double.TryParse(slogWagaTextBox.Text, liczba) Then nieprawidloweFormatyMsgs.Add("Nieprawidłowy format liczby w polu 'Waga'.")
        If Not String.IsNullOrEmpty(slogOdbiorcaMiastoTextBox.Text) AndAlso slogWagaTextBox.Text.Length > 17 Then nieprawidloweFormatyMsgs.Add("Zbyt długa nazwa miasta w danych adresowych (limit 17 znaków).")

        If Not String.IsNullOrEmpty(slogOdbiorcaAdresTextBox.Text) Then
            Try
                Dim adres As New Adres(slogOdbiorcaAdresTextBox.Text)
            Catch ex As Exception
                nieprawidloweFormatyMsgs.Add("Nie udało się wyodrębnić składowych adresu odbiorcy. Prawdopodobnie za dużo znaków. Limit: ulica - 22, nr domu - 7, nr mieszk. - 7, lub łącznie: 36.")
            End Try
        End If

        If Not String.IsNullOrEmpty(slogOdbiorcaTelefonTextBox.Text) Then
            Dim nrTel As String = slogOdbiorcaTelefonTextBox.Text.Replace(" ", "").Replace("-", "").Replace("+", "").Replace("(", "").Replace(")", "").Replace("\", "").Replace("/", "")
            Dim telefonRegex As New System.Text.RegularExpressions.Regex("^[0-9]*$")

            If Not telefonRegex.IsMatch(nrTel) Then
                nieprawidloweFormatyMsgs.Add("Nieprawidłowe znaki w polu 'Odbiorca - Telefon'.")
            End If
        End If

        Dim regex As New System.Text.RegularExpressions.Regex("^[0-9]{2}-[0-9]{3}$")
        If Not String.IsNullOrEmpty(slogOdbiorcaKodPocztowyTextBox.Text) AndAlso Not regex.IsMatch(slogOdbiorcaKodPocztowyTextBox.Text) Then nieprawidloweFormatyMsgs.Add("Niepoprawny format kodu pocztowego.")

        Return nieprawidloweFormatyMsgs.ToArray()
    End Function

    Private Function ZapiszDoBazy() As Boolean
        Try
            Using connection As New SqlConnection(My.Settings.SLOGConnectionString)
                connection.Open()

                Using cmd As New SqlCommand()
                    cmd.Connection = connection
                    cmd.CommandText = "UPDATE paczki SET Waga = @waga, OdbNazwa = @odbNazwa, OdbOsoba = @odbOsoba, OdbAdres = @odbAdres, OdbMiasto = @odbMiasto, OdbKodPocztowy = @odbKodPocztowy, OdbTelefon = @odbTelefon, OdbMail = @odbMail, ZadanieAnulowania = @zadanieAnulowania WHERE id = @id"


                    cmd.Parameters.Add("@waga", SqlDbType.Float).Value = GetDBDouble(slogWagaTextBox.Text)
                    cmd.Parameters.Add("@odbNazwa", SqlDbType.VarChar).Value = GetDBText(slogOdbiorcaNazwaTextBox.Text)
                    cmd.Parameters.Add("@odbOsoba", SqlDbType.VarChar).Value = GetDBText(slogOdbiorcaOsobaTextBox.Text)
                    cmd.Parameters.Add("@odbAdres", SqlDbType.VarChar).Value = GetDBText(slogOdbiorcaAdresTextBox.Text)
                    cmd.Parameters.Add("@odbMiasto", SqlDbType.VarChar).Value = GetDBText(slogOdbiorcaMiastoTextBox.Text)
                    cmd.Parameters.Add("@odbKodPocztowy", SqlDbType.VarChar).Value = GetDBText(slogOdbiorcaKodPocztowyTextBox.Text)
                    cmd.Parameters.Add("@odbTelefon", SqlDbType.VarChar).Value = GetDBText(slogOdbiorcaTelefonTextBox.Text)
                    cmd.Parameters.Add("@odbMail", SqlDbType.VarChar).Value = GetDBText(slogOdbiorcaMailTextBox.Text)

                    If slogZadanieAnulowaniaCheckBox.Checked AndAlso slogStatusTextBox.Text = "10" Then
                        cmd.Parameters.Add("@zadanieAnulowania", SqlDbType.Bit).Value = True
                    Else
                        cmd.Parameters.Add("@zadanieAnulowania", SqlDbType.Bit).Value = DBNull.Value
                    End If

                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = id

                    cmd.ExecuteNonQuery()

                    If slogZadanieAnulowaniaCheckBox.Checked AndAlso slogStatusTextBox.Text <> "10" Then
                        cmd.CommandText = String.Format("UPDATE paczki SET ZadanieAnulowania = 0, Anulowane = 1, Status = 3, DataAnulowania = '{0}', PIStatus = NULL, PIData = NULL, OpisBledu = NULL WHERE ID = {1}",
                                      DateTime.Today.ToString("yyyy-MM-dd"), id)
                        cmd.ExecuteNonQuery()
                    End If
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show("Wystąpił błąd:" & vbCrLf & vbCrLf & ex.ToString(), "sLog Client - Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try

        Return True
    End Function

    Private Function GetDBText(obj As Object) As Object
        If Not obj Is Nothing AndAlso Not String.IsNullOrEmpty(obj.ToString()) Then
            Return obj.ToString()
        Else
            Return DBNull.Value
        End If
    End Function

    Private Function GetDBDouble(obj As Object) As Object
        If Not obj Is Nothing AndAlso Not String.IsNullOrEmpty(obj.ToString()) Then
            Return Double.Parse(obj.ToString())
        Else
            Return DBNull.Value
        End If
    End Function

    Private Sub ZapiszButton_Click(sender As System.Object, e As System.EventArgs) Handles ZapiszButton.Click
        If WalidujDane() AndAlso ZapiszDoBazy() Then
            DialogResult = Windows.Forms.DialogResult.OK
            Close()
        End If
    End Sub

    Private Sub slogWagaTextBox_TextChanged(sender As System.Object, e As System.EventArgs) Handles slogWagaTextBox.TextChanged, slogOdbiorcaTelefonTextBox.TextChanged, slogOdbiorcaOsobaTextBox.TextChanged, slogOdbiorcaNazwaTextBox.TextChanged, slogOdbiorcaMiastoTextBox.TextChanged, slogOdbiorcaMailTextBox.TextChanged, slogOdbiorcaKodPocztowyTextBox.TextChanged, slogOdbiorcaAdresTextBox.TextChanged
        If Not inicjalizacja Then ZapiszButton.Enabled = True
    End Sub

    Private Sub slogZadanieAnulowaniaCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles slogZadanieAnulowaniaCheckBox.CheckedChanged
        If Not inicjalizacja Then
            ZapiszButton.Enabled = True
        End If
    End Sub

    Private Sub MakeReadOnly(allowCancel As Boolean)
        slogWagaTextBox.ReadOnly = True
        slogOdbiorcaNazwaTextBox.ReadOnly = True
        slogOdbiorcaOsobaTextBox.ReadOnly = True
        slogOdbiorcaAdresTextBox.ReadOnly = True
        slogOdbiorcaMiastoTextBox.ReadOnly = True
        slogOdbiorcaKodPocztowyTextBox.ReadOnly = True
        slogOdbiorcaTelefonTextBox.ReadOnly = True
        slogOdbiorcaMailTextBox.ReadOnly = True

        If Not allowCancel Then slogZadanieAnulowaniaCheckBox.Enabled = False
    End Sub
End Class