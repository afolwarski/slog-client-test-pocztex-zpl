﻿Imports System.Data.SqlClient
Imports sLog_Client.dhl24
Imports System.Net.Sockets
Imports System.IO
Imports System.Text
Imports System.Threading
Imports Microsoft.Win32.SafeHandles

Public Class DrukowanieEtykiet
    Public Shared Sub Wykonaj(stanowisko As String, ParamArray paczki As Paczka())
        Dim wydruk As New DrukowanieEtykiet()

        wydruk.stanowisko = stanowisko
        wydruk.paczki = paczki

        wydruk.PobierzKonfiguracje()
        wydruk.PobierzEtykiety()
        wydruk.WydrukujEtykiety()
        'wydruk.WydrukujEtykietyTest()
    End Sub

    Private Sub PobierzKonfiguracje()
        Konfiguracja.Odswiez(stanowisko)

        login = Konfiguracja.DHL_AutoryzacjaLogin
        haslo = Konfiguracja.DHL_AutoryzacjaHaslo
        opoznienie_milisek = Konfiguracja.DrukowanieOpoznienie_milisek
        drukarkaIP = Konfiguracja.DrukarkaIP
        drukarkaPort = Konfiguracja.DrukarkaPort
    End Sub

    Private stanowisko As String = ""
    Private paczki As Paczka() = Nothing

    Private login As String = ""
    Private haslo As String = ""
    Private opoznienie_milisek As Integer = 0

    Private drukarkaIP As String = ""
    Private drukarkaPort As String = ""

    Private Sub PobierzEtykiety()
        Using dhlService As New DHL24WebapiPortClient()
            Dim auth As New AuthData()
            auth.username = login
            auth.password = haslo

            For Each paczka As Paczka In paczki
                paczka.PobierzEtykieteZBazy()

                If paczka.Etykieta Is Nothing Then
                    Spedytor.Instancja(paczka).PobierzEtykiete(paczka)

                    If Not paczka.Etykieta Is Nothing Or Not paczka.EtykietaInPost Is Nothing Or Not paczka.EtykietaNiestandardowa Is Nothing Then
                        paczka.ZapiszEtykieteWBazie()
                    End If
                End If
            Next
        End Using
    End Sub

    Private Sub WydrukujEtykiety()
        Using socket As New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP)
            socket.Connect(drukarkaIP, drukarkaPort)

            Using s As Stream = New NetworkStream(socket, FileAccess.Write)
                Using sw As New StreamWriter(s, Encoding.ASCII)
                    For Each paczka As Paczka In paczki
                        sw.WriteLine(paczka.Etykieta)
                        sw.Flush()
                    Next
                End Using
            End Using
        End Using
    End Sub

    Private Sub WydrukujEtykietyTest()
        For Each paczka As Paczka In paczki
            WydrukujEtykieteNaDrukarceUdostepnionej(paczka)
        Next
    End Sub

    Private Shared Sub WydrukujEtykieteNaDrukarceUdostepnionej(paczka As Paczka)
        Using printer As SafeFileHandle = OpenSharedPrinter()
            Using s As Stream = New FileStream(printer, FileAccess.Write)
                Using sw As New StreamWriter(s, Encoding.ASCII)
                    sw.WriteLine(paczka.Etykieta)
                    sw.Flush()
                End Using
            End Using
        End Using
    End Sub

    Declare Auto Function CreateFile Lib "kernel32.dll" (ByVal lpFileName As String, _
        ByVal dwDesiredAccess As FileAccess, ByVal dwShareMode As UInteger, _
        ByVal lpSecurityAttributes As IntPtr, ByVal dwCreationDisposition As FileMode, _
        ByVal dwFlagsAndAttributes As Integer, ByVal hTemplateFile As IntPtr) As SafeFileHandle

    Private Shared Function OpenSharedPrinter() As SafeFileHandle
        Dim printer As SafeFileHandle = Nothing
        Dim i As Integer

        Const maxAttempts As Integer = 1

        For i = 1 To maxAttempts
            printer = CreateFile("\\neko13-0076\DrukarkaTermiczna", FileAccess.Write, 0, IntPtr.Zero, FileMode.Create, 0, IntPtr.Zero)

            If printer.IsInvalid Then
                printer.Dispose()

                If i < maxAttempts Then
                    Log.WriteError("Nie udało się połączyć z drukarką (próba " & i & ").")
                    Thread.Sleep(5000)
                End If
            Else
                Exit For
            End If
        Next

        If printer.IsInvalid Then Throw New Exception("Nie udało się połączyć z drukarką.")

        Return printer
    End Function
End Class
