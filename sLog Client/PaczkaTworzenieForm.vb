﻿Imports System.Data.SqlClient

Public Class PaczkaTworzenieForm
    Dim loading As Boolean = True

    Private Sub PaczkaTworzenieForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        DialogResult = Windows.Forms.DialogResult.None

        PobierzSlowniki()
        PodepnijSlowniki()
        slogDataNadaniaDateTimePicker.Value = DateTime.Today

        loading = False
        UstawPola()
    End Sub

    Public Sub UstawPola()
        If loading Then Exit Sub

        If slogPobranieCheckBox.Checked Then slogKwotaPobraniaTextBox.ReadOnly = False Else slogKwotaPobraniaTextBox.ReadOnly = True

        If CType(slogRodzajPaczkiComboBox.SelectedValue, String) = "PALLET" Then
            slogRodzajPaletyComboBox.Enabled = True
            slogPaletaDlugoscTextBox.ReadOnly = False
            slogPaletaSzerokoscTextBox.ReadOnly = False
            slogPaletaWysokoscTextBox.ReadOnly = False
        Else
            slogRodzajPaletyComboBox.Enabled = False
            slogPaletaDlugoscTextBox.ReadOnly = True
            slogPaletaSzerokoscTextBox.ReadOnly = True
            slogPaletaWysokoscTextBox.ReadOnly = True
        End If

        If CType(slogRodzajPaczkiComboBox.SelectedValue, String) = "PACKAGE" Then
            slogPaczkaNiestandardowaCheckBox.Enabled = True
        Else
            slogPaczkaNiestandardowaCheckBox.Enabled = False
        End If
    End Sub

    Private slowniki As New Dictionary(Of String, List(Of ElementSlownika))

    Private Sub PobierzSlowniki()
        Try
            Using connection As New SqlConnection(My.Settings.SLOGConnectionString)
                connection.Open()

                Using cmd As New SqlCommand()
                    cmd.Connection = connection
                    cmd.CommandText = "SELECT * FROM slowniki"

                    Using reader As SqlDataReader = cmd.ExecuteReader()
                        While reader.Read()
                            Dim nazwa As String = reader("Nazwa").ToString()
                            Dim tytul As String = reader("Tytul").ToString()
                            Dim kod As String = reader("Kod").ToString()

                            Dim slownik As List(Of ElementSlownika) = Nothing
                            If slowniki.ContainsKey(nazwa) Then
                                slownik = slowniki(nazwa)
                            Else
                                slownik = New List(Of ElementSlownika)
                                'slownik.Add(New ElementSlownika("", "NULL"))
                                slowniki.Add(nazwa, slownik)
                            End If

                            If slownik.Find(Function(elem) elem.Tytul = tytul) Is Nothing Then slownik.Add(New ElementSlownika(tytul, kod))
                        End While
                    End Using
                End Using
            End Using
        Catch ex As Exception
        End Try
    End Sub

    Private Sub PodepnijSlowniki()
        PodepnijPojedynczySlownik(slogSpedytorComboBox, "Spedytor")
        PodepnijPojedynczySlownik(slogTypUslugiComboBox, "TypUslugi")
        PodepnijPojedynczySlownik(slogKodyUslugDodCheckedListBox, "KodyUslugDod")
        PodepnijPojedynczySlownik(slogRodzajPaczkiComboBox, "RodzajPaczki")
        PodepnijPojedynczySlownik(slogRodzajPaletyComboBox, "RodzajPalety")
        PodepnijPojedynczySlownik(slogPlatnoscZaPrzesylkeComboBox, "PlatnoscZaPrzesylke")
    End Sub

    Private Sub PodepnijPojedynczySlownik(combo As ListControl, nazwaSlownika As String)
        If slowniki.ContainsKey(nazwaSlownika) Then
            combo.DataSource = slowniki(nazwaSlownika)
            combo.DisplayMember = "Tytul"
            combo.ValueMember = "Kod"
        End If
    End Sub

    Private Sub slogZapiszButton_Click(sender As System.Object, e As System.EventArgs) Handles slogZapiszButton.Click
        If WalidujDane() AndAlso ZapiszDoBazy() Then
            DialogResult = Windows.Forms.DialogResult.OK
            Close()
        End If
    End Sub

    Private Function WalidujDane() As Boolean
        Dim result As Boolean = True

        Dim polaPusteNazwy As String() = SprawdzNiepustoscPol()
        Dim polaNieprawidloweFormaty As String() = SprawdzFormatDanych()

        If polaPusteNazwy.Length > 0 OrElse polaNieprawidloweFormaty.Length > 0 Then
            Dim separator As String = vbCrLf & "    - "
            Dim msg As String = ""

            If polaPusteNazwy.Length > 0 Then
                msg = "Proszę o wypełnienie następujących pól:" & separator & String.Join(separator, polaPusteNazwy)
            End If

            If polaNieprawidloweFormaty.Length > 0 Then
                If polaPusteNazwy.Length > 0 Then
                    msg = msg & vbCrLf & vbCrLf & "Proszę również poprawić następujące błędy formatu danych: "
                Else
                    msg = "Proszę poprawić następujące błędy formatu danych: "
                End If

                msg = msg & separator & String.Join(separator, polaNieprawidloweFormaty)
            End If

            MessageBox.Show(msg, "Nieprawidłowe dane", MessageBoxButtons.OK, MessageBoxIcon.Warning)

            result = False
        End If

        Return result
    End Function

    Private Function SprawdzNiepustoscPol() As String()
        Dim polaPusteNazwy As New List(Of String)

        If CzyPustePoleCombo(slogSpedytorComboBox) Then polaPusteNazwy.Add("Spedytor")
        If CzyPustePoleCombo(slogTypUslugiComboBox) Then polaPusteNazwy.Add("Typ usługi")
        If slogKodyUslugDodCheckedListBox.CheckedItems.Count = 0 Then polaPusteNazwy.Add("Kody usług dodatkowych")
        If slogPobranieCheckBox.Checked AndAlso String.IsNullOrEmpty(slogKwotaPobraniaTextBox.Text) Then polaPusteNazwy.Add("Kwota pobrania")
        If String.IsNullOrEmpty(slogNrReferencyjnyTextBox.Text) Then polaPusteNazwy.Add("Nr referencyjny")

        If CzyPustePoleCombo(slogRodzajPaczkiComboBox) Then
            polaPusteNazwy.Add("Rodzaj paczki")
        ElseIf slogRodzajPaczkiComboBox.SelectedValue.ToString() = "PALLET" Then
            If CzyPustePoleCombo(slogRodzajPaletyComboBox) Then polaPusteNazwy.Add("Rodzaj palety")
            If String.IsNullOrEmpty(slogPaletaDlugoscTextBox.Text) Then polaPusteNazwy.Add("Paleta - Długość")
            If String.IsNullOrEmpty(slogPaletaSzerokoscTextBox.Text) Then polaPusteNazwy.Add("Paleta - Szerokość")
            If String.IsNullOrEmpty(slogPaletaWysokoscTextBox.Text) Then polaPusteNazwy.Add("Paleta - Wysokość")
        End If

        If String.IsNullOrEmpty(slogWagaTextBox.Text) Then polaPusteNazwy.Add("Waga")
        'If String.IsNullOrEmpty(slogGabarytTextBox.Text) Then polaPusteNazwy.Add("Gabaryt")
        If String.IsNullOrEmpty(slogNrPaczkiTextBox.Text) Then polaPusteNazwy.Add("Nr paczki")
        If String.IsNullOrEmpty(slogOdbiorcaNazwaTextBox.Text) Then polaPusteNazwy.Add("Odbiorca - Nazwa")
        'If String.IsNullOrEmpty(slogOdbiorcaOsobaTextBox.Text) Then polaPusteNazwy.Add("Odbiorca - Osoba")
        If String.IsNullOrEmpty(slogOdbiorcaAdresTextBox.Text) Then polaPusteNazwy.Add("Odbiorca - Adres")
        If String.IsNullOrEmpty(slogOdbiorcaMiastoTextBox.Text) Then polaPusteNazwy.Add("Odbiorca - Miasto")
        If String.IsNullOrEmpty(slogOdbiorcaKodPocztowyTextBox.Text) Then polaPusteNazwy.Add("Odbiorca - Kod pocztowy")
        If CzyPustePoleCombo(slogPlatnoscZaPrzesylkeComboBox) Then polaPusteNazwy.Add("Płatność za przesyłkę")

        Return polaPusteNazwy.ToArray()
    End Function

    Private Function CzyPustePoleCombo(combo As ComboBox) As Boolean
        If combo.SelectedValue Is Nothing OrElse String.IsNullOrEmpty(combo.SelectedValue.ToString()) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function SprawdzFormatDanych() As String()
        Dim nieprawidloweFormatyMsgs As New List(Of String)
        Dim liczba As Double

        If Not String.IsNullOrEmpty(slogKwotaPobraniaTextBox.Text) AndAlso Not Double.TryParse(slogKwotaPobraniaTextBox.Text, liczba) Then nieprawidloweFormatyMsgs.Add("Nieprawidłowy format kwoty pobrania.")
        If Not String.IsNullOrEmpty(slogPaletaDlugoscTextBox.Text) AndAlso Not Double.TryParse(slogPaletaDlugoscTextBox.Text, liczba) Then nieprawidloweFormatyMsgs.Add("Nieprawidłowy format liczby w polu 'Paleta - Długość'.")
        If Not String.IsNullOrEmpty(slogPaletaSzerokoscTextBox.Text) AndAlso Not Double.TryParse(slogPaletaSzerokoscTextBox.Text, liczba) Then nieprawidloweFormatyMsgs.Add("Nieprawidłowy format liczby w polu 'Paleta - Szerokość'.")
        If Not String.IsNullOrEmpty(slogPaletaWysokoscTextBox.Text) AndAlso Not Double.TryParse(slogPaletaWysokoscTextBox.Text, liczba) Then nieprawidloweFormatyMsgs.Add("Nieprawidłowy format liczby w polu 'Paleta - Wysokość'.")
        If Not String.IsNullOrEmpty(slogWagaTextBox.Text) AndAlso Not Double.TryParse(slogWagaTextBox.Text, liczba) Then nieprawidloweFormatyMsgs.Add("Nieprawidłowy format liczby w polu 'Waga'.")
        If Not String.IsNullOrEmpty(slogOdbiorcaMiastoTextBox.Text) AndAlso slogWagaTextBox.Text.Length > 17 Then nieprawidloweFormatyMsgs.Add("Zbyt długa nazwa miasta w danych adresowych (limit 17 znaków).")

        If Not String.IsNullOrEmpty(slogNrReferencyjnyTextBox.Text) Then
            Using connection As New SqlConnection(My.Settings.SLOGConnectionString)
                connection.Open()

                Using cmd As New SqlCommand()
                    cmd.Connection = connection
                    cmd.CommandText = String.Format("SELECT TOP 1 ID FROM paczki WHERE NrRef = '{0}'", slogNrReferencyjnyTextBox.Text)

                    Using reader As SqlDataReader = cmd.ExecuteReader()
                        If reader.HasRows Then nieprawidloweFormatyMsgs.Add("Podany numer referencyjny nie jest unikatowy.")
                    End Using
                End Using
            End Using
        End If

        If Not String.IsNullOrEmpty(slogOdbiorcaAdresTextBox.Text) Then
            Try
                Dim adres As New Adres(slogOdbiorcaAdresTextBox.Text)
            Catch ex As Exception
                nieprawidloweFormatyMsgs.Add("Nie udało się wyodrębnić składowych adresu odbiorcy. Prawdopodobnie za dużo znaków. Limit: ulica - 22, nr domu - 7, nr mieszk. - 7, lub łącznie: 36.")
            End Try
        End If

        If Not String.IsNullOrEmpty(slogOdbiorcaTelefonTextBox.Text) Then
            Dim nrTel As String = slogOdbiorcaTelefonTextBox.Text.Replace(" ", "").Replace("-", "").Replace("+", "").Replace("(", "").Replace(")", "").Replace("\", "").Replace("/", "")
            Dim telefonRegex As New System.Text.RegularExpressions.Regex("^[0-9]*$")

            If Not telefonRegex.IsMatch(nrTel) Then
                nieprawidloweFormatyMsgs.Add("Nieprawidłowe znaki w polu 'Odbiorca - Telefon'.")
                'ElseIf nrTel.Length > 9 Then
                '    nieprawidloweFormatyMsgs.Add("Zbyt dużo  w polu 'Odbiorca - Telefon' (limit 9 znaków).")
            End If
        End If

        Dim regex As New System.Text.RegularExpressions.Regex("^[0-9]{2}-[0-9]{3}$")
        If Not String.IsNullOrEmpty(slogOdbiorcaKodPocztowyTextBox.Text) AndAlso Not regex.IsMatch(slogOdbiorcaKodPocztowyTextBox.Text) Then nieprawidloweFormatyMsgs.Add("Niepoprawny format kodu pocztowego.")


        Return nieprawidloweFormatyMsgs.ToArray()
    End Function

    Private Function ZapiszDoBazy() As Boolean
        Dim liczbaPaczek As Integer = 1

        Try
            Using connection As New SqlConnection(My.Settings.SLOGConnectionString)
                connection.Open()

                Using cmd As New SqlCommand()
                    cmd.Connection = connection
                    cmd.CommandText = String.Concat("INSERT INTO paczki (Spedytor, Stanowisko, DataNadaniaWgSAP, Zrealizowane, Status, TypUslugi, KodyUslugDod, Pobranie, KwotaPobrania, RodzajPaczki, RodzajPalety, PaletaDl, PaletaSzer, PaletaWys, PaczkaNiestandardowa, Waga, Gabaryt, NrRef, NrPaczki, NrPaczkiWiodacej, OdbNazwa, OdbOsoba, OdbAdres, OdbMiasto, OdbKodPocztowy, OdbTelefon, OdbMail, PlatnoscZaPrzesylke, L_paczek, PotwierdzenieWyjsciaZMag) VALUES ",
                                                    "(@spedytor, @stanowisko, @dataNadaniaWgSAP, 0, 1, @typUslugi, @kodyUslugDod, @pobranie, @kwotaPobrania, @rodzajPaczki, @rodzajPalety, @paletaDl, @paletaSzer,",
                                                    "@paletaWys, @paczkaNiestandardowa, @waga, @gabaryt, @nrRef, @nrPaczki, @nrPaczkiWiodacej, @odbNazwa, @odbOsoba, @odbAdres, @odbMiasto, @odbKodPocztowy, @odbTelefon, @odbMail, @platnoscZaPrzesylke, @L_paczek, @PotwierdzenieWyjsciaZMag)")

                    cmd.Parameters.Add("@spedytor", SqlDbType.VarChar).Value = GetDBText(slogSpedytorComboBox.SelectedValue)
                    cmd.Parameters.Add("@stanowisko", SqlDbType.VarChar).Value = Environment.MachineName
                    cmd.Parameters.Add("@dataNadaniaWgSAP", SqlDbType.Date).Value = slogDataNadaniaDateTimePicker.Value
                    cmd.Parameters.Add("@typUslugi", SqlDbType.VarChar).Value = GetDBText(slogTypUslugiComboBox.SelectedValue)

                    cmd.Parameters.Add("@kodyUslugDod", SqlDbType.VarChar).Value = String.Join(",", slogKodyUslugDodCheckedListBox.CheckedItems.Cast(Of ElementSlownika).Select(Function(item) item.Kod))

                    cmd.Parameters.Add("@pobranie", SqlDbType.Int).Value = GetDBBoolAsInteger(slogPobranieCheckBox.Checked)
                    cmd.Parameters.Add("@kwotaPobrania", SqlDbType.Float).Value = GetDBDouble(slogKwotaPobraniaTextBox.Text)
                    cmd.Parameters.Add("@rodzajPaczki", SqlDbType.VarChar).Value = GetDBText(slogRodzajPaczkiComboBox.SelectedValue)
                    cmd.Parameters.Add("@rodzajPalety", SqlDbType.VarChar).Value = GetDBText(slogRodzajPaletyComboBox.SelectedValue)
                    cmd.Parameters.Add("@paletaDl", SqlDbType.Float).Value = GetDBDouble(slogPaletaDlugoscTextBox.Text)
                    cmd.Parameters.Add("@paletaSzer", SqlDbType.Float).Value = GetDBDouble(slogPaletaSzerokoscTextBox.Text)
                    cmd.Parameters.Add("@paletaWys", SqlDbType.Float).Value = GetDBDouble(slogPaletaWysokoscTextBox.Text)
                    cmd.Parameters.Add("@paczkaNiestandardowa", SqlDbType.Int).Value = GetDBBoolAsInteger(slogPaczkaNiestandardowaCheckBox.Checked)
                    cmd.Parameters.Add("@waga", SqlDbType.Float).Value = GetDBDouble(slogWagaTextBox.Text)
                    cmd.Parameters.Add("@gabaryt", SqlDbType.VarChar).Value = GetDBText(slogGabarytTextBox.Text)
                    cmd.Parameters.Add("@nrRef", SqlDbType.VarChar).Value = GetDBText(slogNrReferencyjnyTextBox.Text)                    
                    cmd.Parameters.Add("@nrPaczki", SqlDbType.Int).Value = GetDBInteger(slogNrPaczkiTextBox.Text)
                    If slogNrPaczkiTextBox.Text = 1 Then
                        cmd.Parameters.Add("@nrPaczkiWiodacej", SqlDbType.Int).Value = GetDBInteger("")
                    Else
                        cmd.Parameters.Add("@nrPaczkiWiodacej", SqlDbType.Int).Value = GetDBInteger(slogNrPaczkiWiodacejTextBox.Text)
                    End If                    
                    cmd.Parameters.Add("@odbNazwa", SqlDbType.VarChar).Value = GetDBText(slogOdbiorcaNazwaTextBox.Text)
                    cmd.Parameters.Add("@odbOsoba", SqlDbType.VarChar).Value = GetDBText(slogOdbiorcaOsobaTextBox.Text)
                    cmd.Parameters.Add("@odbAdres", SqlDbType.VarChar).Value = GetDBText(slogOdbiorcaAdresTextBox.Text)
                    cmd.Parameters.Add("@odbMiasto", SqlDbType.VarChar).Value = GetDBText(slogOdbiorcaMiastoTextBox.Text)
                    cmd.Parameters.Add("@odbKodPocztowy", SqlDbType.VarChar).Value = GetDBText(slogOdbiorcaKodPocztowyTextBox.Text)
                    cmd.Parameters.Add("@odbTelefon", SqlDbType.VarChar).Value = GetDBText(slogOdbiorcaTelefonTextBox.Text)
                    cmd.Parameters.Add("@odbMail", SqlDbType.VarChar).Value = GetDBText(slogOdbiorcaMailTextBox.Text)
                    cmd.Parameters.Add("@platnoscZaPrzesylke", SqlDbType.Int).Value = GetDBInteger(slogPlatnoscZaPrzesylkeComboBox.SelectedValue)

                    cmd.Parameters.Add("@L_paczek", SqlDbType.Int).Value = liczbaPaczek
                    cmd.Parameters.Add("@PotwierdzenieWyjsciaZMag", SqlDbType.Int).Value = liczbaPaczek

                    cmd.ExecuteNonQuery()
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show("Wystąpił błąd:" & vbCrLf & vbCrLf & ex.ToString(), "sLog Client - Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try

        Return True
    End Function

    Private Function GetDBText(obj As Object) As Object
        If Not obj Is Nothing AndAlso Not String.IsNullOrEmpty(obj.ToString()) Then
            Return obj.ToString()
        Else
            Return DBNull.Value
        End If
    End Function

    Private Function GetDBDouble(obj As Object) As Object
        If Not obj Is Nothing AndAlso Not String.IsNullOrEmpty(obj.ToString()) Then
            Return Double.Parse(obj.ToString())
        Else
            Return DBNull.Value
        End If
    End Function

    Private Function GetDBInteger(obj As Object) As Object
        If Not obj Is Nothing AndAlso Not String.IsNullOrEmpty(obj.ToString()) Then
            Return Integer.Parse(obj.ToString())
        Else
            Return DBNull.Value
        End If
    End Function

    Private Function GetDBBoolAsInteger(boolValue As Boolean) As Object
        If boolValue = True Then Return 1 Else Return 0
    End Function

    Private Sub slogAnulujButton_Click(sender As System.Object, e As System.EventArgs) Handles slogAnulujButton.Click
        Close()
    End Sub

    Private Sub slogKodyUslugDodCheckedListBox_ItemCheck(sender As System.Object, e As System.Windows.Forms.ItemCheckEventArgs) Handles slogKodyUslugDodCheckedListBox.ItemCheck
        Dim el As ElementSlownika = CType(slogKodyUslugDodCheckedListBox.Items(e.Index), ElementSlownika)

        If el.Kod = "COD" Then
            If e.NewValue = CheckState.Checked Then
                If Not slogPobranieCheckBox.Checked Then slogPobranieCheckBox.Checked = True
            Else
                If slogPobranieCheckBox.Checked Then slogPobranieCheckBox.Checked = False
            End If
        End If
    End Sub

    Private Sub slogPobranieCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles slogPobranieCheckBox.CheckedChanged
        Dim codItem As ElementSlownika = slogKodyUslugDodCheckedListBox.Items.Cast(Of ElementSlownika).FirstOrDefault(Function(el) el.Kod = "COD")
        Dim codItemIndex As Integer = slogKodyUslugDodCheckedListBox.Items.IndexOf(codItem)
        Dim codSelected As Boolean = slogKodyUslugDodCheckedListBox.GetItemChecked(codItemIndex)

        If slogPobranieCheckBox.Checked Then
            If Not codSelected Then slogKodyUslugDodCheckedListBox.SetItemChecked(codItemIndex, True)
        Else
            If codSelected Then slogKodyUslugDodCheckedListBox.SetItemChecked(codItemIndex, False)
        End If

        UstawPola()
    End Sub

    Private Sub slogRodzajPaczkiComboBox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles slogRodzajPaczkiComboBox.SelectedIndexChanged
        UstawPola()
    End Sub

    Private Sub btnPobierzDaneZref_Click(sender As Object, e As EventArgs) Handles btnPobierzDaneZref.Click

        Dim oknoPobieraniaDanychZreferencji As New pobierzDaneZreferncji

        Dim paczkaOrg As PaczkaExtended


        If oknoPobieraniaDanychZreferencji.ShowDialog() = Windows.Forms.DialogResult.OK Then

            paczkaOrg = pobierzDaneZreferencjiService.pobierzDanePaczki()

            If paczkaOrg.Spedytor <> "" Then
                Me.slogSpedytorComboBox.Text = paczkaOrg.Spedytor
                Me.slogOdbiorcaNazwaTextBox.Text = paczkaOrg.OdbiorcaNazwa
                Me.slogOdbiorcaOsobaTextBox.Text = paczkaOrg.OdbiorcaOsoba
                Me.slogOdbiorcaAdresTextBox.Text = paczkaOrg.OdbAdres
                Me.slogOdbiorcaMiastoTextBox.Text = paczkaOrg.OdbiorcaMiejscowosc
                Me.slogOdbiorcaKodPocztowyTextBox.Text = paczkaOrg.OdbiorcaKodPocztowy
                Me.slogOdbiorcaTelefonTextBox.Text = paczkaOrg.OdbiorcaTelefon
                Me.slogOdbiorcaMailTextBox.Text = paczkaOrg.OdbiorcaEmail
            Else
                MsgBox("Nieznaleziiono paczki.", MsgBoxStyle.OkOnly)
            End If

        End If

    End Sub
End Class