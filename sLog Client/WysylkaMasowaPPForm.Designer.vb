﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class WysylkaMasowaPPForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.postepLabel = New System.Windows.Forms.Label()
        Me.rozpocznijAnulujButton = New System.Windows.Forms.Button()
        Me.wysylkaProgressBar = New System.Windows.Forms.ProgressBar()
        Me.nrRefTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.wysylkaBackgroundWorker = New System.ComponentModel.BackgroundWorker()
        Me.KontynuujButton = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'postepLabel
        '
        Me.postepLabel.AutoSize = True
        Me.postepLabel.Location = New System.Drawing.Point(12, 76)
        Me.postepLabel.Name = "postepLabel"
        Me.postepLabel.Size = New System.Drawing.Size(43, 13)
        Me.postepLabel.TabIndex = 12
        Me.postepLabel.Text = "Postęp:"
        '
        'rozpocznijAnulujButton
        '
        Me.rozpocznijAnulujButton.Location = New System.Drawing.Point(201, 42)
        Me.rozpocznijAnulujButton.Name = "rozpocznijAnulujButton"
        Me.rozpocznijAnulujButton.Size = New System.Drawing.Size(82, 23)
        Me.rozpocznijAnulujButton.TabIndex = 11
        Me.rozpocznijAnulujButton.Text = "Rozpocznij"
        Me.rozpocznijAnulujButton.UseVisualStyleBackColor = True
        '
        'wysylkaProgressBar
        '
        Me.wysylkaProgressBar.Location = New System.Drawing.Point(15, 92)
        Me.wysylkaProgressBar.Name = "wysylkaProgressBar"
        Me.wysylkaProgressBar.Size = New System.Drawing.Size(268, 23)
        Me.wysylkaProgressBar.TabIndex = 10
        '
        'nrRefTextBox
        '
        Me.nrRefTextBox.Location = New System.Drawing.Point(129, 16)
        Me.nrRefTextBox.Name = "nrRefTextBox"
        Me.nrRefTextBox.Size = New System.Drawing.Size(154, 20)
        Me.nrRefTextBox.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(111, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Podaj numer dostawy:"
        '
        'wysylkaBackgroundWorker
        '
        Me.wysylkaBackgroundWorker.WorkerReportsProgress = True
        Me.wysylkaBackgroundWorker.WorkerSupportsCancellation = True
        '
        'KontynuujButton
        '
        Me.KontynuujButton.Enabled = False
        Me.KontynuujButton.Location = New System.Drawing.Point(201, 131)
        Me.KontynuujButton.Name = "KontynuujButton"
        Me.KontynuujButton.Size = New System.Drawing.Size(82, 23)
        Me.KontynuujButton.TabIndex = 15
        Me.KontynuujButton.Text = "Kontynuuj"
        Me.KontynuujButton.UseVisualStyleBackColor = True
        '
        'WysylkaMasowaPPForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(306, 172)
        Me.Controls.Add(Me.postepLabel)
        Me.Controls.Add(Me.rozpocznijAnulujButton)
        Me.Controls.Add(Me.wysylkaProgressBar)
        Me.Controls.Add(Me.nrRefTextBox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.KontynuujButton)
        Me.Name = "WysylkaMasowaPPForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Wysyłka masowa POCZTEX"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents postepLabel As System.Windows.Forms.Label
    Friend WithEvents rozpocznijAnulujButton As System.Windows.Forms.Button
    Friend WithEvents wysylkaProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents nrRefTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents wysylkaBackgroundWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents KontynuujButton As System.Windows.Forms.Button
End Class
