﻿Imports sLog_Client.dhl24

Public Class SpedytorDHL
    Inherits Spedytor

    Public Overrides Sub UtworzPrzesylke(paczki As IEnumerable(Of PaczkaExtended))
        Dim etykieta As Etykieta = Nothing

        'Log.WriteInformation("Tworzenie przesyłki...")
        Dim paczka As PaczkaExtended = paczki.First(Function(p) String.IsNullOrEmpty(p.NrPaczkiWiodacej))


        Try
            Using dhlService As New DHL24WebapiPortClient()
                'dhlService.Url = Konfiguracja.SpedytorWebApiAdres

                Dim request As New CreateShipmentRequest()
                request.content = Konfiguracja.ZawartoscPaczki

                request.shipmentInfo = New ShipmentInfo()

                request.shipmentInfo.billing = New Billing()
                request.shipmentInfo.billing.billingAccountNumber = Konfiguracja.PlatnoscNumerKlienta
                request.shipmentInfo.billing.costsCenter = Konfiguracja.PlatnoscCentrumKosztow
                request.shipmentInfo.billing.paymentType = Konfiguracja.PlatnoscTyp
                request.shipmentInfo.billing.shippingPaymentType = Konfiguracja.PlatnoscPlatnik

                request.comment = paczka.NrRef & "/" & paczka.NrPaczki

                request.shipmentInfo.dropOffType = Konfiguracja.DHL_WebApiTypZadania
                request.shipmentInfo.labelType = Konfiguracja.DHL_WebApiTypEtykiety
                request.shipmentInfo.serviceType = Konfiguracja.DHL_WebApiTypUslubi

                request.shipmentInfo.shipmentTime = New ShipmentTime()

                If paczka.DataNadania.HasValue Then request.shipmentInfo.shipmentTime.shipmentDate = paczka.DataNadania.Value.ToString("yyyy-MM-dd")

                request.shipmentInfo.shipmentTime.shipmentStartHour = Konfiguracja.DHL_NadanieWGodzinachStart
                request.shipmentInfo.shipmentTime.shipmentEndHour = Konfiguracja.DHL_NadanieWGodzinachKoniec

                Dim usluga As Service = Nothing
                Dim uslugiDodatkowe As New List(Of Service)

                If Not paczka.UslugiDodatkowe Is Nothing Then
                    For Each u In paczka.UslugiDodatkowe
                        usluga = New Service()
                        usluga.serviceType = u

                        Select Case u
                            Case "COD"
                                usluga.serviceValue = paczka.WartoscZwrotuPobrania
                            Case "UBEZP"
                                usluga.serviceValue = Konfiguracja.WartoscUbezpieczenia
                            Case Else
                                usluga.serviceValue = Nothing
                        End Select

                        usluga.textInstruction = Nothing

                        If u = "COD" Then
                            usluga.collectOnDeliveryForm = Konfiguracja.FormaZwrotuPobrania
                        Else
                            usluga.collectOnDeliveryForm = Nothing
                        End If

                        uslugiDodatkowe.Add(usluga)
                    Next

                    If paczka.UslugiDodatkowe.Contains("COD") AndAlso Not paczka.UslugiDodatkowe.Contains("UBEZP") Then
                        usluga = New Service()
                        usluga.serviceType = "UBEZP"
                        usluga.serviceValue = Konfiguracja.WartoscUbezpieczenia
                        usluga.collectOnDeliveryForm = Nothing

                        uslugiDodatkowe.Add(usluga)
                    End If
                End If

                'usluga = New Service()
                'usluga.serviceType = "UBEZP"
                'usluga.serviceValue = 10
                'usluga.textInstruction = "121212"
                'usluga.collectOnDeliveryForm = Nothing
                'uslugiDodatkowe.Add(usluga)

                request.shipmentInfo.specialServices = uslugiDodatkowe.ToArray()

                request.ship = New Ship()

                request.ship.shipper = New Addressat()
                request.ship.shipper.address = New Address()

                If Konfiguracja.NadawcaNazwa.Length <= 60 Then
                    request.ship.shipper.address.name = Konfiguracja.NadawcaNazwa
                Else
                    request.ship.shipper.address.name = Konfiguracja.NadawcaNazwa.Substring(0, 60)
                End If

                request.ship.shipper.address.postalCode = Konfiguracja.NadawcaKodPocztowy.Replace("-", "")

                If Konfiguracja.NadawcaMiejscowosc.Length <= 17 Then
                    request.ship.shipper.address.city = Konfiguracja.NadawcaMiejscowosc
                Else
                    request.ship.shipper.address.city = Konfiguracja.NadawcaMiejscowosc.Substring(0, 17)
                End If

                request.ship.shipper.address.street = Konfiguracja.NadawcaUlica
                request.ship.shipper.address.houseNumber = Konfiguracja.NadawcaNrBudynku
                request.ship.shipper.address.apartmentNumber = Konfiguracja.NadawcaNrLokalu

                'request.ship.shipper.contact = New PreavisoContact()
                'request.ship.shipper.contact.personName = "Osoba kontaktowa"
                'request.ship.shipper.contact.phoneNumber = "123543908"    ' Limit 9 znaków, cyfry razem.
                'request.ship.shipper.contact.emailAddress = "abc@blabla.pl"

                'request.ship.shipper.preaviso = New PreavisoContact()
                'request.ship.shipper.preaviso.personName = "Osoba preawizująca"
                'request.ship.shipper.preaviso.phoneNumber = "009123234"
                'request.ship.shipper.preaviso.emailAddress = "qwerty@asdf.pl"

                'request.ship.receiver = New Addressat()
                'request.ship.receiver.address = New Address()

                request.ship.receiver = New ReceiverAddressat()
                request.ship.receiver.address = New ReceiverAddress()


                If paczka.OdbiorcaNazwa.Length <= 60 Then
                    request.ship.receiver.address.name = paczka.OdbiorcaNazwa
                Else
                    request.ship.receiver.address.name = paczka.OdbiorcaNazwa.Substring(0, 60)
                End If

                request.ship.receiver.address.postalCode = paczka.OdbiorcaKodPocztowy.Replace("-", "")

                If paczka.OdbiorcaMiejscowosc.Length <= 17 Then
                    request.ship.receiver.address.city = paczka.OdbiorcaMiejscowosc
                Else
                    request.ship.receiver.address.city = paczka.OdbiorcaMiejscowosc.Substring(0, 17)
                End If

                request.ship.receiver.address.street = paczka.OdbiorcaUlica
                request.ship.receiver.address.houseNumber = paczka.OdbiorcaNrBudynku
                request.ship.receiver.address.apartmentNumber = paczka.OdbiorcaNrLokalu

                'request.ship.receiver.address.country = "PL"

                request.ship.receiver.contact = New PreavisoContact()
                request.ship.receiver.contact.personName = paczka.OdbiorcaOsoba
                request.ship.receiver.contact.phoneNumber = paczka.OdbiorcaTelefon.Replace(" ", "").Replace("-", "").Replace("+", "").Replace("(", "").Replace(")", "").Replace("\", "").Replace("/", "")
                request.ship.receiver.contact.emailAddress = paczka.OdbiorcaEmail

                'request.ship.receiver.preaviso = New PreavisoContact()
                'request.ship.receiver.preaviso.personName = "Osoba preawizująca"
                'request.ship.receiver.preaviso.phoneNumber = "009123234"
                'request.ship.receiver.preaviso.emailAddress = "qwerty@asdf.pl"

                Dim packages As New List(Of Package)

                For Each paczka In paczki
                    Dim jednaPaczka As New Package()
                    jednaPaczka.type = paczka.Typ
                    jednaPaczka.quantity = 1
                    If paczka.Dlugosc.HasValue Then
                        jednaPaczka.length = CType(Math.Round(paczka.Dlugosc.Value), Integer)
                    Else
                        jednaPaczka.length = 1
                    End If

                    If paczka.Wysokosc.HasValue Then
                        jednaPaczka.height = CType(Math.Round(paczka.Wysokosc.Value), Integer)
                    Else
                        jednaPaczka.height = 1
                    End If

                    If paczka.Szerokosc.HasValue Then
                        jednaPaczka.width = CType(Math.Round(paczka.Szerokosc.Value), Integer)
                    Else
                        jednaPaczka.width = 1
                    End If

                    If paczka.Waga.HasValue Then
                        jednaPaczka.weight = CType(Math.Round(paczka.Waga.Value), Integer)
                    Else
                        jednaPaczka.weight = 1
                    End If

                    If jednaPaczka.weight < 1 Then jednaPaczka.weight = 1

                    packages.Add(jednaPaczka)
                Next

                request.pieceList = packages.ToArray()

                Dim response As CreateShipmentResponse = dhlService.createShipment(GetAuthData(), request)

                Dim multiEtykieta As Etykieta = New Etykieta(response.label.labelContent)
                Dim etykiety As Etykieta() = multiEtykieta.WyodrebnijEtykietyDHL()
                Dim i As Integer = 0

                For Each paczka In paczki
                    paczka.NrListu = response.shipmentNotificationNumber
                    etykiety(i).UstawPrzesuniecieDHL()
                    etykiety(i).CorrectFont()
                    paczka.Etykieta = etykiety(i)
                    paczka.Etykieta.WstawNrRefINrPaczkiDHL(paczka)
                    i += 1

                    paczka.AktualizujDaneSpedycyjne()
                Next
            End Using
        Catch ex As Exception
            For Each paczka In paczki
                paczka.UstawOpisBledu("[Spedytor] " & ex.Message, 2)
            Next
        End Try
    End Sub

    Private Shared Function NajblizszyRoboczyDzien() As DateTime
        Dim dzien As DateTime = DateTime.Today

        If dzien.DayOfWeek = DayOfWeek.Saturday Then dzien = dzien.AddDays(2)
        If dzien.DayOfWeek = DayOfWeek.Sunday Then dzien = dzien.AddDays(1)

        Return dzien
    End Function

    Public Overrides Sub PobierzEtykiety(paczki As IEnumerable(Of Paczka))
        'Log.WriteInformation("Pobieranie etykiety...")

        Try
            Using dhlService As New DHL24WebapiPortClient()
                Dim requestArray As ItemToPrint() = {New ItemToPrint With {.labelType = Konfiguracja.DHL_WebApiTypEtykiety, .shipmentId = paczki(0).NrListu}}
                Dim responseArray As ItemToPrintResponse() = dhlService.getLabels(GetAuthData(), requestArray)

                For Each paczka As PaczkaExtended In paczki
                    If Not responseArray Is Nothing AndAlso responseArray.Length > 0 Then
                        paczka.Etykieta = New Etykieta(responseArray(0).labelData)
                    End If
                Next
            End Using
        Catch ex As Exception
            For Each paczka As PaczkaExtended In paczki
                paczka.UstawOpisBledu("[Spedytor] " & ex.Message, 2)
            Next
        End Try
    End Sub

    Public Overrides Sub PobierzEtykiete(paczka As Paczka)
        Try
            Using dhlService As New DHL24WebapiPortClient()
                Dim requestArray As ItemToPrint() = {New ItemToPrint With {.labelType = Konfiguracja.DHL_WebApiTypEtykiety, .shipmentId = paczka.NrListu}}
                Dim responseArray As ItemToPrintResponse() = dhlService.getLabels(GetAuthData(), requestArray)

                If Not responseArray Is Nothing AndAlso responseArray.Length > 0 Then
                    paczka.Etykieta = New Etykieta(responseArray(0).labelData)
                End If
            End Using
        Catch ex As Exception
            paczka.UstawOpisBledu("[Spedytor] " & ex.Message, 2)
        End Try
    End Sub

    Public Overrides Sub AnulujPrzesylke(paczki As IEnumerable(Of PaczkaExtended))
        'Log.WriteInformation("Anulowanie przesyłki...")

        Try
            Using dhlService As New DHL24WebapiPortClient()
                Dim request As New DeleteShipmentRequest()

                request.shipmentIdentificationNumber = paczki(0).NrListu
                request.dispatchIdentificationNumber = ""

                Dim response As DeleteShipmentResponse = dhlService.deleteShipment(GetAuthData(), request)

                If response.result.HasValue AndAlso response.result.Value = True Then
                    For Each paczka As PaczkaExtended In paczki
                        paczka.OznaczJakoAnulowane()
                    Next
                End If
            End Using
        Catch ex As Exception
            For Each paczka As PaczkaExtended In paczki
                paczka.UstawOpisBledu("[Spedytor] " & ex.Message, 2)
            Next
        End Try
    End Sub

    Private Shared Function GetAuthData()
        Dim authData As New AuthData()
        authData.username = Konfiguracja.DHL_AutoryzacjaLogin
        authData.password = Konfiguracja.DHL_AutoryzacjaHaslo

        Return authData
    End Function

End Class
