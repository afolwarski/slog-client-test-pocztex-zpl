﻿Module pobierzDaneZreferencjiService
    Private paczkaOrg As PaczkaExtended

    Public Sub pobierzDaneZbazy(ByVal nrReferencji As String, ByVal nrListu As String)

        paczkaOrg = PaczkaExtended.PobierzDanePaczkiZreferencji(nrReferencji, nrListu)

    End Sub

    Public Function pobierzDanePaczki() As Paczka

        Return paczkaOrg

    End Function


End Module
