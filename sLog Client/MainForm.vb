﻿Imports OfficeOpenXml
Imports System.IO
Imports System.Data.SqlClient
Imports Inpost

Public Class MainForm
    Private drzewo As drzewoZdatami
    Private rokZap As String
    Private miesiacZap As String
    Private dzienZap As String

    Private Sub MainForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        drzewo = New drzewoZdatami(Me.tvDaty)
        ZbudujDrzewoZdatami()

    End Sub

    Private Sub paczkiDataGridView_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles paczkiDataGridView.CellDoubleClick
        If e.RowIndex > -1 Then
            Dim id As Integer = Integer.Parse(paczkiDataGridView.Rows(e.RowIndex).Cells("ID").Value.ToString())
            Dim szczegolyForm As New PaczkaSzczegolyForm(id)
            If szczegolyForm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                OdswiezTabele()
            End If
        End If
    End Sub

    Private Sub OdswiezTabele()
        Me.PaczkiTableAdapter.Fill(Me.SLOGDataSet.paczki, rokZap, miesiacZap, dzienZap)
        UstawTylkoOdczyt()

    End Sub

    Private Sub UstawTylkoOdczyt()
        For Each row As DataGridViewRow In paczkiDataGridView.Rows
            row.ReadOnly = True
        Next
    End Sub

    Private Sub PrzygotujFiltry()
        Dim width As Integer = 0

        For i As Integer = 0 To paczkiDataGridView.ColumnCount - 1
            Dim column As DataGridViewColumn = paczkiDataGridView.Columns(i)

            If Not column.Visible Then Continue For

            Dim fieldName As String = column.DataPropertyName & "_TextBox"
            Dim controls As Control() = filtryPanel.Controls.Find(fieldName, False)
            Dim field As TextBox = Nothing

            If controls.Length = 0 Then
                field = New TextBox()
                field.Name = fieldName

                filtryPanel.Controls.Add(field)
                AddHandler field.KeyDown, AddressOf filterField_KeyDown
            Else
                field = CType(controls(0), TextBox)
            End If

            field.Width = column.Width + 1
            field.Left = width
            width = width + column.Width
        Next

        filtryPanel.Width = width
        filtryPanel.Left = -paczkiDataGridView.HorizontalScrollingOffset

        filtryPanel.VerticalScroll.Visible = True

    End Sub

    Private Sub FiltrujDane()
        Dim warunki As New List(Of String)

        For i As Integer = 0 To paczkiDataGridView.ColumnCount - 1
            Dim column As DataGridViewColumn = paczkiDataGridView.Columns(i)
            Dim fieldName As String = column.DataPropertyName & "_TextBox"
            Dim controls As Control() = filtryPanel.Controls.Find(fieldName, False)

            If controls.Length > 0 Then
                Dim field As TextBox = CType(controls(0), TextBox)
                Dim typeName As String = SLOGDataSet.Tables("paczki").Columns(column.DataPropertyName).DataType.FullName

                If Not String.IsNullOrEmpty(field.Text) Then
                    If typeName = "System.Int32" Then
                        Dim number As Integer
                        If Integer.TryParse(field.Text, number) Then
                            warunki.Add(String.Format("{0} = {1}", column.DataPropertyName, field.Text))
                        End If
                    ElseIf typeName = "System.String" Then
                        warunki.Add(String.Format("{0} LIKE '%{1}%'", column.DataPropertyName, field.Text))
                    ElseIf typeName = "System.DateTime" Then
                        warunki.Add(String.Format("Convert({0}, 'System.String') LIKE '%{1}%'", column.DataPropertyName, field.Text))
                    ElseIf typeName = "System.Boolean" Then
                        If field.Text = "0" Then
                            warunki.Add(String.Format("{0} IS NULL OR {0} = 0", column.DataPropertyName))
                        End If

                        If field.Text = "1" Then
                            warunki.Add(String.Format("{0} = 1", column.DataPropertyName))
                        End If
                    End If
                End If
            End If
        Next

        PaczkiBindingSource.Filter = String.Join(" AND ", warunki)
    End Sub

    Private Sub paczkiDataGridView_Scroll(sender As System.Object, e As System.Windows.Forms.ScrollEventArgs)
        If e.ScrollOrientation = ScrollOrientation.HorizontalScroll Then filtryPanel.Left = -e.NewValue
    End Sub

    Private Sub paczkiDataGridView_ColumnWidthChanged(sender As System.Object, e As System.Windows.Forms.DataGridViewColumnEventArgs)
        PrzygotujFiltry()
    End Sub

    Private Sub filterField_KeyDown(sender As Object, e As KeyEventArgs)
        If e.KeyCode = Keys.Enter Then FiltrujDane()
    End Sub

    'Private Sub paczkiDataGridView_Sorted(sender As System.Object, e As System.EventArgs) Handles paczkiDataGridView.Sorted
    '    UstawTylkoOdczyt()
    'End Sub

    Private Sub paczkiDataGridView_DataBindingComplete(sender As System.Object, e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs)
        UstawTylkoOdczyt()
    End Sub

    Private Sub utworzPaczkeButton_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub OdswiezListeButton_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub ImportAdresatowWzorcowaPaczkaButton_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Function GetLabelGUIDByNoRef(nrref As String, nrpaczki As String)

    End Function

    Private Sub GetLabelForPP(NrRef As String, nrpaczki As String)
        ' Guid = E4F0BF0427F64C8BA0E21407698DEDAF, B99F5C24FEB149CF923D39B1CA888877

        Dim GUID As String
        Dim FileName As String

        Try

            Using connection As New SqlConnection(My.Settings.SLOGConnectionString)
                connection.Open()

                Using cmd As New SqlCommand()
                    Dim rd As SqlDataReader
                    cmd.Connection = connection
                    cmd.CommandText = System.String.Format("SELECT IsNull([IDPrzesyłki],'') As GUID FROM Paczki WHERE nrref='{0}' and nrpaczki='{1}'", NrRef, nrpaczki)
                    rd = cmd.ExecuteReader
                    If rd.Read Then
                        If rd.Item("GUID").ToString.Length > 0 Then
                            GUID = rd.Item("GUID")
                        End If
                    End If
                    rd.Close()

                End Using

            End Using


        Catch ex As Exception
            Log.WriteError(ex.ToString())
            ' Log.WriteToDB(Me, 5, ex.ToString())
        End Try

        If GUID = "" Then Exit Sub

        FileName = System.String.Format("c:\\pocztex\\{0}_{1}.pdf", GUID, Now.ToString("yyyyMMddHHmmssfff"))

        Dim en As New pocztapolska.ElektronicznyNadawca
        Dim network_creditential As New System.Net.NetworkCredential
        Dim creditential_cache As New System.Net.CredentialCache

        network_creditential = PP_GetLoginData()

        creditential_cache.Add(New Uri("https://en-testwebapi.poczta-polska.pl/websrv/en.wsdl"), "Basic", network_creditential)
        en.Credentials = creditential_cache

        Dim Bledy As pocztapolska.errorType()
        Dim Labels As Byte()

        'Labels = en.getAddresLabelByGuid({"E4F0BF0427F64C8BA0E21407698DEDAF"}, 0, False, Bledy)
        Labels = en.getAddresLabelByGuidCompact({GUID}, 0, False, Bledy)

        Dim PDFToSave As New System.IO.FileStream(FileName, FileMode.Create)
        PDFToSave.Write(Labels, 0, Labels.Length)
        PDFToSave.Close()
        Process.Start(FileName)

    End Sub




    Private Sub WydrukujEtykieteButton_Click(sender As System.Object, e As System.EventArgs)


    End Sub

    Private Sub EksportujListeButton_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub WysylkaMasowaButton_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub MainForm_Shown(sender As System.Object, e As System.EventArgs) Handles MyBase.Shown

        UstawDateZapytania()
        SprawdzAktualizacje()
        PrzygotujFiltry()
        OdswiezTabele()
        ZaznaczDzien()

    End Sub

    Private Sub SprawdzAktualizacje()
        If Not CzyProgramAktualny() Then
            Dim msg As String = "Pojawiła się nowa wersja programu. Czy chcesz teraz dokonać aktualizacji?"

            If MessageBox.Show(msg, "SLOG CLIENT - Nowa wersja", MessageBoxButtons.OKCancel, MessageBoxIcon.None) = Windows.Forms.DialogResult.OK Then
                UruchomAktualizacje()
                Close()
            End If
        End If
    End Sub

    Private Function CzyProgramAktualny() As Boolean
        Konfiguracja.Odswiez()

        If Not String.IsNullOrEmpty(Konfiguracja.WersjaProgramu) Then
            Dim wersjaBiezaca As Integer = Integer.Parse(Application.ProductVersion.Replace(".", ""))
            Dim wersjaAktualna As Integer = Integer.Parse(Konfiguracja.WersjaProgramu.Replace(".", ""))

            If wersjaBiezaca < wersjaAktualna Then
                Return False
            End If
        End If

        Return True
    End Function

    Private Sub UruchomAktualizacje()
        Process.Start(Konfiguracja.InstalatorProgramu)
    End Sub



    Private Sub Button_inpost_Click(sender As Object, e As EventArgs)


    End Sub

    Public Sub InpostUpdateAfterGenerate(IDPAczki As Integer)
        SQL_UpdateQuery(String.Format("UPDATE paczki SET Zrealizowane = '{0}', Status ={1}, DataNadaniaReal='{2}' WHERE ID = {3}", 1, 10, DateTime.Now.ToString(), IDPAczki))
    End Sub

    Protected Function SQL_UpdateQuery(query As String) As Boolean
        Try
            Using connection As New SqlConnection(My.Settings.SLOGConnectionString)
                connection.Open()

                Using cmd As New SqlCommand()
                    cmd.Connection = connection
                    cmd.CommandText = query
                    cmd.ExecuteNonQuery()
                End Using
            End Using
        Catch ex As Exception
            Log.WriteError(ex.ToString())
            ' Log.WriteToDB(Me, 5, ex.ToString())
            Return False
        End Try

        Return True
    End Function

    Private Sub ZbudujDrzewoZdatami()

        Dim yearTemp As String = ""
        Dim monthTemp As String = ""
        Dim y As Integer
        Dim m As Integer
        Dim datyWysylek As List(Of String)

        Me.tvDaty.Nodes.Clear()

        datyWysylek = drzewo.PobierzDaty

        For Each dzien As String In datyWysylek

            If yearTemp = "" Then
                yearTemp = Year(CDate(dzien))
                y = 0

                drzewo.dodajRok(yearTemp)

            End If

            If monthTemp = "" Then
                monthTemp = Month(CDate(dzien))
                m = 0

                drzewo.DodajMiesiac(drzewo.NazwaMiesiaca(monthTemp), y)

            End If

            If yearTemp <> Year(CDate(dzien)) Then
                yearTemp = Year(CDate(dzien))
                y = y + 1
                m = -1

                drzewo.dodajRok(yearTemp)

            End If

            If monthTemp <> Month(CDate(dzien)) Then
                monthTemp = Month(CDate(dzien))
                m = m + 1

                drzewo.DodajMiesiac(drzewo.NazwaMiesiaca(monthTemp), y)

            End If

            drzewo.DodajDzien(m, y, Microsoft.VisualBasic.DateAndTime.Day(dzien))

        Next

    End Sub

    Private Sub tvDaty_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles tvDaty.AfterSelect
        Dim wybranaData() As String

        wybranaData = Split(tvDaty.SelectedNode.FullPath, "\")
        If wybranaData.Length = 1 Then
            If MsgBox("Wczytać dane z całego roku?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Exit Sub
            End If
        End If

        If wybranaData.Length > 1 Then
            wybranaData(1) = drzewo.NumerMiesiaca(wybranaData(1).ToString)
        End If

        Select Case wybranaData.Length
            Case 3
                rokZap = wybranaData(0)
                miesiacZap = wybranaData(1)
                dzienZap = wybranaData(2)
            Case 2
                rokZap = wybranaData(0)
                miesiacZap = wybranaData(1)
                dzienZap = 0
            Case 1
                rokZap = wybranaData(0)
                miesiacZap = 0
                dzienZap = 0
        End Select

        OdswiezTabele()

    End Sub

    Private Sub UstawDateZapytania()
        rokZap = Year(Now)
        miesiacZap = Month(Now)
        dzienZap = Microsoft.VisualBasic.DateAndTime.Day(Now)
    End Sub


    Private Sub ZaznaczDzien()

        tvDaty.SelectedNode = tvDaty.Nodes(rokZap).Nodes(miesiacZap)

    End Sub

    Private Function PP_GetLoginData() As Net.NetworkCredential
        Dim network_creditential As New System.Net.NetworkCredential
        network_creditential.UserName = Konfiguracja.PP_login
        network_creditential.Password = Konfiguracja.PP_pass

        Return network_creditential

    End Function

    Function WasEnvelopeSendedForDate(data As Date) As Boolean
        Return False
    End Function

    Private Sub SendEvelope()

        If WasEnvelopeSendedForDate(Now) = True Then Exit Sub

        If MessageBox.Show("Czy napewno chcesz przekazać przesyłki z dnia do Poczty Polskiej ?", "Send Envelope", MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> Windows.Forms.DialogResult.Yes Then
            Exit Sub
        End If

        Dim en As New pocztapolska.ElektronicznyNadawca
        Dim network_creditential As New System.Net.NetworkCredential
        Dim creditential_cache As New System.Net.CredentialCache

        network_creditential = PP_GetLoginData()

        creditential_cache.Add(New Uri("https://en-testwebapi.poczta-polska.pl/websrv/en.wsdl"), "Basic", network_creditential)
        en.Credentials = creditential_cache

        Dim uNadania As pocztapolska.urzadNadaniaFullType()
        uNadania = en.getUrzedyNadania()

        Dim idEvelope As Integer
        Dim idEnvelopeSpec As Boolean
        Dim envelopeStatus As pocztapolska.envelopeStatusType
        Dim envelopeStatusSpec As Boolean
        Dim bladwywolania As pocztapolska.errorType()
        Dim pakiet As pocztapolska.pakietType()
        en.sendEnvelope(uNadania(0).urzadNadania, True, pakiet, 0, False, idEvelope, idEnvelopeSpec, envelopeStatus, envelopeStatusSpec, bladwywolania)

        Dim responseStr As String
        responseStr = System.String.Format("Status wysyłki: {0}", envelopeStatus.ToString())
        responseStr = responseStr + vbCr
        For Each blad As pocztapolska.errorType In bladwywolania
            responseStr = responseStr + blad.errorDesc
            responseStr = responseStr + vbCr
        Next
        MessageBox.Show(responseStr, "Rezultat wysyłki")
    End Sub

    Private Sub btnSendEnvelope_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub toolRefresh_Click(sender As Object, e As EventArgs) Handles toolRefresh.Click
        ZbudujDrzewoZdatami()
        OdswiezTabele()
    End Sub

    Private Sub toolCreatePackage_Click(sender As Object, e As EventArgs) Handles toolCreatePackage.Click
        Dim tworzeniePaczkiForm As New PaczkaTworzenieForm()
        If tworzeniePaczkiForm.ShowDialog() = Windows.Forms.DialogResult.OK Then
            OdswiezTabele()
        End If
    End Sub

    Private Sub toolImportAddressList_Click(sender As Object, e As EventArgs) Handles toolImportAddressList.Click
        Dim importAdresatowWzorcowaPaczkaForm As New ImportAdresatowWzorcowaPaczkaForm()
        If importAdresatowWzorcowaPaczkaForm.ShowDialog = Windows.Forms.DialogResult.OK Then
            OdswiezTabele()
        End If
    End Sub

   

   

    Private Sub ToolDataExport_Click(sender As Object, e As EventArgs) Handles ToolDataExport.Click
        Dim path As String = ExcelExporter.Wyeksportuj(paczkiDataGridView, "Paczki", "SLOG Paczki")
        Process.Start(path)
    End Sub

    Private Sub WysyłkaMasowaPozostałeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles WysyłkaMasowaPozostałeToolStripMenuItem.Click
        Dim wysylkaMasowaForm As New WysylkaMasowaForm()
        wysylkaMasowaForm.ShowDialog()
        OdswiezTabele()
    End Sub

    Private Sub toolInpostListsCreate_Click(sender As Object, e As EventArgs) Handles toolInpostListsCreate.Click

        ' należy sprawdzić najpierw czy tylko inpostowe przesyłki zostały zaznaczone


        For Each row As DataGridViewRow In paczkiDataGridView.SelectedRows
            If row.Cells("Spedytor").Value.ToString <> "INPOST LISTY" Then
                MsgBox("Etykiety można wygenerować tylko dla spedytora: INPOST LISTY")
                Exit Sub
            End If
        Next


        'Dim wybierzForm As New WybierzStanowiskoForm()
        'If wybierzForm.ShowDialog() = Windows.Forms.DialogResult.OK Then
        Dim paczki As Paczka() = paczkiDataGridView.SelectedRows.Cast(Of DataGridViewRow).
            Select(Function(r)
                       Dim paczka As New Paczka()

                       paczka.Id = Integer.Parse(r.Cells("ID").Value.ToString())
                       paczka.NrRef = r.Cells("NrRef").Value.ToString()
                       paczka.NrListu = r.Cells("NrListu").Value.ToString()
                       paczka.NrPaczki = r.Cells("NrPaczki").Value.ToString()
                       paczka.Spedytor = r.Cells("Spedytor").Value.ToString()
                       paczka.OdbNazwa = r.Cells("OdbNazwa").Value.ToString()
                       paczka.OdbAdres = r.Cells("OdbAdres").Value.ToString()
                       paczka.OdbOsoba = r.Cells("OdbOsoba").Value.ToString()
                       paczka.OdbKodPocztowy = r.Cells("OdbKodPocztowy").Value.ToString()
                       paczka.OdbMiasto = r.Cells("OdbMiasto").Value.ToString()

                       If Not r.Cells("NrPaczkiWiodacej").Value Is Nothing Then paczka.NrPaczkiWiodacej = r.Cells("NrPaczkiWiodacej").Value.ToString()

                       Return paczka
                   End Function).
            ToArray()

        Inpost.Letters.Generate(paczki)

        'End If

        For Each p As Paczka In paczki
            InpostUpdateAfterGenerate(p.Id)
        Next

    End Sub

    Private Sub toolSendEnv_command_Click(sender As Object, e As EventArgs) Handles toolSendEnv_command.Click
        SendEvelope()
    End Sub

    Private Sub WysyłkaMasowaPocztaPolskaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles WysyłkaMasowaPocztaPolskaToolStripMenuItem.Click
        Dim wysylkaMasowaPPForm As New WysylkaMasowaPPForm()
        wysylkaMasowaPPForm.ShowDialog()
        OdswiezTabele()
    End Sub

    
    Private Sub toolPrintLabelsMenu_Click(sender As Object, e As EventArgs) Handles toolPrintLabelsMenu.Click
        ' jeżeli jest to etykieta poczty polskiej to
        ' sprawdzamy czy jest to 1 paczka w przesyłce, jak nie to kończymy
        ' jak tak to pobieramy z API PP etykietę do PDF, zapisujemy na dysku 
        ' i uruchamiamy Acrobat Readera z tym plikiem


        'Dim czyPoczta As Boolean = False
        'For Each selrow As DataGridViewRow In paczkiDataGridView.SelectedRows
        '    If (selrow.Cells("Spedytor").Value.ToString() = "POCZTEX") Then czyPoczta = True
        '    If (selrow.Cells("Spedytor").Value.ToString() = "POCZTEX" And selrow.Cells("NrPaczki").Value.ToString() = "1") Then
        '
        '        GetLabelForPP(selrow.Cells("NrRef").Value.ToString(), selrow.Cells("NrPaczki").Value.ToString())
        '    End If
        'Next

        'If czyPoczta = True Then Exit Sub


        Dim wybierzForm As New WybierzStanowiskoForm()

        If wybierzForm.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Dim paczki As Paczka() = paczkiDataGridView.SelectedRows.Cast(Of DataGridViewRow).
                Select(Function(r)
                           Dim paczka As New Paczka()

                           paczka.Id = Integer.Parse(r.Cells("ID").Value.ToString())
                           paczka.NrRef = r.Cells("NrRef").Value.ToString()
                           paczka.NrListu = r.Cells("NrListu").Value.ToString()
                           paczka.NrPaczki = r.Cells("NrPaczki").Value.ToString()
                           paczka.Spedytor = r.Cells("Spedytor").Value.ToString()

                           If Not r.Cells("NrPaczkiWiodacej").Value Is Nothing Then paczka.NrPaczkiWiodacej = r.Cells("NrPaczkiWiodacej").Value.ToString()

                           Return paczka
                       End Function).
                ToArray()

            DrukowanieEtykiet.Wykonaj(wybierzForm.Stanowisko, paczki)
        End If
    End Sub
End Class
