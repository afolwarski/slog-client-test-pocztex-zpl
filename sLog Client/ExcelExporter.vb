﻿Imports OfficeOpenXml
Imports System.IO

Public Class ExcelExporter
    Public Shared Function Wyeksportuj(gridView As DataGridView, nazwaArkusza As String, outFileName As String) As String
        Using excel As New ExcelPackage()
            Dim ws As ExcelWorksheet = excel.Workbook.Worksheets.Add(nazwaArkusza)

            Dim j As Integer = 1

            For Each column As DataGridViewColumn In gridView.Columns
                ws.Cells(1, j).Value = column.HeaderText
                j = j + 1
            Next

            ws.Cells(1, 1, 1, gridView.ColumnCount).Style.Font.Bold = True

            Dim i As Integer = 2

            For Each row As DataGridViewRow In gridView.Rows
                j = 1

                For Each column As DataGridViewColumn In gridView.Columns
                    Dim cellValue As String = Nothing

                    If Not row.Cells(column.Name).Value Is Nothing AndAlso Not String.IsNullOrEmpty(row.Cells(column.Name).Value.ToString()) Then
                        cellValue = row.Cells(column.Name).Value.ToString()
                    End If

                    ws.Cells(i, j).Value = cellValue

                    j = j + 1
                Next

                i = i + 1
            Next

            Dim path As String = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) & "\" & outFileName & ".xlsx"

            Using fs As New FileStream(path, FileMode.Create)
                excel.SaveAs(fs)
            End Using

            Return path
        End Using
    End Function
End Class
