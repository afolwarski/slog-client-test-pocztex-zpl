﻿Imports System.Data.SqlClient
Imports OfficeOpenXml

Public Class WysylkaMasowaRaportForm
    Public Sub New(paczkiBledneIds As IEnumerable(Of Integer))

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        Me.paczkiBledneIds = paczkiBledneIds
    End Sub

    Private paczkiBledneIds As IEnumerable(Of Integer)

    Private Sub WysylkaMasowaRaportForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Using conn As New SqlConnection(My.Settings.SLOGConnectionString)
            conn.Open()

            Using cmd As New SqlCommand()
                cmd.Connection = conn

                cmd.CommandText =
                    "SELECT Status, NrRef, NrPaczki, NrPaczkiWiodacej, OdbNazwa, OdbOsoba, " &
                    "OdbAdres, OdbMiasto, OdbKodPocztowy, OpisBledu FROM paczki " &
                    "WHERE ID IN (" & String.Join(", ", paczkiBledneIds) & ")"

                Using reader As SqlDataReader = cmd.ExecuteReader()
                    While reader.Read()
                        raportDataGridView.Rows.Add(reader("Status"), reader("NrRef"), reader("NrPaczki"), reader("NrPaczkiWiodacej"), reader("OdbNazwa"),
                                                    reader("OdbOsoba"), reader("OdbAdres"), reader("OdbMiasto"), reader("OdbKodPocztowy"), reader("OpisBledu"))
                    End While
                End Using
            End Using
        End Using
    End Sub

    Private Sub EksportDoExcelaButton_Click(sender As System.Object, e As System.EventArgs) Handles EksportDoExcelaButton.Click
        Dim path As String = ExcelExporter.Wyeksportuj(raportDataGridView, "Błędne paczki", "SLOG Wysyłka masowa - raport błędnych paczek")
        Process.Start(path)
    End Sub
End Class