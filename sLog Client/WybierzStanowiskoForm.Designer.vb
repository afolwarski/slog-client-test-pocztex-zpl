﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class WybierzStanowiskoForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.stanowiskaComboBox = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.okButton = New System.Windows.Forms.Button()
        Me.AnulujButton = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'stanowiskaComboBox
        '
        Me.stanowiskaComboBox.FormattingEnabled = True
        Me.stanowiskaComboBox.Location = New System.Drawing.Point(15, 53)
        Me.stanowiskaComboBox.Name = "stanowiskaComboBox"
        Me.stanowiskaComboBox.Size = New System.Drawing.Size(194, 21)
        Me.stanowiskaComboBox.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(293, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Wybierz stanowisko na którym chcesz wydrukować etykietę:"
        '
        'okButton
        '
        Me.okButton.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.okButton.Location = New System.Drawing.Point(144, 105)
        Me.okButton.Name = "okButton"
        Me.okButton.Size = New System.Drawing.Size(75, 23)
        Me.okButton.TabIndex = 2
        Me.okButton.Text = "OK"
        Me.okButton.UseVisualStyleBackColor = True
        '
        'AnulujButton
        '
        Me.AnulujButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.AnulujButton.Location = New System.Drawing.Point(225, 105)
        Me.AnulujButton.Name = "AnulujButton"
        Me.AnulujButton.Size = New System.Drawing.Size(75, 23)
        Me.AnulujButton.TabIndex = 3
        Me.AnulujButton.Text = "Anuluj"
        Me.AnulujButton.UseVisualStyleBackColor = True
        '
        'WybierzStanowiskoForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(324, 142)
        Me.Controls.Add(Me.AnulujButton)
        Me.Controls.Add(Me.okButton)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.stanowiskaComboBox)
        Me.Name = "WybierzStanowiskoForm"
        Me.Text = "Drukowanie - wybór stanowiska"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents stanowiskaComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents okButton As System.Windows.Forms.Button
    Friend WithEvents AnulujButton As System.Windows.Forms.Button
End Class
