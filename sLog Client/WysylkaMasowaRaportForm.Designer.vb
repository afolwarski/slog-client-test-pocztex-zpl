﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class WysylkaMasowaRaportForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.raportDataGridView = New System.Windows.Forms.DataGridView()
        Me.EksportDoExcelaButton = New System.Windows.Forms.Button()
        Me.Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NrRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NrPaczki = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NrPaczkiWiodacej = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OdbNazwa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OdbOsoba = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OdbAdres = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OdbMiasto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OdbKodPocztowy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OpidBledu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.raportDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'raportDataGridView
        '
        Me.raportDataGridView.AllowUserToAddRows = False
        Me.raportDataGridView.AllowUserToDeleteRows = False
        Me.raportDataGridView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.raportDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.raportDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Status, Me.NrRef, Me.NrPaczki, Me.NrPaczkiWiodacej, Me.OdbNazwa, Me.OdbOsoba, Me.OdbAdres, Me.OdbMiasto, Me.OdbKodPocztowy, Me.OpidBledu})
        Me.raportDataGridView.Location = New System.Drawing.Point(0, 0)
        Me.raportDataGridView.Margin = New System.Windows.Forms.Padding(0)
        Me.raportDataGridView.Name = "raportDataGridView"
        Me.raportDataGridView.ReadOnly = True
        Me.raportDataGridView.RowHeadersVisible = False
        Me.raportDataGridView.Size = New System.Drawing.Size(953, 241)
        Me.raportDataGridView.TabIndex = 0
        '
        'EksportDoExcelaButton
        '
        Me.EksportDoExcelaButton.Location = New System.Drawing.Point(12, 253)
        Me.EksportDoExcelaButton.Name = "EksportDoExcelaButton"
        Me.EksportDoExcelaButton.Size = New System.Drawing.Size(121, 23)
        Me.EksportDoExcelaButton.TabIndex = 1
        Me.EksportDoExcelaButton.Text = "Eksport do Excela"
        Me.EksportDoExcelaButton.UseVisualStyleBackColor = True
        '
        'Status
        '
        Me.Status.HeaderText = "Status"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        '
        'NrRef
        '
        Me.NrRef.HeaderText = "Nr dostawy"
        Me.NrRef.Name = "NrRef"
        Me.NrRef.ReadOnly = True
        '
        'NrPaczki
        '
        Me.NrPaczki.HeaderText = "Nr paczki"
        Me.NrPaczki.Name = "NrPaczki"
        Me.NrPaczki.ReadOnly = True
        '
        'NrPaczkiWiodacej
        '
        Me.NrPaczkiWiodacej.HeaderText = "Nr paczki wiodącej"
        Me.NrPaczkiWiodacej.Name = "NrPaczkiWiodacej"
        Me.NrPaczkiWiodacej.ReadOnly = True
        '
        'OdbNazwa
        '
        Me.OdbNazwa.HeaderText = "Nazwa odbiorcy"
        Me.OdbNazwa.Name = "OdbNazwa"
        Me.OdbNazwa.ReadOnly = True
        '
        'OdbOsoba
        '
        Me.OdbOsoba.HeaderText = "Osoba"
        Me.OdbOsoba.Name = "OdbOsoba"
        Me.OdbOsoba.ReadOnly = True
        '
        'OdbAdres
        '
        Me.OdbAdres.HeaderText = "Adres"
        Me.OdbAdres.Name = "OdbAdres"
        Me.OdbAdres.ReadOnly = True
        '
        'OdbMiasto
        '
        Me.OdbMiasto.HeaderText = "Miasto"
        Me.OdbMiasto.Name = "OdbMiasto"
        Me.OdbMiasto.ReadOnly = True
        '
        'OdbKodPocztowy
        '
        Me.OdbKodPocztowy.HeaderText = "Kod pocztowy"
        Me.OdbKodPocztowy.Name = "OdbKodPocztowy"
        Me.OdbKodPocztowy.ReadOnly = True
        '
        'OpidBledu
        '
        Me.OpidBledu.HeaderText = "Opis błędu"
        Me.OpidBledu.Name = "OpidBledu"
        Me.OpidBledu.ReadOnly = True
        Me.OpidBledu.Width = 150
        '
        'WysylkaMasowaRaportForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(953, 288)
        Me.Controls.Add(Me.EksportDoExcelaButton)
        Me.Controls.Add(Me.raportDataGridView)
        Me.Name = "WysylkaMasowaRaportForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Wysylka masowa - raport"
        CType(Me.raportDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents raportDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents EksportDoExcelaButton As System.Windows.Forms.Button
    Friend WithEvents Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NrRef As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NrPaczki As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NrPaczkiWiodacej As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OdbNazwa As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OdbOsoba As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OdbAdres As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OdbMiasto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OdbKodPocztowy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OpidBledu As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
