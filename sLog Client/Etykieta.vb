﻿Imports System.Text
Imports System.Linq
Imports System.Collections.Generic

Public Class Etykieta
    Private content As String

    Sub New(base64formatted As String)
        content = Encoding.ASCII.GetString(Convert.FromBase64String(base64formatted))
    End Sub

    Private Sub New()

    End Sub

    Public Sub CorrectFont()
        content = "^CF0,0,0" & content
    End Sub

    Public Sub UstawPrzesuniecieDHL()
        If Not String.IsNullOrEmpty(Konfiguracja.DHL_EtykietaPrzesuniecie_X) Then
            Dim indexLH As Integer = content.IndexOf("^LH")
            Dim indexNextCmd As Integer = indexLH + 3
            While content(indexNextCmd) <> "^" AndAlso content(indexNextCmd) <> "~"
                indexNextCmd = indexNextCmd + 1
            End While

            content = content.Substring(0, indexNextCmd) & "^LS" & Konfiguracja.DHL_EtykietaPrzesuniecie_X & content.Substring(indexNextCmd)
        End If
    End Sub

    Public Sub UstawPrzesuniecieGLS()
        If Not String.IsNullOrEmpty(Konfiguracja.GLS_EtykietaPrzesuniecie_X) OrElse Not String.IsNullOrEmpty(Konfiguracja.GLS_EtykietaPrzesuniecie_Y) Then
            Dim indexLH As Integer = content.IndexOf("^LH")
            Dim indexNextCmd As Integer = indexLH + 3
            While content(indexNextCmd) <> "^" AndAlso content(indexNextCmd) <> "~"
                indexNextCmd = indexNextCmd + 1
            End While

            Dim wstawka As String = ""

            If Not String.IsNullOrEmpty(Konfiguracja.GLS_EtykietaPrzesuniecie_X) Then
                wstawka &= "^LS" & Konfiguracja.GLS_EtykietaPrzesuniecie_X
            End If

            If Not String.IsNullOrEmpty(Konfiguracja.GLS_EtykietaPrzesuniecie_Y) Then
                wstawka &= "^LT" & Konfiguracja.GLS_EtykietaPrzesuniecie_Y
            End If

            content = content.Substring(0, indexNextCmd) & wstawka & content.Substring(indexNextCmd)
        End If
    End Sub

    Public Sub WstawNrRefINrPaczkiDHL(paczki As IEnumerable(Of Paczka))
        Dim contentBuilder As New StringBuilder()
        'Dim paczkaWiodaca As Paczka = paczki.First(Function(p) String.IsNullOrEmpty(p.NrPaczkiWiodacej))
        Dim jakasPaczka As Paczka = paczki(0)
        Dim nrPaczkiWiodacej As String = jakasPaczka.NrPaczki

        If Not String.IsNullOrEmpty(jakasPaczka.NrPaczkiWiodacej) Then nrPaczkiWiodacej = jakasPaczka.NrPaczkiWiodacej

        Dim parts As String() = content.Split(New String() {jakasPaczka.NrRef & "/" & nrPaczkiWiodacej}, StringSplitOptions.None)

        contentBuilder.Append(parts(0))
        Dim i As Integer = 1

        For Each paczka As Paczka In paczki
            contentBuilder.Append(paczka.NrRef & "/" & paczka.NrPaczki)

            If i < parts.Length Then
                contentBuilder.Append(parts(i))
                i = i + 1
            End If
        Next

        content = contentBuilder.ToString()
    End Sub

    Public Sub WstawNrRefINrPaczkiDHL(paczka As Paczka)
        Dim nrPaczkiWiodacej As String = paczka.NrPaczki
        If Not String.IsNullOrEmpty(paczka.NrPaczkiWiodacej) Then nrPaczkiWiodacej = paczka.NrPaczkiWiodacej

        Dim parts As String() = content.Split(New String() {paczka.NrRef & "/" & nrPaczkiWiodacej}, StringSplitOptions.None)
        content = String.Join(paczka.NrRef & "/" & paczka.NrPaczki, parts)
    End Sub

    Public Sub WstawOpisZNumeracjaGLS(nr As Integer, ilosc As Integer)
        Dim wstawka As String = "paczka " & nr & "/" & ilosc
        Dim builder As New StringBuilder()
        Dim parts As String() = content.Split(New String() {"^FO24,453,^A0N,42^FR^FD"}, StringSplitOptions.None)

        builder.Append(parts(0))
        builder.Append("^FO24,453,^A0N,42^FR^FD")
        builder.Append(wstawka)

        If parts(1).StartsWith("^FS") Then
            builder.Append(parts(1))
        Else
            Dim rowText1 As String = parts(1).Split(New String() {"^FS"}, StringSplitOptions.None)(0)
            Dim rowText2 As String = parts(1).
                Split(New String() {"^FO24,502,^A0N,24^FR^FD"}, StringSplitOptions.None)(1).
                Split(New String() {"^FS"}, StringSplitOptions.None)(0)
            Dim rest As String = "^FO24,556,^A0N,24^FR^FD" & parts(1).Split(New String() {"^FO24,556,^A0N,24^FR^FD"}, StringSplitOptions.None)(1)

            builder.AppendLine("^FS")

            builder.Append("^FO24,502,^A0N,42^FR^FD")
            builder.Append(rowText1)
            builder.AppendLine("^FS")

            builder.Append("^FO24,551,^A0N,24^FR^FD")
            builder.Append(rowText2)
            builder.AppendLine("^FS")

            builder.Append(rest)
        End If

        content = builder.ToString()
    End Sub

    Public Function WyodrebnijEtykietyDHL() As Etykieta()
        Return content.Split(New String() {"^XZ"}, StringSplitOptions.RemoveEmptyEntries).
            Select(Function(elem) elem.Trim()).
            Where(Function(elem) Not String.IsNullOrEmpty(elem)).
            Select(Function(elem) New Etykieta() With {.content = elem & "^XZ"}).
            ToArray()
    End Function

    Public Function WyodrebnijEtykietyGLS() As Etykieta()
        Return content.Split(New String() {"~JO"}, StringSplitOptions.RemoveEmptyEntries).
            Select(Function(elem) elem.Trim()).
            Where(Function(elem) Not String.IsNullOrEmpty(elem)).
            Select(Function(elem) New Etykieta() With {.content = "~JO" & elem}).
            ToArray()
    End Function

    Public Function PobierzIdentyfikatorPaczkiGLS() As String
        Return content.
            Split(New String() {"^FO268,221,^BY3,2.0,125,^B2N,125,N,N^FR^FD"}, StringSplitOptions.None)(1).
            Split(New String() {"^FS"}, StringSplitOptions.None)(0)
    End Function

    Public Shared Function FromStrictContent(content As String) As Etykieta
        Return New Etykieta() With {.content = content}
    End Function

    Public Overrides Function ToString() As String
        Return content
    End Function
End Class
