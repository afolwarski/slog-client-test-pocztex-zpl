﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ImportAdresatowWzorcowaPaczkaForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.slogKodyUslugDodCheckedListBox = New System.Windows.Forms.CheckedListBox()
        Me.slogRodzajPaczkiComboBox = New System.Windows.Forms.ComboBox()
        Me.slogPobranieCheckBox = New System.Windows.Forms.CheckBox()
        Me.slogTypUslugiComboBox = New System.Windows.Forms.ComboBox()
        Me.slogSpedytorComboBox = New System.Windows.Forms.ComboBox()
        Me.slogKwotaPobraniaTextBox = New System.Windows.Forms.TextBox()
        Me.slogNrReferencyjnyTextBox = New System.Windows.Forms.TextBox()
        Me.PaczkiNumeryDataGridView = New System.Windows.Forms.DataGridView()
        Me.WyczyscButton = New System.Windows.Forms.Button()
        Me.ZapiszButton = New System.Windows.Forms.Button()
        Me.AnulujButton = New System.Windows.Forms.Button()
        Me.AdresaciDataGridView = New System.Windows.Forms.DataGridView()
        Me.Nazwa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Imie = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nazwisko = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Adres = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KodPocztowy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Miasto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Email = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Telefon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImportujListeAdresatowButton = New System.Windows.Forms.Button()
        Me.UsunButton = New System.Windows.Forms.Button()
        Me.DodajButton = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.wagaCalkowitaTextBox = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.NrPaczki = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Waga = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Wiodaca = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        CType(Me.PaczkiNumeryDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AdresaciDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(52, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Spedytor:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(44, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Typ usługi:"
        '
        'Label3
        '
        Me.Label3.AutoEllipsis = True
        Me.Label3.Location = New System.Drawing.Point(22, 66)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 33)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Kody usług dodatkowych:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(52, 148)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Pobranie:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(20, 171)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(84, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Kwota pobrania:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(27, 124)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(77, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Rodzaj paczki:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(268, 17)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(39, 13)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Nr ref.:"
        '
        'slogKodyUslugDodCheckedListBox
        '
        Me.slogKodyUslugDodCheckedListBox.CheckOnClick = True
        Me.slogKodyUslugDodCheckedListBox.FormattingEnabled = True
        Me.slogKodyUslugDodCheckedListBox.Location = New System.Drawing.Point(110, 66)
        Me.slogKodyUslugDodCheckedListBox.Name = "slogKodyUslugDodCheckedListBox"
        Me.slogKodyUslugDodCheckedListBox.Size = New System.Drawing.Size(124, 49)
        Me.slogKodyUslugDodCheckedListBox.TabIndex = 106
        '
        'slogRodzajPaczkiComboBox
        '
        Me.slogRodzajPaczkiComboBox.FormattingEnabled = True
        Me.slogRodzajPaczkiComboBox.Location = New System.Drawing.Point(110, 121)
        Me.slogRodzajPaczkiComboBox.Name = "slogRodzajPaczkiComboBox"
        Me.slogRodzajPaczkiComboBox.Size = New System.Drawing.Size(124, 21)
        Me.slogRodzajPaczkiComboBox.TabIndex = 105
        '
        'slogPobranieCheckBox
        '
        Me.slogPobranieCheckBox.AutoSize = True
        Me.slogPobranieCheckBox.Location = New System.Drawing.Point(110, 148)
        Me.slogPobranieCheckBox.Name = "slogPobranieCheckBox"
        Me.slogPobranieCheckBox.Size = New System.Drawing.Size(15, 14)
        Me.slogPobranieCheckBox.TabIndex = 104
        Me.slogPobranieCheckBox.UseVisualStyleBackColor = True
        '
        'slogTypUslugiComboBox
        '
        Me.slogTypUslugiComboBox.FormattingEnabled = True
        Me.slogTypUslugiComboBox.Location = New System.Drawing.Point(110, 40)
        Me.slogTypUslugiComboBox.Name = "slogTypUslugiComboBox"
        Me.slogTypUslugiComboBox.Size = New System.Drawing.Size(124, 21)
        Me.slogTypUslugiComboBox.TabIndex = 103
        '
        'slogSpedytorComboBox
        '
        Me.slogSpedytorComboBox.FormattingEnabled = True
        Me.slogSpedytorComboBox.Location = New System.Drawing.Point(110, 14)
        Me.slogSpedytorComboBox.Name = "slogSpedytorComboBox"
        Me.slogSpedytorComboBox.Size = New System.Drawing.Size(124, 21)
        Me.slogSpedytorComboBox.TabIndex = 102
        '
        'slogKwotaPobraniaTextBox
        '
        Me.slogKwotaPobraniaTextBox.Location = New System.Drawing.Point(110, 168)
        Me.slogKwotaPobraniaTextBox.Name = "slogKwotaPobraniaTextBox"
        Me.slogKwotaPobraniaTextBox.ReadOnly = True
        Me.slogKwotaPobraniaTextBox.Size = New System.Drawing.Size(124, 20)
        Me.slogKwotaPobraniaTextBox.TabIndex = 101
        '
        'slogNrReferencyjnyTextBox
        '
        Me.slogNrReferencyjnyTextBox.Location = New System.Drawing.Point(313, 14)
        Me.slogNrReferencyjnyTextBox.Name = "slogNrReferencyjnyTextBox"
        Me.slogNrReferencyjnyTextBox.Size = New System.Drawing.Size(133, 20)
        Me.slogNrReferencyjnyTextBox.TabIndex = 108
        '
        'PaczkiNumeryDataGridView
        '
        Me.PaczkiNumeryDataGridView.AllowUserToAddRows = False
        Me.PaczkiNumeryDataGridView.AllowUserToDeleteRows = False
        Me.PaczkiNumeryDataGridView.AllowUserToResizeRows = False
        Me.PaczkiNumeryDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.PaczkiNumeryDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.PaczkiNumeryDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NrPaczki, Me.Waga, Me.Wiodaca})
        Me.PaczkiNumeryDataGridView.Location = New System.Drawing.Point(490, 14)
        Me.PaczkiNumeryDataGridView.MultiSelect = False
        Me.PaczkiNumeryDataGridView.Name = "PaczkiNumeryDataGridView"
        Me.PaczkiNumeryDataGridView.RowHeadersVisible = False
        Me.PaczkiNumeryDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.PaczkiNumeryDataGridView.Size = New System.Drawing.Size(242, 108)
        Me.PaczkiNumeryDataGridView.TabIndex = 109
        '
        'WyczyscButton
        '
        Me.WyczyscButton.Location = New System.Drawing.Point(147, 10)
        Me.WyczyscButton.Name = "WyczyscButton"
        Me.WyczyscButton.Size = New System.Drawing.Size(75, 23)
        Me.WyczyscButton.TabIndex = 111
        Me.WyczyscButton.Text = "Wyczyść"
        Me.WyczyscButton.UseVisualStyleBackColor = True
        '
        'ZapiszButton
        '
        Me.ZapiszButton.Location = New System.Drawing.Point(682, 433)
        Me.ZapiszButton.Name = "ZapiszButton"
        Me.ZapiszButton.Size = New System.Drawing.Size(75, 23)
        Me.ZapiszButton.TabIndex = 111
        Me.ZapiszButton.Text = "Zapisz"
        Me.ZapiszButton.UseVisualStyleBackColor = True
        '
        'AnulujButton
        '
        Me.AnulujButton.Location = New System.Drawing.Point(763, 433)
        Me.AnulujButton.Name = "AnulujButton"
        Me.AnulujButton.Size = New System.Drawing.Size(75, 23)
        Me.AnulujButton.TabIndex = 111
        Me.AnulujButton.Text = "Anuluj"
        Me.AnulujButton.UseVisualStyleBackColor = True
        '
        'AdresaciDataGridView
        '
        Me.AdresaciDataGridView.AllowUserToAddRows = False
        Me.AdresaciDataGridView.AllowUserToDeleteRows = False
        Me.AdresaciDataGridView.AllowUserToResizeRows = False
        Me.AdresaciDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.AdresaciDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Nazwa, Me.Imie, Me.Nazwisko, Me.Adres, Me.KodPocztowy, Me.Miasto, Me.Email, Me.Telefon})
        Me.AdresaciDataGridView.Location = New System.Drawing.Point(10, 39)
        Me.AdresaciDataGridView.Name = "AdresaciDataGridView"
        Me.AdresaciDataGridView.RowHeadersVisible = False
        Me.AdresaciDataGridView.Size = New System.Drawing.Size(802, 150)
        Me.AdresaciDataGridView.TabIndex = 112
        '
        'Nazwa
        '
        Me.Nazwa.HeaderText = "Nazwa"
        Me.Nazwa.Name = "Nazwa"
        '
        'Imie
        '
        Me.Imie.HeaderText = "Imię"
        Me.Imie.Name = "Imie"
        '
        'Nazwisko
        '
        Me.Nazwisko.HeaderText = "Nazwisko"
        Me.Nazwisko.Name = "Nazwisko"
        '
        'Adres
        '
        Me.Adres.HeaderText = "Adres"
        Me.Adres.Name = "Adres"
        '
        'KodPocztowy
        '
        Me.KodPocztowy.HeaderText = "Kod pocztowy"
        Me.KodPocztowy.Name = "KodPocztowy"
        '
        'Miasto
        '
        Me.Miasto.HeaderText = "Miasto"
        Me.Miasto.Name = "Miasto"
        '
        'Email
        '
        Me.Email.HeaderText = "Email"
        Me.Email.Name = "Email"
        '
        'Telefon
        '
        Me.Telefon.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Telefon.HeaderText = "Telefon"
        Me.Telefon.Name = "Telefon"
        '
        'ImportujListeAdresatowButton
        '
        Me.ImportujListeAdresatowButton.Location = New System.Drawing.Point(10, 10)
        Me.ImportujListeAdresatowButton.Name = "ImportujListeAdresatowButton"
        Me.ImportujListeAdresatowButton.Size = New System.Drawing.Size(131, 23)
        Me.ImportujListeAdresatowButton.TabIndex = 111
        Me.ImportujListeAdresatowButton.Text = "Importuj listę adresatów"
        Me.ImportujListeAdresatowButton.UseVisualStyleBackColor = True
        '
        'UsunButton
        '
        Me.UsunButton.BackgroundImage = Global.sLog_Client.My.Resources.Resources.remove
        Me.UsunButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.UsunButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.UsunButton.Location = New System.Drawing.Point(700, 128)
        Me.UsunButton.Name = "UsunButton"
        Me.UsunButton.Size = New System.Drawing.Size(32, 32)
        Me.UsunButton.TabIndex = 110
        Me.UsunButton.UseVisualStyleBackColor = True
        '
        'DodajButton
        '
        Me.DodajButton.BackgroundImage = Global.sLog_Client.My.Resources.Resources.add
        Me.DodajButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.DodajButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.DodajButton.Location = New System.Drawing.Point(490, 128)
        Me.DodajButton.Name = "DodajButton"
        Me.DodajButton.Size = New System.Drawing.Size(32, 32)
        Me.DodajButton.TabIndex = 110
        Me.DodajButton.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.wagaCalkowitaTextBox)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.UsunButton)
        Me.Panel1.Controls.Add(Me.DodajButton)
        Me.Panel1.Controls.Add(Me.PaczkiNumeryDataGridView)
        Me.Panel1.Controls.Add(Me.slogNrReferencyjnyTextBox)
        Me.Panel1.Controls.Add(Me.slogKodyUslugDodCheckedListBox)
        Me.Panel1.Controls.Add(Me.slogRodzajPaczkiComboBox)
        Me.Panel1.Controls.Add(Me.slogPobranieCheckBox)
        Me.Panel1.Controls.Add(Me.slogTypUslugiComboBox)
        Me.Panel1.Controls.Add(Me.slogSpedytorComboBox)
        Me.Panel1.Controls.Add(Me.slogKwotaPobraniaTextBox)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(826, 200)
        Me.Panel1.TabIndex = 113
        '
        'wagaCalkowitaTextBox
        '
        Me.wagaCalkowitaTextBox.Location = New System.Drawing.Point(603, 168)
        Me.wagaCalkowitaTextBox.Name = "wagaCalkowitaTextBox"
        Me.wagaCalkowitaTextBox.ReadOnly = True
        Me.wagaCalkowitaTextBox.Size = New System.Drawing.Size(87, 20)
        Me.wagaCalkowitaTextBox.TabIndex = 112
        Me.wagaCalkowitaTextBox.Text = "0"
        Me.wagaCalkowitaTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(487, 171)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(110, 13)
        Me.Label9.TabIndex = 111
        Me.Label9.Text = "Waga całkowita [kg]:"
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.AdresaciDataGridView)
        Me.Panel2.Controls.Add(Me.ImportujListeAdresatowButton)
        Me.Panel2.Controls.Add(Me.WyczyscButton)
        Me.Panel2.Location = New System.Drawing.Point(12, 218)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(826, 204)
        Me.Panel2.TabIndex = 114
        '
        'NrPaczki
        '
        Me.NrPaczki.HeaderText = "Nr paczki"
        Me.NrPaczki.Name = "NrPaczki"
        Me.NrPaczki.Width = 80
        '
        'Waga
        '
        Me.Waga.FillWeight = 136.7521!
        Me.Waga.HeaderText = "Waga [kg]"
        Me.Waga.Name = "Waga"
        Me.Waga.Width = 80
        '
        'Wiodaca
        '
        Me.Wiodaca.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Wiodaca.FillWeight = 63.24787!
        Me.Wiodaca.HeaderText = "Wiodąca"
        Me.Wiodaca.Name = "Wiodaca"
        Me.Wiodaca.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Wiodaca.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'ImportAdresatowWzorcowaPaczkaForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(850, 468)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.AnulujButton)
        Me.Controls.Add(Me.ZapiszButton)
        Me.Name = "ImportAdresatowWzorcowaPaczkaForm"
        Me.Text = "Import adresatów do wzorcowej paczki"
        CType(Me.PaczkiNumeryDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AdresaciDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents slogKodyUslugDodCheckedListBox As System.Windows.Forms.CheckedListBox
    Friend WithEvents slogRodzajPaczkiComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents slogPobranieCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents slogTypUslugiComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents slogSpedytorComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents slogKwotaPobraniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogNrReferencyjnyTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PaczkiNumeryDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DodajButton As System.Windows.Forms.Button
    Friend WithEvents UsunButton As System.Windows.Forms.Button
    Friend WithEvents WyczyscButton As System.Windows.Forms.Button
    Friend WithEvents ZapiszButton As System.Windows.Forms.Button
    Friend WithEvents AnulujButton As System.Windows.Forms.Button
    Friend WithEvents AdresaciDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents ImportujListeAdresatowButton As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents wagaCalkowitaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Nazwa As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Imie As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nazwisko As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Adres As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KodPocztowy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Miasto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Email As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Telefon As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NrPaczki As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Waga As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Wiodaca As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class
