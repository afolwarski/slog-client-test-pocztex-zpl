﻿Imports System.Data.SqlClient
Imports System.Threading

Public Class WysylkaMasowaForm
    Private wysylkaWToku As Boolean = False
    Private paczki As PaczkaExtended() = Nothing
    Private stanowisko As String = Nothing

    Private Sub rozpocznijAnulujButton_Click(sender As System.Object, e As System.EventArgs) Handles rozpocznijAnulujButton.Click
        If Not wysylkaWToku Then
            If String.IsNullOrEmpty(nrRefTextBox.Text) Then
                MessageBox.Show("Podaj numer dostawy")
                Exit Sub
            End If

            paczki = PaczkaExtended.PobierzZBazy(nrRefTextBox.Text)

            If paczki.Length = 0 Then
                MessageBox.Show("Nie znaleziono oczekujących paczek dla podanego numeru dostawy.")
                Exit Sub
            End If

            progressMax = paczki.Count()
            progressCounter = 0
            wyslaneGrupyPaczek.Clear()
            paczkiBledne.Clear()
            wysylkaProgressBar.Value = 0

            stanowisko = CType(stanowiskoComboBox.SelectedItem, String)

            wysylkaWToku = True
            rozpocznijAnulujButton.Text = "Zatrzymaj"
            wysylkaBackgroundWorker.RunWorkerAsync()


        Else
            wysylkaWToku = False

            If progressCounter < progressMax Then
                KontynuujButton.Enabled = False
            Else
                rozpocznijAnulujButton.Enabled = False
            End If

            If wysylkaBackgroundWorker.IsBusy Then
                wysylkaBackgroundWorker.CancelAsync()
            Else
                rozpocznijAnulujButton.Text = "Rozpocznij"

                If paczkiBledne.Count > 0 Then
                    Dim raportBledowForm As New WysylkaMasowaRaportForm(paczkiBledne)
                    raportBledowForm.ShowDialog()
                End If
            End If
        End If
    End Sub

    Private progressMax As Integer = 0
    Private progressCounter As Integer = 0

    Private wyslaneGrupyPaczek As New HashSet(Of String)
    Private paczkiBledne As New List(Of Integer)

    Private Sub wysylkaBackgroundWorker_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles wysylkaBackgroundWorker.DoWork
        Konfiguracja.Odswiez(stanowisko)

        Try
            Dim pierwszaGrupa As Boolean = True
            Dim progressPartCounter As Integer = 0

            Dim paczkiPogrupowane As IEnumerable(Of IGrouping(Of String, PaczkaExtended)) = paczki.
                OrderBy(Function(p)
                            Dim groupKey As String

                            If Not p.NrPaczkiWiodacej Is Nothing Then
                                groupKey = p.NrRef & "/" & p.NrPaczkiWiodacej.PadLeft(5, "0")
                            ElseIf Not p.NrPaczki Is Nothing Then
                                groupKey = p.NrRef & "/" & p.NrPaczki.PadLeft(5, "0")
                            Else
                                groupKey = Guid.NewGuid().ToString()
                            End If

                            Return groupKey & "/" & p.NrPaczki.PadLeft(5, "0")
                        End Function).
                GroupBy(Function(p)
                            If Not p.NrPaczkiWiodacej Is Nothing Then
                                Return p.NrRef & "/" & p.NrPaczkiWiodacej
                            ElseIf Not p.NrPaczki Is Nothing Then
                                Return p.NrRef & "/" & p.NrPaczki
                            Else
                                Return Guid.NewGuid().ToString()
                            End If
                        End Function).ToArray()

            wysylkaBackgroundWorker.ReportProgress(progressCounter * 100 / progressMax)

            For Each grupaPaczek As IGrouping(Of String, PaczkaExtended) In paczkiPogrupowane
                If wyslaneGrupyPaczek.Contains(grupaPaczek.Key) Then
                    Continue For
                End If

                Try
                    progressPartCounter += grupaPaczek.Count()
                    If Not pierwszaGrupa AndAlso progressPartCounter > Konfiguracja.WysylkaMasowaMinimalnaLiczbaPaczek Then
                        Exit For
                    End If

                    Dim anuluj As Boolean = Not grupaPaczek.FirstOrDefault(Function(p) p.Anuluj) Is Nothing
                    Dim czyNadano As Boolean = Not grupaPaczek.FirstOrDefault(Function(p) p.CzyNadana) Is Nothing

                    If anuluj Then
                        Spedytor.Instancja(grupaPaczek).AnulujPrzesylke(grupaPaczek)
                    ElseIf Not czyNadano Then
                        Spedytor.Instancja(grupaPaczek).UtworzPrzesylke(grupaPaczek)
                    Else
                        Spedytor.Instancja(grupaPaczek).PobierzEtykiety(grupaPaczek)
                    End If

                    If Not anuluj Then
                        Dim paczka As PaczkaExtended = grupaPaczek(0)

                        If paczka.CzyNadana AndAlso (Not paczka.Etykieta Is Nothing Or Not paczka.EtykietaInPost Is Nothing) Then
                            For Each p As Paczka In grupaPaczek
                                Drukarka.WydrukujEtykiete(p)
                            Next
                        End If

                        If paczka.Zrealizowane Then
                            For i As Integer = 1 To grupaPaczek.Count()
                                Thread.Sleep(Konfiguracja.DrukowanieOpoznienie_milisek)
                                progressCounter += 1
                                wysylkaBackgroundWorker.ReportProgress(progressCounter * 100 / progressMax)
                            Next
                        Else
                            paczkiBledne.AddRange(grupaPaczek.Select(Function(p) p.Id))
                        End If
                    End If
                Catch ex As Exception
                    Log.WriteError(ex.ToString())
                    Log.WriteToDB(grupaPaczek.FirstOrDefault(Function(p) String.IsNullOrEmpty(p.NrPaczkiWiodacej)), 5, ex.ToString())
                    paczkiBledne.AddRange(grupaPaczek.Select(Function(p) p.Id))
                End Try

                wyslaneGrupyPaczek.Add(grupaPaczek.Key)
                pierwszaGrupa = False
                progressCounter += grupaPaczek.Count()
                wysylkaBackgroundWorker.ReportProgress(progressCounter * 100 / progressMax)

                If wysylkaBackgroundWorker.CancellationPending Then
                    e.Cancel = True
                    Exit For
                End If
            Next
        Catch ex As Exception
            Log.WriteError(ex.ToString())
            Log.WriteToDB(Nothing, 5, ex.ToString())
        End Try
    End Sub

    Private Sub wysylkaBackgroundWorker_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles wysylkaBackgroundWorker.ProgressChanged
        postepLabel.Text = String.Format("Postęp: {0:###}%", e.ProgressPercentage)
        wysylkaProgressBar.Value = e.ProgressPercentage
    End Sub

    Private Sub wysylkaBackgroundWorker_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles wysylkaBackgroundWorker.RunWorkerCompleted
        If e.Cancelled OrElse progressCounter = progressMax Then
            wysylkaWToku = False
            rozpocznijAnulujButton.Text = "Rozpocznij"
            rozpocznijAnulujButton.Enabled = True

            If paczkiBledne.Count > 0 Then
                Dim raportBledowForm As New WysylkaMasowaRaportForm(paczkiBledne)
                raportBledowForm.ShowDialog()
            Else
                MessageBox.Show("Operacja zakończona sukcesem. Przetworzono " & progressCounter & " paczek.")
            End If
        Else
            KontynuujButton.Enabled = True
        End If
    End Sub

    Private Sub WysylkaMasowaForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim stanowiskaList As New List(Of String)

        Using conn As New SqlConnection(My.Settings.SLOGConnectionString)
            conn.Open()

            Using cmd As New SqlCommand()
                cmd.Connection = conn
                cmd.CommandText = "SELECT DISTINCT Stanowisko FROM stanowiska"

                Using reader As SqlDataReader = cmd.ExecuteReader()
                    While reader.Read()
                        If Not IsDBNull(reader("Stanowisko")) AndAlso Not String.IsNullOrEmpty(reader("Stanowisko").ToString()) Then
                            stanowiskaList.Add(reader("Stanowisko").ToString())
                        End If
                    End While
                End Using
            End Using
        End Using

        stanowiskoComboBox.DataSource = stanowiskaList

        If stanowiskaList.Contains(Environment.MachineName) Then
            stanowiskoComboBox.SelectedItem = Environment.MachineName
        Else
            stanowiskoComboBox.SelectedIndex = 0
        End If
    End Sub

    Private Sub KontynuujButton_Click(sender As System.Object, e As System.EventArgs) Handles KontynuujButton.Click
        KontynuujButton.Enabled = False
        wysylkaBackgroundWorker.RunWorkerAsync()
    End Sub
End Class