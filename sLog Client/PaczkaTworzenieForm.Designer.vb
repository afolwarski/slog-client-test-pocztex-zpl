﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PaczkaTworzenieForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.slogAnulujButton = New System.Windows.Forms.Button()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.slogPaletaSzerokoscTextBox = New System.Windows.Forms.TextBox()
        Me.slogOdbiorcaMailTextBox = New System.Windows.Forms.TextBox()
        Me.slogPaletaDlugoscTextBox = New System.Windows.Forms.TextBox()
        Me.slogOdbiorcaTelefonTextBox = New System.Windows.Forms.TextBox()
        Me.slogOdbiorcaKodPocztowyTextBox = New System.Windows.Forms.TextBox()
        Me.slogOdbiorcaMiastoTextBox = New System.Windows.Forms.TextBox()
        Me.slogKwotaPobraniaTextBox = New System.Windows.Forms.TextBox()
        Me.slogOdbiorcaAdresTextBox = New System.Windows.Forms.TextBox()
        Me.slogOdbiorcaOsobaTextBox = New System.Windows.Forms.TextBox()
        Me.slogOdbiorcaNazwaTextBox = New System.Windows.Forms.TextBox()
        Me.slogNrPaczkiWiodacejTextBox = New System.Windows.Forms.TextBox()
        Me.slogNrPaczkiTextBox = New System.Windows.Forms.TextBox()
        Me.slogNrReferencyjnyTextBox = New System.Windows.Forms.TextBox()
        Me.slogGabarytTextBox = New System.Windows.Forms.TextBox()
        Me.slogWagaTextBox = New System.Windows.Forms.TextBox()
        Me.slogPaletaWysokoscTextBox = New System.Windows.Forms.TextBox()
        Me.slogZapiszButton = New System.Windows.Forms.Button()
        Me.slogSpedytorComboBox = New System.Windows.Forms.ComboBox()
        Me.slogTypUslugiComboBox = New System.Windows.Forms.ComboBox()
        Me.slogPobranieCheckBox = New System.Windows.Forms.CheckBox()
        Me.slogRodzajPaczkiComboBox = New System.Windows.Forms.ComboBox()
        Me.slogRodzajPaletyComboBox = New System.Windows.Forms.ComboBox()
        Me.slogPaczkaNiestandardowaCheckBox = New System.Windows.Forms.CheckBox()
        Me.slogPlatnoscZaPrzesylkeComboBox = New System.Windows.Forms.ComboBox()
        Me.slogKodyUslugDodCheckedListBox = New System.Windows.Forms.CheckedListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.slogDataNadaniaDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.btnPobierzDaneZref = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'slogAnulujButton
        '
        Me.slogAnulujButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.slogAnulujButton.Location = New System.Drawing.Point(536, 409)
        Me.slogAnulujButton.Name = "slogAnulujButton"
        Me.slogAnulujButton.Size = New System.Drawing.Size(130, 34)
        Me.slogAnulujButton.TabIndex = 27
        Me.slogAnulujButton.Text = "Anuluj"
        Me.slogAnulujButton.UseVisualStyleBackColor = True
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(42, 271)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(96, 13)
        Me.Label16.TabIndex = 64
        Me.Label16.Text = "Paleta - Szerokość"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(374, 297)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(78, 13)
        Me.Label31.TabIndex = 62
        Me.Label31.Text = "Odbiorca - Mail"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(42, 245)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(87, 13)
        Me.Label15.TabIndex = 60
        Me.Label15.Text = "Paleta - Długość"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(374, 271)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(95, 13)
        Me.Label30.TabIndex = 61
        Me.Label30.Text = "Odbiorca - Telefon"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(42, 219)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(71, 13)
        Me.Label14.TabIndex = 68
        Me.Label14.Text = "Rodzaj palety"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(374, 245)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(126, 13)
        Me.Label29.TabIndex = 69
        Me.Label29.Text = "Odbiorca - Kod pocztowy"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(42, 193)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(74, 13)
        Me.Label13.TabIndex = 67
        Me.Label13.Text = "Rodzaj paczki"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(374, 219)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(90, 13)
        Me.Label28.TabIndex = 66
        Me.Label28.Text = "Odbiorca - Miasto"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(42, 167)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(81, 13)
        Me.Label12.TabIndex = 59
        Me.Label12.Text = "Kwota pobrania"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(374, 193)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(86, 13)
        Me.Label27.TabIndex = 53
        Me.Label27.Text = "Odbiorca - Adres"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(42, 141)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(49, 13)
        Me.Label11.TabIndex = 51
        Me.Label11.Text = "Pobranie"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(374, 167)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(90, 13)
        Me.Label26.TabIndex = 50
        Me.Label26.Text = "Odbiorca - Osoba"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(42, 89)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(128, 13)
        Me.Label10.TabIndex = 57
        Me.Label10.Text = "Kody usług dodatkowych"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(374, 141)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(92, 13)
        Me.Label25.TabIndex = 56
        Me.Label25.Text = "Odbiorca - Nazwa"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(42, 63)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(57, 13)
        Me.Label9.TabIndex = 54
        Me.Label9.Text = "Typ usługi"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(374, 115)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(97, 13)
        Me.Label24.TabIndex = 84
        Me.Label24.Text = "Nr paczki wiodącej"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(374, 89)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(52, 13)
        Me.Label23.TabIndex = 81
        Me.Label23.Text = "Nr paczki"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(374, 63)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(78, 13)
        Me.Label22.TabIndex = 90
        Me.Label22.Text = "Nr referencyjny"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(42, 375)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(44, 13)
        Me.Label20.TabIndex = 74
        Me.Label20.Text = "Gabaryt"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(42, 37)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 13)
        Me.Label4.TabIndex = 72
        Me.Label4.Text = "Spedytor"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(374, 323)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(113, 13)
        Me.Label35.TabIndex = 70
        Me.Label35.Text = "Płatność za przesyłkę"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(42, 349)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(36, 13)
        Me.Label19.TabIndex = 71
        Me.Label19.Text = "Waga"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(42, 323)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(121, 13)
        Me.Label18.TabIndex = 77
        Me.Label18.Text = "Paczka niestandardowa"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(42, 297)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(96, 13)
        Me.Label17.TabIndex = 48
        Me.Label17.Text = "Paleta - Wysokość"
        '
        'slogPaletaSzerokoscTextBox
        '
        Me.slogPaletaSzerokoscTextBox.Location = New System.Drawing.Point(179, 268)
        Me.slogPaletaSzerokoscTextBox.Name = "slogPaletaSzerokoscTextBox"
        Me.slogPaletaSzerokoscTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogPaletaSzerokoscTextBox.TabIndex = 9
        '
        'slogOdbiorcaMailTextBox
        '
        Me.slogOdbiorcaMailTextBox.Location = New System.Drawing.Point(511, 294)
        Me.slogOdbiorcaMailTextBox.MaxLength = 100
        Me.slogOdbiorcaMailTextBox.Name = "slogOdbiorcaMailTextBox"
        Me.slogOdbiorcaMailTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogOdbiorcaMailTextBox.TabIndex = 24
        '
        'slogPaletaDlugoscTextBox
        '
        Me.slogPaletaDlugoscTextBox.Location = New System.Drawing.Point(179, 242)
        Me.slogPaletaDlugoscTextBox.Name = "slogPaletaDlugoscTextBox"
        Me.slogPaletaDlugoscTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogPaletaDlugoscTextBox.TabIndex = 8
        '
        'slogOdbiorcaTelefonTextBox
        '
        Me.slogOdbiorcaTelefonTextBox.Location = New System.Drawing.Point(511, 268)
        Me.slogOdbiorcaTelefonTextBox.Name = "slogOdbiorcaTelefonTextBox"
        Me.slogOdbiorcaTelefonTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogOdbiorcaTelefonTextBox.TabIndex = 23
        '
        'slogOdbiorcaKodPocztowyTextBox
        '
        Me.slogOdbiorcaKodPocztowyTextBox.Location = New System.Drawing.Point(511, 242)
        Me.slogOdbiorcaKodPocztowyTextBox.Name = "slogOdbiorcaKodPocztowyTextBox"
        Me.slogOdbiorcaKodPocztowyTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogOdbiorcaKodPocztowyTextBox.TabIndex = 22
        '
        'slogOdbiorcaMiastoTextBox
        '
        Me.slogOdbiorcaMiastoTextBox.Location = New System.Drawing.Point(511, 216)
        Me.slogOdbiorcaMiastoTextBox.Name = "slogOdbiorcaMiastoTextBox"
        Me.slogOdbiorcaMiastoTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogOdbiorcaMiastoTextBox.TabIndex = 21
        '
        'slogKwotaPobraniaTextBox
        '
        Me.slogKwotaPobraniaTextBox.Location = New System.Drawing.Point(179, 164)
        Me.slogKwotaPobraniaTextBox.Name = "slogKwotaPobraniaTextBox"
        Me.slogKwotaPobraniaTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogKwotaPobraniaTextBox.TabIndex = 5
        '
        'slogOdbiorcaAdresTextBox
        '
        Me.slogOdbiorcaAdresTextBox.Location = New System.Drawing.Point(511, 190)
        Me.slogOdbiorcaAdresTextBox.Name = "slogOdbiorcaAdresTextBox"
        Me.slogOdbiorcaAdresTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogOdbiorcaAdresTextBox.TabIndex = 20
        '
        'slogOdbiorcaOsobaTextBox
        '
        Me.slogOdbiorcaOsobaTextBox.Location = New System.Drawing.Point(511, 164)
        Me.slogOdbiorcaOsobaTextBox.Name = "slogOdbiorcaOsobaTextBox"
        Me.slogOdbiorcaOsobaTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogOdbiorcaOsobaTextBox.TabIndex = 19
        '
        'slogOdbiorcaNazwaTextBox
        '
        Me.slogOdbiorcaNazwaTextBox.Location = New System.Drawing.Point(511, 138)
        Me.slogOdbiorcaNazwaTextBox.Name = "slogOdbiorcaNazwaTextBox"
        Me.slogOdbiorcaNazwaTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogOdbiorcaNazwaTextBox.TabIndex = 18
        '
        'slogNrPaczkiWiodacejTextBox
        '
        Me.slogNrPaczkiWiodacejTextBox.Location = New System.Drawing.Point(511, 112)
        Me.slogNrPaczkiWiodacejTextBox.Name = "slogNrPaczkiWiodacejTextBox"
        Me.slogNrPaczkiWiodacejTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogNrPaczkiWiodacejTextBox.TabIndex = 17
        '
        'slogNrPaczkiTextBox
        '
        Me.slogNrPaczkiTextBox.Location = New System.Drawing.Point(511, 86)
        Me.slogNrPaczkiTextBox.Name = "slogNrPaczkiTextBox"
        Me.slogNrPaczkiTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogNrPaczkiTextBox.TabIndex = 16
        '
        'slogNrReferencyjnyTextBox
        '
        Me.slogNrReferencyjnyTextBox.Location = New System.Drawing.Point(511, 60)
        Me.slogNrReferencyjnyTextBox.Name = "slogNrReferencyjnyTextBox"
        Me.slogNrReferencyjnyTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogNrReferencyjnyTextBox.TabIndex = 15
        '
        'slogGabarytTextBox
        '
        Me.slogGabarytTextBox.Location = New System.Drawing.Point(179, 372)
        Me.slogGabarytTextBox.Name = "slogGabarytTextBox"
        Me.slogGabarytTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogGabarytTextBox.TabIndex = 13
        '
        'slogWagaTextBox
        '
        Me.slogWagaTextBox.Location = New System.Drawing.Point(179, 346)
        Me.slogWagaTextBox.Name = "slogWagaTextBox"
        Me.slogWagaTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogWagaTextBox.TabIndex = 12
        '
        'slogPaletaWysokoscTextBox
        '
        Me.slogPaletaWysokoscTextBox.Location = New System.Drawing.Point(179, 294)
        Me.slogPaletaWysokoscTextBox.Name = "slogPaletaWysokoscTextBox"
        Me.slogPaletaWysokoscTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogPaletaWysokoscTextBox.TabIndex = 10
        '
        'slogZapiszButton
        '
        Me.slogZapiszButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.slogZapiszButton.Location = New System.Drawing.Point(400, 409)
        Me.slogZapiszButton.Name = "slogZapiszButton"
        Me.slogZapiszButton.Size = New System.Drawing.Size(130, 34)
        Me.slogZapiszButton.TabIndex = 26
        Me.slogZapiszButton.Text = "Zapisz"
        Me.slogZapiszButton.UseVisualStyleBackColor = True
        '
        'slogSpedytorComboBox
        '
        Me.slogSpedytorComboBox.FormattingEnabled = True
        Me.slogSpedytorComboBox.Location = New System.Drawing.Point(179, 34)
        Me.slogSpedytorComboBox.Name = "slogSpedytorComboBox"
        Me.slogSpedytorComboBox.Size = New System.Drawing.Size(154, 21)
        Me.slogSpedytorComboBox.TabIndex = 1
        '
        'slogTypUslugiComboBox
        '
        Me.slogTypUslugiComboBox.FormattingEnabled = True
        Me.slogTypUslugiComboBox.Location = New System.Drawing.Point(179, 60)
        Me.slogTypUslugiComboBox.Name = "slogTypUslugiComboBox"
        Me.slogTypUslugiComboBox.Size = New System.Drawing.Size(154, 21)
        Me.slogTypUslugiComboBox.TabIndex = 2
        '
        'slogPobranieCheckBox
        '
        Me.slogPobranieCheckBox.AutoSize = True
        Me.slogPobranieCheckBox.Location = New System.Drawing.Point(179, 141)
        Me.slogPobranieCheckBox.Name = "slogPobranieCheckBox"
        Me.slogPobranieCheckBox.Size = New System.Drawing.Size(15, 14)
        Me.slogPobranieCheckBox.TabIndex = 4
        Me.slogPobranieCheckBox.UseVisualStyleBackColor = True
        '
        'slogRodzajPaczkiComboBox
        '
        Me.slogRodzajPaczkiComboBox.FormattingEnabled = True
        Me.slogRodzajPaczkiComboBox.Location = New System.Drawing.Point(179, 190)
        Me.slogRodzajPaczkiComboBox.Name = "slogRodzajPaczkiComboBox"
        Me.slogRodzajPaczkiComboBox.Size = New System.Drawing.Size(154, 21)
        Me.slogRodzajPaczkiComboBox.TabIndex = 6
        '
        'slogRodzajPaletyComboBox
        '
        Me.slogRodzajPaletyComboBox.FormattingEnabled = True
        Me.slogRodzajPaletyComboBox.Location = New System.Drawing.Point(179, 216)
        Me.slogRodzajPaletyComboBox.Name = "slogRodzajPaletyComboBox"
        Me.slogRodzajPaletyComboBox.Size = New System.Drawing.Size(154, 21)
        Me.slogRodzajPaletyComboBox.TabIndex = 7
        '
        'slogPaczkaNiestandardowaCheckBox
        '
        Me.slogPaczkaNiestandardowaCheckBox.AutoSize = True
        Me.slogPaczkaNiestandardowaCheckBox.Location = New System.Drawing.Point(179, 323)
        Me.slogPaczkaNiestandardowaCheckBox.Name = "slogPaczkaNiestandardowaCheckBox"
        Me.slogPaczkaNiestandardowaCheckBox.Size = New System.Drawing.Size(15, 14)
        Me.slogPaczkaNiestandardowaCheckBox.TabIndex = 11
        Me.slogPaczkaNiestandardowaCheckBox.UseVisualStyleBackColor = True
        '
        'slogPlatnoscZaPrzesylkeComboBox
        '
        Me.slogPlatnoscZaPrzesylkeComboBox.FormattingEnabled = True
        Me.slogPlatnoscZaPrzesylkeComboBox.Location = New System.Drawing.Point(511, 320)
        Me.slogPlatnoscZaPrzesylkeComboBox.Name = "slogPlatnoscZaPrzesylkeComboBox"
        Me.slogPlatnoscZaPrzesylkeComboBox.Size = New System.Drawing.Size(155, 21)
        Me.slogPlatnoscZaPrzesylkeComboBox.TabIndex = 25
        '
        'slogKodyUslugDodCheckedListBox
        '
        Me.slogKodyUslugDodCheckedListBox.CheckOnClick = True
        Me.slogKodyUslugDodCheckedListBox.FormattingEnabled = True
        Me.slogKodyUslugDodCheckedListBox.Location = New System.Drawing.Point(179, 86)
        Me.slogKodyUslugDodCheckedListBox.Name = "slogKodyUslugDodCheckedListBox"
        Me.slogKodyUslugDodCheckedListBox.Size = New System.Drawing.Size(154, 49)
        Me.slogKodyUslugDodCheckedListBox.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(374, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 13)
        Me.Label1.TabIndex = 90
        Me.Label1.Text = "Data nadania"
        '
        'slogDataNadaniaDateTimePicker
        '
        Me.slogDataNadaniaDateTimePicker.Location = New System.Drawing.Point(511, 34)
        Me.slogDataNadaniaDateTimePicker.Name = "slogDataNadaniaDateTimePicker"
        Me.slogDataNadaniaDateTimePicker.Size = New System.Drawing.Size(155, 20)
        Me.slogDataNadaniaDateTimePicker.TabIndex = 14
        Me.slogDataNadaniaDateTimePicker.Value = New Date(2014, 6, 13, 15, 16, 52, 0)
        '
        'btnPobierzDaneZref
        '
        Me.btnPobierzDaneZref.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.btnPobierzDaneZref.Location = New System.Drawing.Point(45, 409)
        Me.btnPobierzDaneZref.Name = "btnPobierzDaneZref"
        Me.btnPobierzDaneZref.Size = New System.Drawing.Size(149, 34)
        Me.btnPobierzDaneZref.TabIndex = 92
        Me.btnPobierzDaneZref.Text = "Pobierz dane z ref."
        Me.btnPobierzDaneZref.UseVisualStyleBackColor = True
        '
        'PaczkaTworzenieForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(706, 458)
        Me.Controls.Add(Me.btnPobierzDaneZref)
        Me.Controls.Add(Me.slogDataNadaniaDateTimePicker)
        Me.Controls.Add(Me.slogKodyUslugDodCheckedListBox)
        Me.Controls.Add(Me.slogPlatnoscZaPrzesylkeComboBox)
        Me.Controls.Add(Me.slogPaczkaNiestandardowaCheckBox)
        Me.Controls.Add(Me.slogRodzajPaletyComboBox)
        Me.Controls.Add(Me.slogRodzajPaczkiComboBox)
        Me.Controls.Add(Me.slogPobranieCheckBox)
        Me.Controls.Add(Me.slogTypUslugiComboBox)
        Me.Controls.Add(Me.slogSpedytorComboBox)
        Me.Controls.Add(Me.slogZapiszButton)
        Me.Controls.Add(Me.slogAnulujButton)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label35)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.slogPaletaSzerokoscTextBox)
        Me.Controls.Add(Me.slogOdbiorcaMailTextBox)
        Me.Controls.Add(Me.slogPaletaDlugoscTextBox)
        Me.Controls.Add(Me.slogOdbiorcaTelefonTextBox)
        Me.Controls.Add(Me.slogOdbiorcaKodPocztowyTextBox)
        Me.Controls.Add(Me.slogOdbiorcaMiastoTextBox)
        Me.Controls.Add(Me.slogKwotaPobraniaTextBox)
        Me.Controls.Add(Me.slogOdbiorcaAdresTextBox)
        Me.Controls.Add(Me.slogOdbiorcaOsobaTextBox)
        Me.Controls.Add(Me.slogOdbiorcaNazwaTextBox)
        Me.Controls.Add(Me.slogNrPaczkiWiodacejTextBox)
        Me.Controls.Add(Me.slogNrPaczkiTextBox)
        Me.Controls.Add(Me.slogNrReferencyjnyTextBox)
        Me.Controls.Add(Me.slogGabarytTextBox)
        Me.Controls.Add(Me.slogWagaTextBox)
        Me.Controls.Add(Me.slogPaletaWysokoscTextBox)
        Me.Name = "PaczkaTworzenieForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Tworzenie paczki"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents slogAnulujButton As System.Windows.Forms.Button
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents slogPaletaSzerokoscTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogOdbiorcaMailTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogPaletaDlugoscTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogOdbiorcaTelefonTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogOdbiorcaKodPocztowyTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogOdbiorcaMiastoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogKwotaPobraniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogOdbiorcaAdresTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogOdbiorcaOsobaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogOdbiorcaNazwaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogNrPaczkiWiodacejTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogNrPaczkiTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogNrReferencyjnyTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogGabarytTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogWagaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogPaletaWysokoscTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogZapiszButton As System.Windows.Forms.Button
    Friend WithEvents slogSpedytorComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents slogTypUslugiComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents slogPobranieCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents slogRodzajPaczkiComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents slogRodzajPaletyComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents slogPaczkaNiestandardowaCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents slogPlatnoscZaPrzesylkeComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents slogKodyUslugDodCheckedListBox As System.Windows.Forms.CheckedListBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents slogDataNadaniaDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnPobierzDaneZref As System.Windows.Forms.Button
End Class
