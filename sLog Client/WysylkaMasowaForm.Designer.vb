﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class WysylkaMasowaForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.nrRefTextBox = New System.Windows.Forms.TextBox()
        Me.wysylkaProgressBar = New System.Windows.Forms.ProgressBar()
        Me.rozpocznijAnulujButton = New System.Windows.Forms.Button()
        Me.postepLabel = New System.Windows.Forms.Label()
        Me.wysylkaBackgroundWorker = New System.ComponentModel.BackgroundWorker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.stanowiskoComboBox = New System.Windows.Forms.ComboBox()
        Me.KontynuujButton = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 46)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(111, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Podaj numer dostawy:"
        '
        'nrRefTextBox
        '
        Me.nrRefTextBox.Location = New System.Drawing.Point(129, 43)
        Me.nrRefTextBox.Name = "nrRefTextBox"
        Me.nrRefTextBox.Size = New System.Drawing.Size(154, 20)
        Me.nrRefTextBox.TabIndex = 1
        '
        'wysylkaProgressBar
        '
        Me.wysylkaProgressBar.Location = New System.Drawing.Point(15, 119)
        Me.wysylkaProgressBar.Name = "wysylkaProgressBar"
        Me.wysylkaProgressBar.Size = New System.Drawing.Size(268, 23)
        Me.wysylkaProgressBar.TabIndex = 2
        '
        'rozpocznijAnulujButton
        '
        Me.rozpocznijAnulujButton.Location = New System.Drawing.Point(201, 69)
        Me.rozpocznijAnulujButton.Name = "rozpocznijAnulujButton"
        Me.rozpocznijAnulujButton.Size = New System.Drawing.Size(82, 23)
        Me.rozpocznijAnulujButton.TabIndex = 3
        Me.rozpocznijAnulujButton.Text = "Rozpocznij"
        Me.rozpocznijAnulujButton.UseVisualStyleBackColor = True
        '
        'postepLabel
        '
        Me.postepLabel.AutoSize = True
        Me.postepLabel.Location = New System.Drawing.Point(12, 103)
        Me.postepLabel.Name = "postepLabel"
        Me.postepLabel.Size = New System.Drawing.Size(43, 13)
        Me.postepLabel.TabIndex = 4
        Me.postepLabel.Text = "Postęp:"
        '
        'wysylkaBackgroundWorker
        '
        Me.wysylkaBackgroundWorker.WorkerReportsProgress = True
        Me.wysylkaBackgroundWorker.WorkerSupportsCancellation = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Stanowisko:"
        '
        'stanowiskoComboBox
        '
        Me.stanowiskoComboBox.FormattingEnabled = True
        Me.stanowiskoComboBox.Location = New System.Drawing.Point(129, 16)
        Me.stanowiskoComboBox.Name = "stanowiskoComboBox"
        Me.stanowiskoComboBox.Size = New System.Drawing.Size(154, 21)
        Me.stanowiskoComboBox.TabIndex = 6
        '
        'KontynuujButton
        '
        Me.KontynuujButton.Enabled = False
        Me.KontynuujButton.Location = New System.Drawing.Point(201, 158)
        Me.KontynuujButton.Name = "KontynuujButton"
        Me.KontynuujButton.Size = New System.Drawing.Size(82, 23)
        Me.KontynuujButton.TabIndex = 7
        Me.KontynuujButton.Text = "Kontynuuj"
        Me.KontynuujButton.UseVisualStyleBackColor = True
        '
        'WysylkaMasowaForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(306, 193)
        Me.Controls.Add(Me.KontynuujButton)
        Me.Controls.Add(Me.stanowiskoComboBox)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.postepLabel)
        Me.Controls.Add(Me.rozpocznijAnulujButton)
        Me.Controls.Add(Me.wysylkaProgressBar)
        Me.Controls.Add(Me.nrRefTextBox)
        Me.Controls.Add(Me.Label1)
        Me.Name = "WysylkaMasowaForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Wysyłka masowa"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents nrRefTextBox As System.Windows.Forms.TextBox
    Friend WithEvents wysylkaProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents rozpocznijAnulujButton As System.Windows.Forms.Button
    Friend WithEvents postepLabel As System.Windows.Forms.Label
    Friend WithEvents wysylkaBackgroundWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents stanowiskoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents KontynuujButton As System.Windows.Forms.Button
End Class
