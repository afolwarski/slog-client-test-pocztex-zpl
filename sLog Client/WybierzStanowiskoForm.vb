﻿Imports System.Data.SqlClient

Public Class WybierzStanowiskoForm
    Public Property Stanowisko As String = ""

    Private Sub WybierzStanowiskoForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        DialogResult = Windows.Forms.DialogResult.None

        Dim stanowiskaList As New List(Of String)

        Using conn As New SqlConnection(My.Settings.SLOGConnectionString)
            conn.Open()

            Using cmd As New SqlCommand()
                cmd.Connection = conn
                cmd.CommandText = "SELECT DISTINCT Stanowisko FROM stanowiska"

                Using reader As SqlDataReader = cmd.ExecuteReader()
                    While reader.Read()
                        If Not IsDBNull(reader("Stanowisko")) AndAlso Not String.IsNullOrEmpty(reader("Stanowisko").ToString()) Then stanowiskaList.Add(reader("Stanowisko").ToString())
                    End While
                End Using
            End Using
        End Using

        stanowiskaComboBox.DataSource = stanowiskaList

        If stanowiskaList.Contains(Environment.MachineName) Then
            stanowiskaComboBox.SelectedItem = Environment.MachineName
        Else
            stanowiskaComboBox.SelectedIndex = 0
        End If
    End Sub

    Private Sub stanowiskaComboBox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles stanowiskaComboBox.SelectedIndexChanged
        Stanowisko = stanowiskaComboBox.SelectedValue.ToString()
    End Sub
End Class