﻿Imports System.Data.SqlClient
Imports OfficeOpenXml
Imports System.IO

Public Class ImportAdresatowWzorcowaPaczkaForm

    Private loading As Boolean = True

    Private Sub ImportAdresatowWzorcowaPaczkaForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        DialogResult = Windows.Forms.DialogResult.None

        PobierzSlowniki()
        PodepnijSlowniki()

        loading = False
        UstawPola()
    End Sub

    Private slowniki As New Dictionary(Of String, List(Of ElementSlownika))

    Private Sub PobierzSlowniki()
        Try
            Using connection As New SqlConnection(My.Settings.SLOGConnectionString)
                connection.Open()

                Using cmd As New SqlCommand()
                    cmd.Connection = connection
                    cmd.CommandText = "SELECT * FROM slowniki"

                    Using reader As SqlDataReader = cmd.ExecuteReader()
                        While reader.Read()
                            Dim nazwa As String = reader("Nazwa").ToString()
                            Dim tytul As String = reader("Tytul").ToString()
                            Dim kod As String = reader("Kod").ToString()

                            Dim slownik As List(Of ElementSlownika) = Nothing
                            If slowniki.ContainsKey(nazwa) Then
                                slownik = slowniki(nazwa)
                            Else
                                slownik = New List(Of ElementSlownika)
                                'slownik.Add(New ElementSlownika("", "NULL"))
                                slowniki.Add(nazwa, slownik)
                            End If

                            If slownik.Find(Function(elem) elem.Tytul = tytul) Is Nothing Then slownik.Add(New ElementSlownika(tytul, kod))
                        End While
                    End Using
                End Using
            End Using
        Catch ex As Exception
        End Try
    End Sub

    Private Sub PodepnijSlowniki()
        PodepnijPojedynczySlownik(slogSpedytorComboBox, "Spedytor")
        PodepnijPojedynczySlownik(slogTypUslugiComboBox, "TypUslugi")
        PodepnijPojedynczySlownik(slogKodyUslugDodCheckedListBox, "KodyUslugDod")
        PodepnijPojedynczySlownik(slogRodzajPaczkiComboBox, "RodzajPaczki")
    End Sub

    Private Sub PodepnijPojedynczySlownik(combo As ListControl, nazwaSlownika As String)
        If slowniki.ContainsKey(nazwaSlownika) Then
            combo.DataSource = slowniki(nazwaSlownika)
            combo.DisplayMember = "Tytul"
            combo.ValueMember = "Kod"
        End If
    End Sub

    Private Class ElementSlownika
        Public Property Tytul As String
        Public Property Kod As String

        Public Sub New(title As String, code As String)
            Tytul = title
            Kod = code
        End Sub
    End Class

    Public Sub UstawPola()
        If loading Then Exit Sub

        If slogPobranieCheckBox.Checked Then slogKwotaPobraniaTextBox.ReadOnly = False Else slogKwotaPobraniaTextBox.ReadOnly = True
    End Sub

    Private Sub slogKodyUslugDodCheckedListBox_ItemCheck(sender As System.Object, e As System.Windows.Forms.ItemCheckEventArgs) Handles slogKodyUslugDodCheckedListBox.ItemCheck
        Dim el As ElementSlownika = CType(slogKodyUslugDodCheckedListBox.Items(e.Index), ElementSlownika)

        If el.Kod = "COD" Then
            If e.NewValue = CheckState.Checked Then
                If Not slogPobranieCheckBox.Checked Then slogPobranieCheckBox.Checked = True
            Else
                If slogPobranieCheckBox.Checked Then slogPobranieCheckBox.Checked = False
            End If
        End If
    End Sub

    Private Sub slogPobranieCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles slogPobranieCheckBox.CheckedChanged
        Dim codItem As ElementSlownika = slogKodyUslugDodCheckedListBox.Items.Cast(Of ElementSlownika).FirstOrDefault(Function(el) el.Kod = "COD")
        Dim codItemIndex As Integer = slogKodyUslugDodCheckedListBox.Items.IndexOf(codItem)
        Dim codSelected As Boolean = slogKodyUslugDodCheckedListBox.GetItemChecked(codItemIndex)

        If slogPobranieCheckBox.Checked Then
            If Not codSelected Then slogKodyUslugDodCheckedListBox.SetItemChecked(codItemIndex, True)
        Else
            If codSelected Then slogKodyUslugDodCheckedListBox.SetItemChecked(codItemIndex, False)
        End If

        UstawPola()
    End Sub

    Private Sub slogRodzajPaczkiComboBox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles slogRodzajPaczkiComboBox.SelectedIndexChanged
        UstawPola()
    End Sub

    Private Sub DodajButton_Click(sender As System.Object, e As System.EventArgs) Handles DodajButton.Click
        'If PaczkiNumeryDataGridView.EndEdit() Then
        Dim maxi As Integer = 0

        For Each row As DataGridViewRow In PaczkiNumeryDataGridView.Rows
            Dim i As Integer = 0
            Dim value As Object = row.Cells("NrPaczki").Value

            If Not value Is Nothing AndAlso Not String.IsNullOrEmpty(value.ToString()) AndAlso Integer.TryParse(value.ToString(), i) AndAlso i > maxi Then
                maxi = i
            End If
        Next

        Dim rowIndex As Integer = PaczkiNumeryDataGridView.Rows.Add(maxi + 1, "")
        PaczkiNumeryDataGridView.Rows(rowIndex).Selected = True
        'End If
    End Sub

    Private Sub UsunButton_Click(sender As System.Object, e As System.EventArgs) Handles UsunButton.Click
        'If PaczkiNumeryDataGridView.EndEdit() Then
        For Each rowIndex As Integer In PaczkiNumeryDataGridView.SelectedRows.Cast(Of DataGridViewRow).Select(Function(r) r.Index).ToArray()
            PaczkiNumeryDataGridView.Rows.RemoveAt(rowIndex)
        Next
        'End If
    End Sub

    Private Sub ImportujListeAdresatowButton_Click(sender As System.Object, e As System.EventArgs) Handles ImportujListeAdresatowButton.Click
        Dim fileDialog As New OpenFileDialog()
        fileDialog.CheckFileExists = True
        fileDialog.Multiselect = False
        fileDialog.Filter = "Excel (*.xls;*xlsx)|*.xls;*.xlsx"

        If fileDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Try
                Using excel As New ExcelPackage()
                    Using inputStream As New FileStream(fileDialog.FileName, FileMode.Open)
                        excel.Load(inputStream)
                    End Using

                    Dim ws As ExcelWorksheet = excel.Workbook.Worksheets(1)
                    Dim i As Integer = 2
                    Dim j As Integer

                    While CellNotEmpty(ws.Cells(i, 1).Value) OrElse CellNotEmpty(ws.Cells(i, 2).Value) OrElse CellNotEmpty(ws.Cells(i, 3).Value) OrElse CellNotEmpty(ws.Cells(i, 4).Value)
                        Dim adresatDane As New List(Of String)

                        For j = 1 To 8
                            Dim value As String = GetCellValue(ws.Cells(i, j).Value)
                            adresatDane.Add(value)
                        Next

                        AdresaciDataGridView.Rows.Add(adresatDane.ToArray())

                        i = i + 1
                    End While
                End Using
            Catch ex As Exception
                MessageBox.Show(ex.ToString())
            End Try
        End If
    End Sub

    Private Function CellNotEmpty(obj As Object) As Boolean
        Return Not obj Is Nothing AndAlso Not String.IsNullOrEmpty(obj.ToString())
    End Function

    Private Function GetCellValue(obj As Object) As String
        If obj Is Nothing OrElse String.IsNullOrEmpty(obj.ToString()) Then
            Return ""
        Else
            Return obj.ToString()
        End If
    End Function

    Private Sub WyczyscButton_Click(sender As System.Object, e As System.EventArgs) Handles WyczyscButton.Click
        AdresaciDataGridView.Rows.Clear()
    End Sub

    Private Sub ZapiszButton_Click(sender As System.Object, e As System.EventArgs) Handles ZapiszButton.Click
        If WalidujDane() AndAlso ZapiszDane() Then Close()
    End Sub

    Private Function WalidujDane() As Boolean
        Dim msgs As New List(Of String)

        If String.IsNullOrEmpty(slogNrReferencyjnyTextBox.Text) Then
            msgs.Add("Należy podać numer rerefencyjny przesyłki.")
        Else
            Using connection As New SqlConnection(My.Settings.SLOGConnectionString)
                connection.Open()

                Using cmd As New SqlCommand()
                    cmd.Connection = connection
                    cmd.CommandText = String.Format("SELECT TOP 1 ID FROM paczki WHERE NrRef = '{0}'", slogNrReferencyjnyTextBox.Text)

                    Using reader As SqlDataReader = cmd.ExecuteReader()
                        If reader.HasRows Then msgs.Add("Podany numer referencyjny nie jest unikatowy.")
                    End Using
                End Using
            End Using
        End If

        If slogPobranieCheckBox.Checked Then
            Dim kwota As Double

            If String.IsNullOrEmpty(slogKwotaPobraniaTextBox.Text) Then
                msgs.Add("Kwota pobrania nie może być pusta.")
            ElseIf Not Double.TryParse(slogKwotaPobraniaTextBox.Text, kwota) Then
                msgs.Add("Niepoprawny format kwoty pobrania.")
            End If
        End If

        If PaczkiNumeryDataGridView.RowCount = 0 Then
            msgs.Add("Należy dodać przynajmniej jedną paczkę.")
        Else
            Dim czyPodaneWagi As Boolean = PaczkiNumeryDataGridView.Rows.Cast(Of DataGridViewRow).
                FirstOrDefault(Function(r)
                                   Return r.Cells("Waga").Value Is Nothing OrElse String.IsNullOrEmpty(r.Cells("Waga").Value.ToString())
                               End Function) Is Nothing

            If Not czyPodaneWagi Then
                msgs.Add("Należy podać wagę każdej dodanej paczki.")
            End If
        End If

        If AdresaciDataGridView.RowCount = 0 Then
            msgs.Add("Należy podać przynajmniej jednego adresata.")
        Else
            For Each row As DataGridViewRow In AdresaciDataGridView.Rows
                Dim cells As DataGridViewCellCollection = row.Cells

                'If (cells("Nazwa").Value OrElse String.IsNullOrEmpty(cells("Nazwa").ToString())) AndAlso (
                '        cells("Imie").Value OrElse String.IsNullOrEmpty(cells("Imie").ToString()) OrElse
                '        cells("Nazwisko").Value OrElse String.IsNullOrEmpty(cells("Nazwisko").ToString())
                '    ) Then msgs.Add("Brak nazwy lub imienia i nazwiska adresata w wierszu" & row.Index)

                If cells("Nazwa").Value Is Nothing OrElse String.IsNullOrEmpty(cells("Nazwa").Value.ToString()) Then
                    msgs.Add("Pole 'Nazwa' jest puste w wierszu " & (row.Index + 1))
                ElseIf cells("Nazwa").Value.ToString().Length > 60 Then
                    msgs.Add("Pole 'Nazwa' przekracza limit znaków (60) w wierszu " & (row.Index + 1))
                End If

                If cells("Miasto").Value Is Nothing OrElse String.IsNullOrEmpty(cells("Miasto").Value.ToString()) Then
                    msgs.Add("Pole 'Miasto' jest puste w wierszu " & (row.Index + 1))
                ElseIf cells("Miasto").Value.ToString().Length > 17 Then
                    msgs.Add("Pole 'Miasto' przekracza limit znaków (17) w wierszu " & (row.Index + 1))
                End If

                If cells("Adres").Value Is Nothing OrElse String.IsNullOrEmpty(cells("Adres").Value.ToString()) Then
                    msgs.Add("Pole 'Adres' jest puste w wierszu " & (row.Index + 1))
                Else
                    Try
                        Dim adres As New Adres(cells("Adres").Value.ToString())
                    Catch ex As Exception
                        msgs.Add("Nie udało się wyodrębnić składowych adresu odbiorcy w wierszu " & (row.Index + 1) & ". Prawdopodobnie za dużo znaków. Limit: ulica - 22, nr domu - 7, nr mieszk. - 7, lub łącznie: 36.")
                    End Try
                End If

                If Not cells("Telefon").Value Is Nothing AndAlso Not String.IsNullOrEmpty(cells("Telefon").Value.ToString()) Then
                    Dim nrTel As String = cells("Telefon").Value.ToString().Replace(" ", "").Replace("-", "").Replace("+", "").Replace("(", "").Replace(")", "").Replace("\", "").Replace("/", "")
                    Dim telefonRegex As New System.Text.RegularExpressions.Regex("^[0-9]*$")

                    If Not telefonRegex.IsMatch(nrTel) Then
                        msgs.Add("Nieprawidłowe znaki w polu 'Telefon' w wierszu " & (row.Index + 1))
                    End If
                End If

                If cells("KodPocztowy").Value Is Nothing OrElse String.IsNullOrEmpty(cells("KodPocztowy").Value.ToString()) Then
                    msgs.Add("Pole 'Kod pocztowy' jest puste w wierszu " & (row.Index + 1))
                Else
                    Dim regex As New System.Text.RegularExpressions.Regex("^[0-9]{2}-[0-9]{3}$")
                    Dim kodPocztowy As String = cells("KodPocztowy").Value.ToString()

                    If Not String.IsNullOrEmpty(kodPocztowy) AndAlso Not regex.IsMatch(kodPocztowy) Then msgs.Add("Niepoprawny format kodu pocztowego w wierszu " & (row.Index + 1))
                End If
            Next
        End If

        If msgs.Count > 0 Then
            Dim msg As String = "Proszę poprawić następujące błędy:" & vbCrLf & vbTab & String.Join(vbCrLf & vbTab, msgs.ToArray())
            MessageBox.Show(msg)
            Return False
        Else
            Return True
        End If
    End Function

    Private Function ZapiszDane() As Boolean
        Dim liczbaPaczek As Integer

        liczbaPaczek = PaczkiNumeryDataGridView.RowCount

        Try
            Using connection As New SqlConnection(My.Settings.SLOGConnectionString)
                connection.Open()

                Using cmd As New SqlCommand()
                    cmd.Connection = connection
                    cmd.CommandText = String.Concat("INSERT INTO paczki (Spedytor, Stanowisko, Zrealizowane, Status, TypUslugi, KodyUslugDod, Pobranie, KwotaPobrania, RodzajPaczki, RodzajPalety, PaletaDl, PaletaSzer, PaletaWys, PaczkaNiestandardowa, Waga, Gabaryt, NrRef, NrPaczki, NrPaczkiWiodacej, OdbNazwa, OdbOsoba, OdbAdres, OdbMiasto, OdbKodPocztowy, OdbTelefon, OdbMail, PlatnoscZaPrzesylke, DataNadaniaWgSAP,L_paczek,PotwierdzenieWyjsciaZMag) VALUES ",
                                                    "(@spedytor, @stanowisko, 0, 1, @typUslugi, @kodyUslugDod, @pobranie, @kwotaPobrania, @rodzajPaczki, @rodzajPalety, @paletaDl, @paletaSzer,",
                                                    "@paletaWys, @paczkaNiestandardowa, @waga, @gabaryt, @nrRef, @nrPaczki, @nrPaczkiWiodacej, @odbNazwa, @odbOsoba, @odbAdres, @odbMiasto, @odbKodPocztowy, @odbTelefon, @odbMail, @platnoscZaPrzesylke, @DataNadaniaWgSAP, @L_paczek, @PotwierdzenieWyjsciaZMag)")

                    Dim nrPaczki As Integer = 1

                    For Each adresatRow As DataGridViewRow In AdresaciDataGridView.Rows
                        Dim nrPaczkiWiodacej As Integer = nrPaczki

                        For Each paczkaRow As DataGridViewRow In PaczkiNumeryDataGridView.Rows
                            cmd.Parameters.Clear()

                            cmd.Parameters.Add("@spedytor", SqlDbType.VarChar).Value = GetDBText(slogSpedytorComboBox.SelectedValue)
                            cmd.Parameters.Add("@stanowisko", SqlDbType.VarChar).Value = Environment.MachineName
                            cmd.Parameters.Add("@typUslugi", SqlDbType.VarChar).Value = GetDBText(slogTypUslugiComboBox.SelectedValue)

                            cmd.Parameters.Add("@kodyUslugDod", SqlDbType.VarChar).Value = String.Join(",", slogKodyUslugDodCheckedListBox.CheckedItems.Cast(Of ElementSlownika).Select(Function(item) item.Kod))

                            cmd.Parameters.Add("@pobranie", SqlDbType.Int).Value = GetDBBoolAsInteger(slogPobranieCheckBox.Checked)
                            cmd.Parameters.Add("@kwotaPobrania", SqlDbType.Float).Value = GetDBDouble(slogKwotaPobraniaTextBox.Text)
                            cmd.Parameters.Add("@rodzajPaczki", SqlDbType.VarChar).Value = GetDBText(slogRodzajPaczkiComboBox.SelectedValue)
                            cmd.Parameters.Add("@rodzajPalety", SqlDbType.VarChar).Value = DBNull.Value
                            cmd.Parameters.Add("@paletaDl", SqlDbType.Float).Value = DBNull.Value
                            cmd.Parameters.Add("@paletaSzer", SqlDbType.Float).Value = DBNull.Value
                            cmd.Parameters.Add("@paletaWys", SqlDbType.Float).Value = DBNull.Value
                            cmd.Parameters.Add("@paczkaNiestandardowa", SqlDbType.Int).Value = 0
                            cmd.Parameters.Add("@waga", SqlDbType.Float).Value = GetDBDouble(paczkaRow.Cells("Waga").Value)
                            cmd.Parameters.Add("@gabaryt", SqlDbType.VarChar).Value = DBNull.Value
                            cmd.Parameters.Add("@nrRef", SqlDbType.VarChar).Value = GetDBText(slogNrReferencyjnyTextBox.Text)
                            cmd.Parameters.Add("@nrPaczki", SqlDbType.Int).Value = nrPaczki

                            If nrPaczki = nrPaczkiWiodacej Then
                                cmd.Parameters.Add("@nrPaczkiWiodacej", SqlDbType.Int).Value = DBNull.Value
                                cmd.Parameters.Add("@L_paczek", SqlDbType.Int).Value = liczbaPaczek
                                cmd.Parameters.Add("@PotwierdzenieWyjsciaZMag", SqlDbType.Int).Value = liczbaPaczek
                            Else
                                cmd.Parameters.Add("@nrPaczkiWiodacej", SqlDbType.Int).Value = nrPaczkiWiodacej
                                cmd.Parameters.Add("@L_paczek", SqlDbType.Int).Value = DBNull.Value
                                cmd.Parameters.Add("@PotwierdzenieWyjsciaZMag", SqlDbType.Int).Value = DBNull.Value
                            End If

                            cmd.Parameters.Add("@odbNazwa", SqlDbType.VarChar).Value = GetDBText(adresatRow.Cells("Nazwa").Value)

                            Dim osoba As String = ""

                            If Not adresatRow.Cells("Imie").Value Is Nothing AndAlso Not String.IsNullOrEmpty(adresatRow.Cells("Imie").Value.ToString()) Then osoba = osoba & adresatRow.Cells("Imie").Value.ToString() & " "
                            If Not adresatRow.Cells("Nazwisko").Value Is Nothing AndAlso Not String.IsNullOrEmpty(adresatRow.Cells("Nazwisko").Value.ToString()) Then osoba = osoba & adresatRow.Cells("Nazwisko").Value.ToString()

                            cmd.Parameters.Add("@odbOsoba", SqlDbType.VarChar).Value = GetDBText(osoba.Trim())

                            cmd.Parameters.Add("@odbAdres", SqlDbType.VarChar).Value = GetDBText(adresatRow.Cells("Adres").Value.ToString())
                            cmd.Parameters.Add("@odbMiasto", SqlDbType.VarChar).Value = GetDBText(adresatRow.Cells("Miasto").Value.ToString())
                            cmd.Parameters.Add("@odbKodPocztowy", SqlDbType.VarChar).Value = GetDBText(adresatRow.Cells("KodPocztowy").Value.ToString())
                            cmd.Parameters.Add("@odbTelefon", SqlDbType.VarChar).Value = GetDBText(adresatRow.Cells("Telefon").Value.ToString())
                            cmd.Parameters.Add("@odbMail", SqlDbType.VarChar).Value = GetDBText(adresatRow.Cells("Email").Value.ToString())
                            cmd.Parameters.Add("@platnoscZaPrzesylke", SqlDbType.Int).Value = 0

                            cmd.Parameters.Add("@DataNadaniaWgSAP", SqlDbType.DateTime).Value = DateTime.Today

                            cmd.ExecuteNonQuery()

                            nrPaczki = nrPaczki + 1
                        Next
                    Next
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show("Wystąpił błąd:" & vbCrLf & vbCrLf & ex.ToString(), "sLog Client - Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try

        Return True
    End Function

    Private Function GetDBText(obj As Object) As Object
        If Not obj Is Nothing AndAlso Not String.IsNullOrEmpty(obj.ToString()) Then
            Return obj.ToString()
        Else
            Return DBNull.Value
        End If
    End Function

    Private Function GetDBDouble(obj As Object) As Object
        If Not obj Is Nothing AndAlso Not String.IsNullOrEmpty(obj.ToString()) Then
            Return Double.Parse(obj.ToString())
        Else
            Return DBNull.Value
        End If
    End Function

    Private Function GetDBBoolAsInteger(boolValue As Boolean) As Object
        If boolValue = True Then Return 1 Else Return 0
    End Function

    Private Sub AnulujButton_Click(sender As System.Object, e As System.EventArgs) Handles AnulujButton.Click
        Close()
    End Sub

    Private Sub PaczkiNumeryDataGridView_CellValidating(sender As System.Object, e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles PaczkiNumeryDataGridView.CellValidating
        If e.RowIndex >= 0 Then
            Dim intNum As Integer
            Dim doubleNum As Double

            Dim pozostaleNumeryPaczek As IEnumerable(Of Integer) = PaczkiNumeryDataGridView.Rows.Cast(Of DataGridViewRow)().
                Where(Function(r)
                          Return r.Index <> e.RowIndex AndAlso Not r.Cells("NrPaczki").Value Is Nothing AndAlso Not String.IsNullOrEmpty(r.Cells("NrPaczki").Value.ToString())
                      End Function).
                Select(Function(r)
                           Return Integer.Parse(r.Cells("NrPaczki").Value.ToString())
                       End Function)

            If e.ColumnIndex = 0 AndAlso Not e.FormattedValue Is Nothing AndAlso Not String.IsNullOrEmpty(e.FormattedValue.ToString()) AndAlso (
                    Not Integer.TryParse(e.FormattedValue.ToString(), intNum) OrElse
                    pozostaleNumeryPaczek.Contains(intNum)
                ) Then e.Cancel = True

            If e.ColumnIndex = 1 AndAlso Not e.FormattedValue Is Nothing AndAlso Not String.IsNullOrEmpty(e.FormattedValue.ToString()) AndAlso
                Not Double.TryParse(e.FormattedValue.ToString(), doubleNum) Then e.Cancel = True
        End If
    End Sub

    Private Sub PaczkiNumeryDataGridView_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles PaczkiNumeryDataGridView.CellClick
        If e.RowIndex >= 0 AndAlso e.ColumnIndex = 1 Then
            Dim cell As DataGridViewCell = PaczkiNumeryDataGridView.Rows(e.RowIndex).Cells(e.ColumnIndex)

            If Object.ReferenceEquals(PaczkiNumeryDataGridView.CurrentCell, cell) Then
                PaczkiNumeryDataGridView.BeginEdit(True)
            Else
                PaczkiNumeryDataGridView.CurrentCell = cell
            End If
        End If
    End Sub

    Private Sub PaczkiNumeryDataGridView_CellValueChanged(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles PaczkiNumeryDataGridView.CellValueChanged
        If e.RowIndex >= 0 AndAlso e.ColumnIndex = 1 Then
            wagaCalkowitaTextBox.Text = PaczkiNumeryDataGridView.Rows.Cast(Of DataGridViewRow).
                Select(Function(r)
                           If Not r.Cells(e.ColumnIndex).Value Is Nothing AndAlso Not String.IsNullOrEmpty(r.Cells(e.ColumnIndex).Value.ToString()) Then
                               Return Double.Parse(r.Cells(e.ColumnIndex).Value.ToString())
                           Else
                               Return 0.0
                           End If
                       End Function).Sum()
        End If
    End Sub

    Private Sub PaczkiNumeryDataGridView_RowsAdded(sender As System.Object, e As System.Windows.Forms.DataGridViewRowsAddedEventArgs) Handles PaczkiNumeryDataGridView.RowsAdded
        Dim row As DataGridViewRow = PaczkiNumeryDataGridView.Rows(e.RowIndex)
        row.Cells("NrPaczki").ReadOnly = True
        row.Cells("Wiodaca").ReadOnly = True

        If e.RowIndex = 0 Then row.Cells("Wiodaca").Value = True
    End Sub
End Class