﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.toolRefresh = New System.Windows.Forms.ToolStripButton()
        Me.toolCreatePackage = New System.Windows.Forms.ToolStripButton()
        Me.toolImportAddressList = New System.Windows.Forms.ToolStripButton()
        Me.toolPrintLabelsMenu = New System.Windows.Forms.ToolStripButton()
        Me.ToolDataExport = New System.Windows.Forms.ToolStripButton()
        Me.toolMassSped = New System.Windows.Forms.ToolStripDropDownButton()
        Me.WysyłkaMasowaPocztaPolskaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WysyłkaMasowaPozostałeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.toolInpostListsCreate = New System.Windows.Forms.ToolStripButton()
        Me.toolSendEnv_command = New System.Windows.Forms.ToolStripButton()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.tvDaty = New System.Windows.Forms.TreeView()
        Me.filtryPanel = New System.Windows.Forms.Panel()
        Me.paczkiDataGridView = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Stanowisko1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Zrealizowane = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Spedytor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataNadaniaWgSAP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataNadaniaReal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TypUslugi = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KodyUslugDod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Pobranie = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KwotaPobrania = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RodzajPaczki = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PaczkaNiestandardowa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Waga = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NrListu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NrRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NrPaczki = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NrPaczkiWiodacej = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OdbNazwa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OdbOsoba = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OdbAdres = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OdbMiasto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OdbKodPocztowy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PaczkiBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SLOGDataSet = New sLog_Client.SLOGDataSet()
        Me.PaczkiTableAdapter = New sLog_Client.SLOGDataSetTableAdapters.paczkiTableAdapter()
        Me.Panel1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.filtryPanel.SuspendLayout()
        CType(Me.paczkiDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PaczkiBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SLOGDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ToolStrip1)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1297, 29)
        Me.Panel1.TabIndex = 2
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolRefresh, Me.toolCreatePackage, Me.toolImportAddressList, Me.toolPrintLabelsMenu, Me.ToolDataExport, Me.toolMassSped, Me.toolInpostListsCreate, Me.toolSendEnv_command})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1297, 29)
        Me.ToolStrip1.TabIndex = 5
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'toolRefresh
        '
        Me.toolRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.toolRefresh.Image = CType(resources.GetObject("toolRefresh.Image"), System.Drawing.Image)
        Me.toolRefresh.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolRefresh.Name = "toolRefresh"
        Me.toolRefresh.Size = New System.Drawing.Size(55, 26)
        Me.toolRefresh.Text = "Odśwież"
        '
        'toolCreatePackage
        '
        Me.toolCreatePackage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.toolCreatePackage.Image = CType(resources.GetObject("toolCreatePackage.Image"), System.Drawing.Image)
        Me.toolCreatePackage.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolCreatePackage.Name = "toolCreatePackage"
        Me.toolCreatePackage.Size = New System.Drawing.Size(87, 26)
        Me.toolCreatePackage.Text = "Utwórz paczkę"
        '
        'toolImportAddressList
        '
        Me.toolImportAddressList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.toolImportAddressList.Image = CType(resources.GetObject("toolImportAddressList.Image"), System.Drawing.Image)
        Me.toolImportAddressList.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolImportAddressList.Name = "toolImportAddressList"
        Me.toolImportAddressList.Size = New System.Drawing.Size(216, 26)
        Me.toolImportAddressList.Text = "Import adresatów do wzorcowej paczki"
        '
        'toolPrintLabelsMenu
        '
        Me.toolPrintLabelsMenu.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.toolPrintLabelsMenu.Image = CType(resources.GetObject("toolPrintLabelsMenu.Image"), System.Drawing.Image)
        Me.toolPrintLabelsMenu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolPrintLabelsMenu.Name = "toolPrintLabelsMenu"
        Me.toolPrintLabelsMenu.Size = New System.Drawing.Size(90, 26)
        Me.toolPrintLabelsMenu.Text = "Wydruk etykiet"
        '
        'ToolDataExport
        '
        Me.ToolDataExport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolDataExport.Image = CType(resources.GetObject("ToolDataExport.Image"), System.Drawing.Image)
        Me.ToolDataExport.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolDataExport.Name = "ToolDataExport"
        Me.ToolDataExport.Size = New System.Drawing.Size(78, 26)
        Me.ToolDataExport.Text = "Exportuj listę"
        '
        'toolMassSped
        '
        Me.toolMassSped.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.toolMassSped.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.WysyłkaMasowaPocztaPolskaToolStripMenuItem, Me.WysyłkaMasowaPozostałeToolStripMenuItem})
        Me.toolMassSped.Image = CType(resources.GetObject("toolMassSped.Image"), System.Drawing.Image)
        Me.toolMassSped.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolMassSped.Name = "toolMassSped"
        Me.toolMassSped.Size = New System.Drawing.Size(110, 26)
        Me.toolMassSped.Text = "Wysyłka masowa"
        '
        'WysyłkaMasowaPocztaPolskaToolStripMenuItem
        '
        Me.WysyłkaMasowaPocztaPolskaToolStripMenuItem.Name = "WysyłkaMasowaPocztaPolskaToolStripMenuItem"
        Me.WysyłkaMasowaPocztaPolskaToolStripMenuItem.Size = New System.Drawing.Size(239, 22)
        Me.WysyłkaMasowaPocztaPolskaToolStripMenuItem.Text = "Wysyłka masowa Poczta Polska"
        '
        'WysyłkaMasowaPozostałeToolStripMenuItem
        '
        Me.WysyłkaMasowaPozostałeToolStripMenuItem.Name = "WysyłkaMasowaPozostałeToolStripMenuItem"
        Me.WysyłkaMasowaPozostałeToolStripMenuItem.Size = New System.Drawing.Size(239, 22)
        Me.WysyłkaMasowaPozostałeToolStripMenuItem.Text = "Wysyłka masowa pozostałe"
        '
        'toolInpostListsCreate
        '
        Me.toolInpostListsCreate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.toolInpostListsCreate.Image = CType(resources.GetObject("toolInpostListsCreate.Image"), System.Drawing.Image)
        Me.toolInpostListsCreate.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolInpostListsCreate.Name = "toolInpostListsCreate"
        Me.toolInpostListsCreate.Size = New System.Drawing.Size(116, 26)
        Me.toolInpostListsCreate.Text = "Utwórz listy INPOST"
        '
        'toolSendEnv_command
        '
        Me.toolSendEnv_command.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.toolSendEnv_command.Image = CType(resources.GetObject("toolSendEnv_command.Image"), System.Drawing.Image)
        Me.toolSendEnv_command.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolSendEnv_command.Name = "toolSendEnv_command"
        Me.toolSendEnv_command.Size = New System.Drawing.Size(102, 26)
        Me.toolSendEnv_command.Text = "PP SendEnvelope"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 31)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.tvDaty)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.filtryPanel)
        Me.SplitContainer1.Size = New System.Drawing.Size(1117, 555)
        Me.SplitContainer1.SplitterDistance = 121
        Me.SplitContainer1.TabIndex = 3
        '
        'tvDaty
        '
        Me.tvDaty.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvDaty.Location = New System.Drawing.Point(0, 0)
        Me.tvDaty.Name = "tvDaty"
        Me.tvDaty.Size = New System.Drawing.Size(119, 553)
        Me.tvDaty.TabIndex = 0
        '
        'filtryPanel
        '
        Me.filtryPanel.AutoScroll = True
        Me.filtryPanel.Controls.Add(Me.paczkiDataGridView)
        Me.filtryPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.filtryPanel.Location = New System.Drawing.Point(0, 0)
        Me.filtryPanel.Margin = New System.Windows.Forms.Padding(0)
        Me.filtryPanel.Name = "filtryPanel"
        Me.filtryPanel.Size = New System.Drawing.Size(990, 553)
        Me.filtryPanel.TabIndex = 2
        '
        'paczkiDataGridView
        '
        Me.paczkiDataGridView.AllowUserToAddRows = False
        Me.paczkiDataGridView.AllowUserToDeleteRows = False
        Me.paczkiDataGridView.AllowUserToResizeRows = False
        Me.paczkiDataGridView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.paczkiDataGridView.AutoGenerateColumns = False
        Me.paczkiDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.paczkiDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.paczkiDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.Stanowisko1, Me.Zrealizowane, Me.Status, Me.Spedytor, Me.DataNadaniaWgSAP, Me.DataNadaniaReal, Me.TypUslugi, Me.KodyUslugDod, Me.Pobranie, Me.KwotaPobrania, Me.RodzajPaczki, Me.PaczkaNiestandardowa, Me.Waga, Me.NrListu, Me.NrRef, Me.NrPaczki, Me.NrPaczkiWiodacej, Me.OdbNazwa, Me.OdbOsoba, Me.OdbAdres, Me.OdbMiasto, Me.OdbKodPocztowy})
        Me.paczkiDataGridView.DataSource = Me.PaczkiBindingSource
        Me.paczkiDataGridView.Location = New System.Drawing.Point(0, 20)
        Me.paczkiDataGridView.Margin = New System.Windows.Forms.Padding(0)
        Me.paczkiDataGridView.Name = "paczkiDataGridView"
        Me.paczkiDataGridView.RowHeadersVisible = False
        Me.paczkiDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.paczkiDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.paczkiDataGridView.Size = New System.Drawing.Size(991, 532)
        Me.paczkiDataGridView.TabIndex = 4
        '
        'ID
        '
        Me.ID.DataPropertyName = "ID"
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        Me.ID.Visible = False
        '
        'Stanowisko1
        '
        Me.Stanowisko1.DataPropertyName = "Stanowisko"
        Me.Stanowisko1.HeaderText = "Stanowisko"
        Me.Stanowisko1.Name = "Stanowisko1"
        '
        'Zrealizowane
        '
        Me.Zrealizowane.DataPropertyName = "Zrealizowane"
        Me.Zrealizowane.HeaderText = "Zrealizowane"
        Me.Zrealizowane.Name = "Zrealizowane"
        '
        'Status
        '
        Me.Status.DataPropertyName = "Status"
        Me.Status.HeaderText = "Status"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        '
        'Spedytor
        '
        Me.Spedytor.DataPropertyName = "Spedytor"
        Me.Spedytor.HeaderText = "Spedytor"
        Me.Spedytor.Name = "Spedytor"
        '
        'DataNadaniaWgSAP
        '
        Me.DataNadaniaWgSAP.DataPropertyName = "DataNadaniaWgSAP"
        Me.DataNadaniaWgSAP.HeaderText = "Data nadania wg SAP"
        Me.DataNadaniaWgSAP.Name = "DataNadaniaWgSAP"
        '
        'DataNadaniaReal
        '
        Me.DataNadaniaReal.DataPropertyName = "DataNadaniaReal"
        Me.DataNadaniaReal.HeaderText = "Data nadania realna"
        Me.DataNadaniaReal.Name = "DataNadaniaReal"
        '
        'TypUslugi
        '
        Me.TypUslugi.DataPropertyName = "TypUslugi"
        Me.TypUslugi.HeaderText = "Typ usługi"
        Me.TypUslugi.Name = "TypUslugi"
        '
        'KodyUslugDod
        '
        Me.KodyUslugDod.DataPropertyName = "KodyUslugDod"
        Me.KodyUslugDod.HeaderText = "Kody usług dodatkowych"
        Me.KodyUslugDod.Name = "KodyUslugDod"
        '
        'Pobranie
        '
        Me.Pobranie.DataPropertyName = "Pobranie"
        Me.Pobranie.HeaderText = "Pobranie"
        Me.Pobranie.Name = "Pobranie"
        '
        'KwotaPobrania
        '
        Me.KwotaPobrania.DataPropertyName = "KwotaPobrania"
        Me.KwotaPobrania.HeaderText = "Kwota pobrania"
        Me.KwotaPobrania.Name = "KwotaPobrania"
        '
        'RodzajPaczki
        '
        Me.RodzajPaczki.DataPropertyName = "RodzajPaczki"
        Me.RodzajPaczki.HeaderText = "Rodzaj paczki"
        Me.RodzajPaczki.Name = "RodzajPaczki"
        '
        'PaczkaNiestandardowa
        '
        Me.PaczkaNiestandardowa.DataPropertyName = "PaczkaNiestandardowa"
        Me.PaczkaNiestandardowa.HeaderText = "Paczka niestandardowa"
        Me.PaczkaNiestandardowa.Name = "PaczkaNiestandardowa"
        '
        'Waga
        '
        Me.Waga.DataPropertyName = "Waga"
        Me.Waga.HeaderText = "Waga"
        Me.Waga.Name = "Waga"
        '
        'NrListu
        '
        Me.NrListu.DataPropertyName = "NrListu"
        Me.NrListu.HeaderText = "Nr listu"
        Me.NrListu.Name = "NrListu"
        '
        'NrRef
        '
        Me.NrRef.DataPropertyName = "NrRef"
        Me.NrRef.HeaderText = "NrRef"
        Me.NrRef.Name = "NrRef"
        '
        'NrPaczki
        '
        Me.NrPaczki.DataPropertyName = "NrPaczki"
        Me.NrPaczki.HeaderText = "Nr paczki"
        Me.NrPaczki.Name = "NrPaczki"
        '
        'NrPaczkiWiodacej
        '
        Me.NrPaczkiWiodacej.DataPropertyName = "NrPaczkiWiodacej"
        Me.NrPaczkiWiodacej.HeaderText = "Nr paczki wiodącej"
        Me.NrPaczkiWiodacej.Name = "NrPaczkiWiodacej"
        '
        'OdbNazwa
        '
        Me.OdbNazwa.DataPropertyName = "OdbNazwa"
        Me.OdbNazwa.HeaderText = "Odbiorca - Nazwa"
        Me.OdbNazwa.Name = "OdbNazwa"
        '
        'OdbOsoba
        '
        Me.OdbOsoba.DataPropertyName = "OdbOsoba"
        Me.OdbOsoba.HeaderText = "Odbiorca - Osoba"
        Me.OdbOsoba.Name = "OdbOsoba"
        '
        'OdbAdres
        '
        Me.OdbAdres.DataPropertyName = "OdbAdres"
        Me.OdbAdres.HeaderText = "Odbiorca - Adres"
        Me.OdbAdres.Name = "OdbAdres"
        '
        'OdbMiasto
        '
        Me.OdbMiasto.DataPropertyName = "OdbMiasto"
        Me.OdbMiasto.HeaderText = "Odbiorca - Miasto"
        Me.OdbMiasto.Name = "OdbMiasto"
        '
        'OdbKodPocztowy
        '
        Me.OdbKodPocztowy.DataPropertyName = "OdbKodPocztowy"
        Me.OdbKodPocztowy.HeaderText = "Odbiorca - Kod pocztowy"
        Me.OdbKodPocztowy.Name = "OdbKodPocztowy"
        '
        'PaczkiBindingSource
        '
        Me.PaczkiBindingSource.DataMember = "paczki"
        Me.PaczkiBindingSource.DataSource = Me.SLOGDataSet
        '
        'SLOGDataSet
        '
        Me.SLOGDataSet.DataSetName = "SLOGDataSet"
        Me.SLOGDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PaczkiTableAdapter
        '
        Me.PaczkiTableAdapter.ClearBeforeFill = True
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1119, 586)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "MainForm"
        Me.Text = "sLog Client TEST"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.filtryPanel.ResumeLayout(False)
        CType(Me.paczkiDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PaczkiBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SLOGDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SLOGDataSet As sLog_Client.SLOGDataSet
    Friend WithEvents PaczkiBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PaczkiTableAdapter As sLog_Client.SLOGDataSetTableAdapters.paczkiTableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Stanowisko As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataUtworzeniaRekordu As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RodzajPalety As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PaletaDl As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PaletaSzer As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PaletaWys As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Gabaryt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OdbTelefon As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OdbMail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ZadanieAnulowania As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Anulowane As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataAnulowania As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PlatnoscZaPrzesylke As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NrKlientaPlac As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PIStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PIData As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OpisBledu As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PotwierdzenieWyjsciaZMag As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PotwierdzenieOsoba As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PotwierdzenieData As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Modyfikacja As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ModyfikacjaOsoba As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents filtryPanel As System.Windows.Forms.Panel
    Friend WithEvents tvDaty As System.Windows.Forms.TreeView
    Friend WithEvents paczkiDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Stanowisko1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Zrealizowane As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Spedytor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataNadaniaWgSAP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataNadaniaReal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TypUslugi As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KodyUslugDod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Pobranie As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KwotaPobrania As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RodzajPaczki As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PaczkaNiestandardowa As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Waga As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NrListu As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NrRef As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NrPaczki As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NrPaczkiWiodacej As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OdbNazwa As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OdbOsoba As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OdbAdres As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OdbMiasto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OdbKodPocztowy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents toolRefresh As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolCreatePackage As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolImportAddressList As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolDataExport As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolMassSped As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents WysyłkaMasowaPocztaPolskaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WysyłkaMasowaPozostałeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolInpostListsCreate As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolSendEnv_command As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolPrintLabelsMenu As System.Windows.Forms.ToolStripButton

End Class
