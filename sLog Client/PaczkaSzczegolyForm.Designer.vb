﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PaczkaSzczegolyForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.slogIdTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.slogZrealizowaneTextBox = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.slogStatusTextBox = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.slogSpedytorTextBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.slogStanowiskoTextBox = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.slogDataNadaniaWgSAPTextBox = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.slogDataUtworzeniaRekorduTextBox = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.slogDataNadaniaRealnieTextBox = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.slogTypUslugiTextBox = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.slogKodyUslugDodatkowychTextBox1 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.slogPobranieTextBox = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.slogKwotaPobraniaTextBox = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.slogRodzajPaczkiTextBox = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.slogRodzajPaletyTextBox = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.slogPaletaDlugoscTextBox = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.slogPaletaSzerokoscTextBox = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.slogPaletaWysokoscTextBox = New System.Windows.Forms.TextBox()
        Me.slogPaczkaNiestandardowaTextBox = New System.Windows.Forms.TextBox()
        Me.slogWagaTextBox = New System.Windows.Forms.TextBox()
        Me.slogGabarytTextBox = New System.Windows.Forms.TextBox()
        Me.slogNrListuTextBox = New System.Windows.Forms.TextBox()
        Me.slogNrReferencyjnyTextBox = New System.Windows.Forms.TextBox()
        Me.slogNrPaczkiTextBox = New System.Windows.Forms.TextBox()
        Me.slogNrPaczkiWiodacejTextBox = New System.Windows.Forms.TextBox()
        Me.slogOdbiorcaNazwaTextBox = New System.Windows.Forms.TextBox()
        Me.slogOdbiorcaOsobaTextBox = New System.Windows.Forms.TextBox()
        Me.slogOdbiorcaAdresTextBox = New System.Windows.Forms.TextBox()
        Me.slogOdbiorcaMiastoTextBox = New System.Windows.Forms.TextBox()
        Me.slogOdbiorcaKodPocztowyTextBox = New System.Windows.Forms.TextBox()
        Me.slogOdbiorcaTelefonTextBox = New System.Windows.Forms.TextBox()
        Me.slogOdbiorcaMailTextBox = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.slogAnulowaneTextBox = New System.Windows.Forms.TextBox()
        Me.slogDataAnulowaniaTextBox = New System.Windows.Forms.TextBox()
        Me.slogPlatnoscZaPrzesylkeTextBox = New System.Windows.Forms.TextBox()
        Me.slogNrKlientaPlacTextBox = New System.Windows.Forms.TextBox()
        Me.slogPIStatusTextBox = New System.Windows.Forms.TextBox()
        Me.slogPIDataTextBox = New System.Windows.Forms.TextBox()
        Me.slogOpisBleduTextBox = New System.Windows.Forms.TextBox()
        Me.slogPotwierdzenieWyjsciaZMagTextBox = New System.Windows.Forms.TextBox()
        Me.slogPotwierdzenieOsobaTextBox = New System.Windows.Forms.TextBox()
        Me.slogPotwierdzenieDataTextBox = New System.Windows.Forms.TextBox()
        Me.slogModyfikacjaOsobaTextBox = New System.Windows.Forms.TextBox()
        Me.slogModyfikacjaDataTextBox = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.slogZamknijButton = New System.Windows.Forms.Button()
        Me.slogZadanieAnulowaniaCheckBox = New System.Windows.Forms.CheckBox()
        Me.ZapiszButton = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'slogIdTextBox
        '
        Me.slogIdTextBox.Location = New System.Drawing.Point(167, 27)
        Me.slogIdTextBox.Name = "slogIdTextBox"
        Me.slogIdTextBox.ReadOnly = True
        Me.slogIdTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogIdTextBox.TabIndex = 100
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(30, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(18, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "ID"
        '
        'slogZrealizowaneTextBox
        '
        Me.slogZrealizowaneTextBox.Location = New System.Drawing.Point(167, 53)
        Me.slogZrealizowaneTextBox.Name = "slogZrealizowaneTextBox"
        Me.slogZrealizowaneTextBox.ReadOnly = True
        Me.slogZrealizowaneTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogZrealizowaneTextBox.TabIndex = 100
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(30, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Zrealizowane"
        '
        'slogStatusTextBox
        '
        Me.slogStatusTextBox.Location = New System.Drawing.Point(167, 79)
        Me.slogStatusTextBox.Name = "slogStatusTextBox"
        Me.slogStatusTextBox.ReadOnly = True
        Me.slogStatusTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogStatusTextBox.TabIndex = 100
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(30, 82)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(37, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Status"
        '
        'slogSpedytorTextBox
        '
        Me.slogSpedytorTextBox.Location = New System.Drawing.Point(167, 105)
        Me.slogSpedytorTextBox.Name = "slogSpedytorTextBox"
        Me.slogSpedytorTextBox.ReadOnly = True
        Me.slogSpedytorTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogSpedytorTextBox.TabIndex = 100
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(30, 108)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Spedytor"
        '
        'slogStanowiskoTextBox
        '
        Me.slogStanowiskoTextBox.Location = New System.Drawing.Point(167, 131)
        Me.slogStanowiskoTextBox.Name = "slogStanowiskoTextBox"
        Me.slogStanowiskoTextBox.ReadOnly = True
        Me.slogStanowiskoTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogStanowiskoTextBox.TabIndex = 100
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(30, 134)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Stanowisko"
        '
        'slogDataNadaniaWgSAPTextBox
        '
        Me.slogDataNadaniaWgSAPTextBox.Location = New System.Drawing.Point(167, 157)
        Me.slogDataNadaniaWgSAPTextBox.Name = "slogDataNadaniaWgSAPTextBox"
        Me.slogDataNadaniaWgSAPTextBox.ReadOnly = True
        Me.slogDataNadaniaWgSAPTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogDataNadaniaWgSAPTextBox.TabIndex = 100
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(30, 160)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(112, 13)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Data nadania wg SAP"
        '
        'slogDataUtworzeniaRekorduTextBox
        '
        Me.slogDataUtworzeniaRekorduTextBox.Location = New System.Drawing.Point(167, 183)
        Me.slogDataUtworzeniaRekorduTextBox.Name = "slogDataUtworzeniaRekorduTextBox"
        Me.slogDataUtworzeniaRekorduTextBox.ReadOnly = True
        Me.slogDataUtworzeniaRekorduTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogDataUtworzeniaRekorduTextBox.TabIndex = 100
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(30, 186)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(123, 13)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Data utworzenia rekordu"
        '
        'slogDataNadaniaRealnieTextBox
        '
        Me.slogDataNadaniaRealnieTextBox.Location = New System.Drawing.Point(167, 209)
        Me.slogDataNadaniaRealnieTextBox.Name = "slogDataNadaniaRealnieTextBox"
        Me.slogDataNadaniaRealnieTextBox.ReadOnly = True
        Me.slogDataNadaniaRealnieTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogDataNadaniaRealnieTextBox.TabIndex = 100
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(30, 212)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(105, 13)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Data nadania realnie"
        '
        'slogTypUslugiTextBox
        '
        Me.slogTypUslugiTextBox.Location = New System.Drawing.Point(167, 235)
        Me.slogTypUslugiTextBox.Name = "slogTypUslugiTextBox"
        Me.slogTypUslugiTextBox.ReadOnly = True
        Me.slogTypUslugiTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogTypUslugiTextBox.TabIndex = 100
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(30, 238)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(57, 13)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Typ usługi"
        '
        'slogKodyUslugDodatkowychTextBox1
        '
        Me.slogKodyUslugDodatkowychTextBox1.Location = New System.Drawing.Point(167, 261)
        Me.slogKodyUslugDodatkowychTextBox1.Name = "slogKodyUslugDodatkowychTextBox1"
        Me.slogKodyUslugDodatkowychTextBox1.ReadOnly = True
        Me.slogKodyUslugDodatkowychTextBox1.Size = New System.Drawing.Size(154, 20)
        Me.slogKodyUslugDodatkowychTextBox1.TabIndex = 100
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(30, 264)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(128, 13)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "Kody usług dodatkowych"
        '
        'slogPobranieTextBox
        '
        Me.slogPobranieTextBox.Location = New System.Drawing.Point(167, 287)
        Me.slogPobranieTextBox.Name = "slogPobranieTextBox"
        Me.slogPobranieTextBox.ReadOnly = True
        Me.slogPobranieTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogPobranieTextBox.TabIndex = 100
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(30, 290)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(49, 13)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Pobranie"
        '
        'slogKwotaPobraniaTextBox
        '
        Me.slogKwotaPobraniaTextBox.Location = New System.Drawing.Point(167, 313)
        Me.slogKwotaPobraniaTextBox.Name = "slogKwotaPobraniaTextBox"
        Me.slogKwotaPobraniaTextBox.ReadOnly = True
        Me.slogKwotaPobraniaTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogKwotaPobraniaTextBox.TabIndex = 100
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(30, 316)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(81, 13)
        Me.Label12.TabIndex = 1
        Me.Label12.Text = "Kwota pobrania"
        '
        'slogRodzajPaczkiTextBox
        '
        Me.slogRodzajPaczkiTextBox.Location = New System.Drawing.Point(167, 339)
        Me.slogRodzajPaczkiTextBox.Name = "slogRodzajPaczkiTextBox"
        Me.slogRodzajPaczkiTextBox.ReadOnly = True
        Me.slogRodzajPaczkiTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogRodzajPaczkiTextBox.TabIndex = 100
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(30, 342)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(74, 13)
        Me.Label13.TabIndex = 1
        Me.Label13.Text = "Rodzaj paczki"
        '
        'slogRodzajPaletyTextBox
        '
        Me.slogRodzajPaletyTextBox.Location = New System.Drawing.Point(167, 365)
        Me.slogRodzajPaletyTextBox.Name = "slogRodzajPaletyTextBox"
        Me.slogRodzajPaletyTextBox.ReadOnly = True
        Me.slogRodzajPaletyTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogRodzajPaletyTextBox.TabIndex = 100
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(30, 368)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(71, 13)
        Me.Label14.TabIndex = 1
        Me.Label14.Text = "Rodzaj palety"
        '
        'slogPaletaDlugoscTextBox
        '
        Me.slogPaletaDlugoscTextBox.Location = New System.Drawing.Point(167, 391)
        Me.slogPaletaDlugoscTextBox.Name = "slogPaletaDlugoscTextBox"
        Me.slogPaletaDlugoscTextBox.ReadOnly = True
        Me.slogPaletaDlugoscTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogPaletaDlugoscTextBox.TabIndex = 100
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(30, 394)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(87, 13)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "Paleta - Długość"
        '
        'slogPaletaSzerokoscTextBox
        '
        Me.slogPaletaSzerokoscTextBox.Location = New System.Drawing.Point(167, 417)
        Me.slogPaletaSzerokoscTextBox.Name = "slogPaletaSzerokoscTextBox"
        Me.slogPaletaSzerokoscTextBox.ReadOnly = True
        Me.slogPaletaSzerokoscTextBox.Size = New System.Drawing.Size(154, 20)
        Me.slogPaletaSzerokoscTextBox.TabIndex = 100
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(30, 420)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(96, 13)
        Me.Label16.TabIndex = 1
        Me.Label16.Text = "Paleta - Szerokość"
        '
        'slogPaletaWysokoscTextBox
        '
        Me.slogPaletaWysokoscTextBox.Location = New System.Drawing.Point(504, 27)
        Me.slogPaletaWysokoscTextBox.Name = "slogPaletaWysokoscTextBox"
        Me.slogPaletaWysokoscTextBox.ReadOnly = True
        Me.slogPaletaWysokoscTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogPaletaWysokoscTextBox.TabIndex = 100
        '
        'slogPaczkaNiestandardowaTextBox
        '
        Me.slogPaczkaNiestandardowaTextBox.Location = New System.Drawing.Point(504, 53)
        Me.slogPaczkaNiestandardowaTextBox.Name = "slogPaczkaNiestandardowaTextBox"
        Me.slogPaczkaNiestandardowaTextBox.ReadOnly = True
        Me.slogPaczkaNiestandardowaTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogPaczkaNiestandardowaTextBox.TabIndex = 100
        '
        'slogWagaTextBox
        '
        Me.slogWagaTextBox.Location = New System.Drawing.Point(504, 79)
        Me.slogWagaTextBox.Name = "slogWagaTextBox"
        Me.slogWagaTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogWagaTextBox.TabIndex = 1
        '
        'slogGabarytTextBox
        '
        Me.slogGabarytTextBox.Location = New System.Drawing.Point(504, 105)
        Me.slogGabarytTextBox.Name = "slogGabarytTextBox"
        Me.slogGabarytTextBox.ReadOnly = True
        Me.slogGabarytTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogGabarytTextBox.TabIndex = 100
        '
        'slogNrListuTextBox
        '
        Me.slogNrListuTextBox.Location = New System.Drawing.Point(504, 131)
        Me.slogNrListuTextBox.Name = "slogNrListuTextBox"
        Me.slogNrListuTextBox.ReadOnly = True
        Me.slogNrListuTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogNrListuTextBox.TabIndex = 100
        '
        'slogNrReferencyjnyTextBox
        '
        Me.slogNrReferencyjnyTextBox.Location = New System.Drawing.Point(504, 157)
        Me.slogNrReferencyjnyTextBox.Name = "slogNrReferencyjnyTextBox"
        Me.slogNrReferencyjnyTextBox.ReadOnly = True
        Me.slogNrReferencyjnyTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogNrReferencyjnyTextBox.TabIndex = 100
        '
        'slogNrPaczkiTextBox
        '
        Me.slogNrPaczkiTextBox.Location = New System.Drawing.Point(504, 183)
        Me.slogNrPaczkiTextBox.Name = "slogNrPaczkiTextBox"
        Me.slogNrPaczkiTextBox.ReadOnly = True
        Me.slogNrPaczkiTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogNrPaczkiTextBox.TabIndex = 100
        '
        'slogNrPaczkiWiodacejTextBox
        '
        Me.slogNrPaczkiWiodacejTextBox.Location = New System.Drawing.Point(504, 209)
        Me.slogNrPaczkiWiodacejTextBox.Name = "slogNrPaczkiWiodacejTextBox"
        Me.slogNrPaczkiWiodacejTextBox.ReadOnly = True
        Me.slogNrPaczkiWiodacejTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogNrPaczkiWiodacejTextBox.TabIndex = 100
        '
        'slogOdbiorcaNazwaTextBox
        '
        Me.slogOdbiorcaNazwaTextBox.Location = New System.Drawing.Point(504, 235)
        Me.slogOdbiorcaNazwaTextBox.Name = "slogOdbiorcaNazwaTextBox"
        Me.slogOdbiorcaNazwaTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogOdbiorcaNazwaTextBox.TabIndex = 2
        '
        'slogOdbiorcaOsobaTextBox
        '
        Me.slogOdbiorcaOsobaTextBox.Location = New System.Drawing.Point(504, 261)
        Me.slogOdbiorcaOsobaTextBox.Name = "slogOdbiorcaOsobaTextBox"
        Me.slogOdbiorcaOsobaTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogOdbiorcaOsobaTextBox.TabIndex = 3
        '
        'slogOdbiorcaAdresTextBox
        '
        Me.slogOdbiorcaAdresTextBox.Location = New System.Drawing.Point(504, 287)
        Me.slogOdbiorcaAdresTextBox.Name = "slogOdbiorcaAdresTextBox"
        Me.slogOdbiorcaAdresTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogOdbiorcaAdresTextBox.TabIndex = 4
        '
        'slogOdbiorcaMiastoTextBox
        '
        Me.slogOdbiorcaMiastoTextBox.Location = New System.Drawing.Point(504, 313)
        Me.slogOdbiorcaMiastoTextBox.Name = "slogOdbiorcaMiastoTextBox"
        Me.slogOdbiorcaMiastoTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogOdbiorcaMiastoTextBox.TabIndex = 5
        '
        'slogOdbiorcaKodPocztowyTextBox
        '
        Me.slogOdbiorcaKodPocztowyTextBox.Location = New System.Drawing.Point(504, 339)
        Me.slogOdbiorcaKodPocztowyTextBox.Name = "slogOdbiorcaKodPocztowyTextBox"
        Me.slogOdbiorcaKodPocztowyTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogOdbiorcaKodPocztowyTextBox.TabIndex = 6
        '
        'slogOdbiorcaTelefonTextBox
        '
        Me.slogOdbiorcaTelefonTextBox.Location = New System.Drawing.Point(504, 365)
        Me.slogOdbiorcaTelefonTextBox.Name = "slogOdbiorcaTelefonTextBox"
        Me.slogOdbiorcaTelefonTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogOdbiorcaTelefonTextBox.TabIndex = 7
        '
        'slogOdbiorcaMailTextBox
        '
        Me.slogOdbiorcaMailTextBox.Location = New System.Drawing.Point(504, 391)
        Me.slogOdbiorcaMailTextBox.Name = "slogOdbiorcaMailTextBox"
        Me.slogOdbiorcaMailTextBox.Size = New System.Drawing.Size(155, 20)
        Me.slogOdbiorcaMailTextBox.TabIndex = 8
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(367, 30)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(96, 13)
        Me.Label17.TabIndex = 1
        Me.Label17.Text = "Paleta - Wysokość"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(367, 56)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(121, 13)
        Me.Label18.TabIndex = 1
        Me.Label18.Text = "Paczka niestandardowa"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(367, 82)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(36, 13)
        Me.Label19.TabIndex = 1
        Me.Label19.Text = "Waga"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(367, 108)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(44, 13)
        Me.Label20.TabIndex = 1
        Me.Label20.Text = "Gabaryt"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(367, 134)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(39, 13)
        Me.Label21.TabIndex = 1
        Me.Label21.Text = "Nr listu"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(367, 160)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(78, 13)
        Me.Label22.TabIndex = 1
        Me.Label22.Text = "Nr referencyjny"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(367, 186)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(52, 13)
        Me.Label23.TabIndex = 1
        Me.Label23.Text = "Nr paczki"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(367, 212)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(97, 13)
        Me.Label24.TabIndex = 1
        Me.Label24.Text = "Nr paczki wiodącej"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(367, 238)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(92, 13)
        Me.Label25.TabIndex = 1
        Me.Label25.Text = "Odbiorca - Nazwa"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(367, 264)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(90, 13)
        Me.Label26.TabIndex = 1
        Me.Label26.Text = "Odbiorca - Osoba"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(367, 290)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(86, 13)
        Me.Label27.TabIndex = 1
        Me.Label27.Text = "Odbiorca - Adres"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(367, 316)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(90, 13)
        Me.Label28.TabIndex = 1
        Me.Label28.Text = "Odbiorca - Miasto"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(367, 342)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(126, 13)
        Me.Label29.TabIndex = 1
        Me.Label29.Text = "Odbiorca - Kod pocztowy"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(367, 368)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(95, 13)
        Me.Label30.TabIndex = 1
        Me.Label30.Text = "Odbiorca - Telefon"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(367, 394)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(78, 13)
        Me.Label31.TabIndex = 1
        Me.Label31.Text = "Odbiorca - Mail"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(704, 30)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(103, 13)
        Me.Label32.TabIndex = 1
        Me.Label32.Text = "Żądanie anulowania"
        '
        'slogAnulowaneTextBox
        '
        Me.slogAnulowaneTextBox.Location = New System.Drawing.Point(884, 53)
        Me.slogAnulowaneTextBox.Name = "slogAnulowaneTextBox"
        Me.slogAnulowaneTextBox.ReadOnly = True
        Me.slogAnulowaneTextBox.Size = New System.Drawing.Size(205, 20)
        Me.slogAnulowaneTextBox.TabIndex = 100
        '
        'slogDataAnulowaniaTextBox
        '
        Me.slogDataAnulowaniaTextBox.Location = New System.Drawing.Point(884, 79)
        Me.slogDataAnulowaniaTextBox.Name = "slogDataAnulowaniaTextBox"
        Me.slogDataAnulowaniaTextBox.ReadOnly = True
        Me.slogDataAnulowaniaTextBox.Size = New System.Drawing.Size(205, 20)
        Me.slogDataAnulowaniaTextBox.TabIndex = 100
        '
        'slogPlatnoscZaPrzesylkeTextBox
        '
        Me.slogPlatnoscZaPrzesylkeTextBox.Location = New System.Drawing.Point(884, 105)
        Me.slogPlatnoscZaPrzesylkeTextBox.Name = "slogPlatnoscZaPrzesylkeTextBox"
        Me.slogPlatnoscZaPrzesylkeTextBox.ReadOnly = True
        Me.slogPlatnoscZaPrzesylkeTextBox.Size = New System.Drawing.Size(205, 20)
        Me.slogPlatnoscZaPrzesylkeTextBox.TabIndex = 100
        '
        'slogNrKlientaPlacTextBox
        '
        Me.slogNrKlientaPlacTextBox.Location = New System.Drawing.Point(884, 131)
        Me.slogNrKlientaPlacTextBox.Name = "slogNrKlientaPlacTextBox"
        Me.slogNrKlientaPlacTextBox.ReadOnly = True
        Me.slogNrKlientaPlacTextBox.Size = New System.Drawing.Size(205, 20)
        Me.slogNrKlientaPlacTextBox.TabIndex = 100
        '
        'slogPIStatusTextBox
        '
        Me.slogPIStatusTextBox.Location = New System.Drawing.Point(884, 157)
        Me.slogPIStatusTextBox.Name = "slogPIStatusTextBox"
        Me.slogPIStatusTextBox.ReadOnly = True
        Me.slogPIStatusTextBox.Size = New System.Drawing.Size(205, 20)
        Me.slogPIStatusTextBox.TabIndex = 100
        '
        'slogPIDataTextBox
        '
        Me.slogPIDataTextBox.Location = New System.Drawing.Point(884, 183)
        Me.slogPIDataTextBox.Name = "slogPIDataTextBox"
        Me.slogPIDataTextBox.ReadOnly = True
        Me.slogPIDataTextBox.Size = New System.Drawing.Size(205, 20)
        Me.slogPIDataTextBox.TabIndex = 100
        '
        'slogOpisBleduTextBox
        '
        Me.slogOpisBleduTextBox.Location = New System.Drawing.Point(884, 209)
        Me.slogOpisBleduTextBox.Multiline = True
        Me.slogOpisBleduTextBox.Name = "slogOpisBleduTextBox"
        Me.slogOpisBleduTextBox.ReadOnly = True
        Me.slogOpisBleduTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.slogOpisBleduTextBox.Size = New System.Drawing.Size(205, 72)
        Me.slogOpisBleduTextBox.TabIndex = 100
        '
        'slogPotwierdzenieWyjsciaZMagTextBox
        '
        Me.slogPotwierdzenieWyjsciaZMagTextBox.Location = New System.Drawing.Point(884, 287)
        Me.slogPotwierdzenieWyjsciaZMagTextBox.Name = "slogPotwierdzenieWyjsciaZMagTextBox"
        Me.slogPotwierdzenieWyjsciaZMagTextBox.ReadOnly = True
        Me.slogPotwierdzenieWyjsciaZMagTextBox.Size = New System.Drawing.Size(205, 20)
        Me.slogPotwierdzenieWyjsciaZMagTextBox.TabIndex = 100
        '
        'slogPotwierdzenieOsobaTextBox
        '
        Me.slogPotwierdzenieOsobaTextBox.Location = New System.Drawing.Point(884, 313)
        Me.slogPotwierdzenieOsobaTextBox.Name = "slogPotwierdzenieOsobaTextBox"
        Me.slogPotwierdzenieOsobaTextBox.ReadOnly = True
        Me.slogPotwierdzenieOsobaTextBox.Size = New System.Drawing.Size(205, 20)
        Me.slogPotwierdzenieOsobaTextBox.TabIndex = 100
        '
        'slogPotwierdzenieDataTextBox
        '
        Me.slogPotwierdzenieDataTextBox.Location = New System.Drawing.Point(884, 339)
        Me.slogPotwierdzenieDataTextBox.Name = "slogPotwierdzenieDataTextBox"
        Me.slogPotwierdzenieDataTextBox.ReadOnly = True
        Me.slogPotwierdzenieDataTextBox.Size = New System.Drawing.Size(205, 20)
        Me.slogPotwierdzenieDataTextBox.TabIndex = 100
        '
        'slogModyfikacjaOsobaTextBox
        '
        Me.slogModyfikacjaOsobaTextBox.Location = New System.Drawing.Point(884, 365)
        Me.slogModyfikacjaOsobaTextBox.Name = "slogModyfikacjaOsobaTextBox"
        Me.slogModyfikacjaOsobaTextBox.ReadOnly = True
        Me.slogModyfikacjaOsobaTextBox.Size = New System.Drawing.Size(205, 20)
        Me.slogModyfikacjaOsobaTextBox.TabIndex = 100
        '
        'slogModyfikacjaDataTextBox
        '
        Me.slogModyfikacjaDataTextBox.Location = New System.Drawing.Point(884, 391)
        Me.slogModyfikacjaDataTextBox.Name = "slogModyfikacjaDataTextBox"
        Me.slogModyfikacjaDataTextBox.ReadOnly = True
        Me.slogModyfikacjaDataTextBox.Size = New System.Drawing.Size(205, 20)
        Me.slogModyfikacjaDataTextBox.TabIndex = 100
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(704, 56)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(60, 13)
        Me.Label33.TabIndex = 1
        Me.Label33.Text = "Anulowane"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(704, 82)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(87, 13)
        Me.Label34.TabIndex = 1
        Me.Label34.Text = "Data anulowania"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(704, 108)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(113, 13)
        Me.Label35.TabIndex = 1
        Me.Label35.Text = "Płatność za przesyłkę"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(704, 134)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(77, 13)
        Me.Label36.TabIndex = 1
        Me.Label36.Text = "Nr klienta płac"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(704, 160)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(50, 13)
        Me.Label37.TabIndex = 1
        Me.Label37.Text = "PI Status"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(704, 186)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(43, 13)
        Me.Label38.TabIndex = 1
        Me.Label38.Text = "PI Data"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(704, 212)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(59, 13)
        Me.Label39.TabIndex = 1
        Me.Label39.Text = "Opis błędu"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(704, 290)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(169, 13)
        Me.Label40.TabIndex = 1
        Me.Label40.Text = "Potwierdzenie wyjścia z magazynu"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(704, 316)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(113, 13)
        Me.Label41.TabIndex = 1
        Me.Label41.Text = "Potwierdzenie - Osoba"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(704, 342)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(105, 13)
        Me.Label42.TabIndex = 1
        Me.Label42.Text = "Potwierdzenie - Data"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(704, 368)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(104, 13)
        Me.Label43.TabIndex = 1
        Me.Label43.Text = "Modyfikacja - Osoba"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(704, 394)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(96, 13)
        Me.Label44.TabIndex = 1
        Me.Label44.Text = "Modyfikacja - Data"
        '
        'slogZamknijButton
        '
        Me.slogZamknijButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.slogZamknijButton.Location = New System.Drawing.Point(949, 429)
        Me.slogZamknijButton.Name = "slogZamknijButton"
        Me.slogZamknijButton.Size = New System.Drawing.Size(166, 38)
        Me.slogZamknijButton.TabIndex = 11
        Me.slogZamknijButton.Text = "Zamknij"
        Me.slogZamknijButton.UseVisualStyleBackColor = True
        '
        'slogZadanieAnulowaniaCheckBox
        '
        Me.slogZadanieAnulowaniaCheckBox.AutoSize = True
        Me.slogZadanieAnulowaniaCheckBox.Location = New System.Drawing.Point(884, 29)
        Me.slogZadanieAnulowaniaCheckBox.Name = "slogZadanieAnulowaniaCheckBox"
        Me.slogZadanieAnulowaniaCheckBox.Size = New System.Drawing.Size(15, 14)
        Me.slogZadanieAnulowaniaCheckBox.TabIndex = 9
        Me.slogZadanieAnulowaniaCheckBox.UseVisualStyleBackColor = True
        '
        'ZapiszButton
        '
        Me.ZapiszButton.Enabled = False
        Me.ZapiszButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.ZapiszButton.Location = New System.Drawing.Point(777, 429)
        Me.ZapiszButton.Name = "ZapiszButton"
        Me.ZapiszButton.Size = New System.Drawing.Size(166, 38)
        Me.ZapiszButton.TabIndex = 10
        Me.ZapiszButton.Text = "Zapisz"
        Me.ZapiszButton.UseVisualStyleBackColor = True
        '
        'PaczkaSzczegolyForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1127, 482)
        Me.Controls.Add(Me.slogZadanieAnulowaniaCheckBox)
        Me.Controls.Add(Me.ZapiszButton)
        Me.Controls.Add(Me.slogZamknijButton)
        Me.Controls.Add(Me.Label32)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label44)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label43)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label42)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label41)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label40)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label39)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label38)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label37)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label35)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label34)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label33)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.slogPaletaSzerokoscTextBox)
        Me.Controls.Add(Me.slogOdbiorcaMailTextBox)
        Me.Controls.Add(Me.slogPaletaDlugoscTextBox)
        Me.Controls.Add(Me.slogOdbiorcaTelefonTextBox)
        Me.Controls.Add(Me.slogRodzajPaletyTextBox)
        Me.Controls.Add(Me.slogOdbiorcaKodPocztowyTextBox)
        Me.Controls.Add(Me.slogRodzajPaczkiTextBox)
        Me.Controls.Add(Me.slogModyfikacjaDataTextBox)
        Me.Controls.Add(Me.slogOdbiorcaMiastoTextBox)
        Me.Controls.Add(Me.slogKwotaPobraniaTextBox)
        Me.Controls.Add(Me.slogModyfikacjaOsobaTextBox)
        Me.Controls.Add(Me.slogOdbiorcaAdresTextBox)
        Me.Controls.Add(Me.slogPobranieTextBox)
        Me.Controls.Add(Me.slogPotwierdzenieDataTextBox)
        Me.Controls.Add(Me.slogOdbiorcaOsobaTextBox)
        Me.Controls.Add(Me.slogKodyUslugDodatkowychTextBox1)
        Me.Controls.Add(Me.slogPotwierdzenieOsobaTextBox)
        Me.Controls.Add(Me.slogOdbiorcaNazwaTextBox)
        Me.Controls.Add(Me.slogTypUslugiTextBox)
        Me.Controls.Add(Me.slogPotwierdzenieWyjsciaZMagTextBox)
        Me.Controls.Add(Me.slogNrPaczkiWiodacejTextBox)
        Me.Controls.Add(Me.slogDataNadaniaRealnieTextBox)
        Me.Controls.Add(Me.slogOpisBleduTextBox)
        Me.Controls.Add(Me.slogNrPaczkiTextBox)
        Me.Controls.Add(Me.slogDataUtworzeniaRekorduTextBox)
        Me.Controls.Add(Me.slogPIDataTextBox)
        Me.Controls.Add(Me.slogNrReferencyjnyTextBox)
        Me.Controls.Add(Me.slogDataNadaniaWgSAPTextBox)
        Me.Controls.Add(Me.slogPIStatusTextBox)
        Me.Controls.Add(Me.slogNrListuTextBox)
        Me.Controls.Add(Me.slogStanowiskoTextBox)
        Me.Controls.Add(Me.slogNrKlientaPlacTextBox)
        Me.Controls.Add(Me.slogGabarytTextBox)
        Me.Controls.Add(Me.slogSpedytorTextBox)
        Me.Controls.Add(Me.slogPlatnoscZaPrzesylkeTextBox)
        Me.Controls.Add(Me.slogWagaTextBox)
        Me.Controls.Add(Me.slogStatusTextBox)
        Me.Controls.Add(Me.slogDataAnulowaniaTextBox)
        Me.Controls.Add(Me.slogPaczkaNiestandardowaTextBox)
        Me.Controls.Add(Me.slogZrealizowaneTextBox)
        Me.Controls.Add(Me.slogAnulowaneTextBox)
        Me.Controls.Add(Me.slogPaletaWysokoscTextBox)
        Me.Controls.Add(Me.slogIdTextBox)
        Me.Name = "PaczkaSzczegolyForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Szczegóły paczki"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents slogIdTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents slogZrealizowaneTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents slogStatusTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents slogSpedytorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents slogStanowiskoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents slogDataNadaniaWgSAPTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents slogDataUtworzeniaRekorduTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents slogDataNadaniaRealnieTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents slogTypUslugiTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents slogKodyUslugDodatkowychTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents slogPobranieTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents slogKwotaPobraniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents slogRodzajPaczkiTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents slogRodzajPaletyTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents slogPaletaDlugoscTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents slogPaletaSzerokoscTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents slogPaletaWysokoscTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogPaczkaNiestandardowaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogWagaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogGabarytTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogNrListuTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogNrReferencyjnyTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogNrPaczkiTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogNrPaczkiWiodacejTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogOdbiorcaNazwaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogOdbiorcaOsobaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogOdbiorcaAdresTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogOdbiorcaMiastoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogOdbiorcaKodPocztowyTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogOdbiorcaTelefonTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogOdbiorcaMailTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents slogAnulowaneTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogDataAnulowaniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogPlatnoscZaPrzesylkeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogNrKlientaPlacTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogPIStatusTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogPIDataTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogOpisBleduTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogPotwierdzenieWyjsciaZMagTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogPotwierdzenieOsobaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogPotwierdzenieDataTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogModyfikacjaOsobaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents slogModyfikacjaDataTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents slogZamknijButton As System.Windows.Forms.Button
    Friend WithEvents slogZadanieAnulowaniaCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents ZapiszButton As System.Windows.Forms.Button
End Class
