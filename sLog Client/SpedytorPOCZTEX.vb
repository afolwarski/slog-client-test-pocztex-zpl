﻿Imports System.IO
Imports System.Xml.Serialization
Imports NowaEra.Slog.PocztaPolska

Public Class SpedytorPOCZTEX
    Inherits Spedytor

    Public Overrides Sub AnulujPrzesylke(paczki As IEnumerable(Of PaczkaExtended))
        Dim config = UzupełnijKonfigurację(paczki.FirstOrDefault().Stanowisko)
        Dim flag = ElektronicznyNadawca.RemoveShipmentFromBufor(paczki, config)
    End Sub

    Public Overrides Sub UtworzPrzesylke(paczki As IEnumerable(Of PaczkaExtended))
        Dim config = UzupełnijKonfigurację(paczki.FirstOrDefault().Stanowisko)
        Dim flag = ElektronicznyNadawca.Create(paczki, config)
    End Sub

    Private Function UzupełnijKonfigurację(stanowisko As String) As AdresNadawcy

        Dim config = New AdresNadawcy()
        config.NadawcaNazwa = Konfiguracja.NadawcaNazwa
        config.NadawcaUlica = Konfiguracja.NadawcaUlica
        config.NadawcaNrBudynku = Konfiguracja.NadawcaNrBudynku
        config.NadawcaNrLokalu = Konfiguracja.NadawcaNrLokalu
        config.NadawcaKodPocztowy = Konfiguracja.NadawcaKodPocztowy
        config.NadawcaMiejscowosc = Konfiguracja.NadawcaMiejscowosc
        'config.NazwaDrukarki = Konfiguracja.Stanowiska(stanowisko)("NazwaDrukarki")
        config.COD_nr_konta = Konfiguracja.COD_nr_konta
        config.login = Konfiguracja.PP_login
        config.pass = Konfiguracja.PP_pass
        config.url = Konfiguracja.PP_Api_Url

        Return config
    End Function

    Public Overrides Sub PobierzEtykiete(paczka As Paczka)

    End Sub

    Public Overloads Overrides Sub PobierzEtykiety(paczki As IEnumerable(Of Paczka))

    End Sub
End Class