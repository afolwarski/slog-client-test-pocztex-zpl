﻿Public Class pobierzDaneZreferncji

    Private Sub btnAnuluj_Click(sender As Object, e As EventArgs) Handles btnAnuluj.Click

        Me.DialogResult = Windows.Forms.DialogResult.Cancel

        Me.Close()

    End Sub

    Private Sub btnOk_Click(sender As Object, e As EventArgs) Handles btnOk.Click
        If Me.tbReferencja.Text.Length > 0 Or Me.tbNrListu.Text.Length > 0 Then

            pobierzDaneZreferencjiService.pobierzDaneZbazy(Me.tbReferencja.Text.Trim, Me.tbNrListu.Text.Trim)

            Me.DialogResult = Windows.Forms.DialogResult.OK

            Me.Close()

        Else

            MsgBox("Wpisz numer referencyjny.", MsgBoxStyle.OkOnly)

        End If

    End Sub
End Class