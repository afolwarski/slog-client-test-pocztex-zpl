﻿Public MustInherit Class Spedytor
    Public MustOverride Sub UtworzPrzesylke(paczki As IEnumerable(Of PaczkaExtended))
    Public MustOverride Sub PobierzEtykiety(paczki As IEnumerable(Of Paczka))
    Public MustOverride Sub PobierzEtykiete(paczka As Paczka)
    Public MustOverride Sub AnulujPrzesylke(paczki As IEnumerable(Of PaczkaExtended))

    Private Shared Property DHL As New SpedytorDHL()
    Private Shared Property GLS As New SpedytorGLS()
    Private Shared Property Inpost As New SpedytorInpostKurier()
    Private Shared Property POCZTEX As New SpedytorPOCZTEX()
    Private Shared Property INNY As New SpedytorINNY()

    Public Shared Function Instancja(paczki As IEnumerable(Of Paczka)) As Spedytor
        If Not paczki Is Nothing AndAlso paczki.Count() > 0 Then
            Return Instancja(paczki(0))
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function Instancja(paczka As Paczka) As Spedytor
        If Not paczka Is Nothing Then
            If paczka.Spedytor = "DHL" Then
                Return DHL
            ElseIf paczka.Spedytor = "GLS" Then
                Return GLS
            ElseIf paczka.Spedytor = "INPOST KURIER" Then
                Return Inpost
            ElseIf paczka.Spedytor = "POCZTEX" Then
                Return POCZTEX
            Else
                Return INNY
            End If
        Else
            Return Nothing
        End If
    End Function
End Class
