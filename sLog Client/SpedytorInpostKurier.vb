﻿Imports NowaEra.Slog.InPost
Public Class SpedytorInpostKurier
    Inherits Spedytor

    Public Overrides Sub AnulujPrzesylke(paczki As IEnumerable(Of PaczkaExtended))

    End Sub

    Public Overrides Sub PobierzEtykiete(paczka As Paczka)

    End Sub

    Public Overrides Sub PobierzEtykiety(paczki As IEnumerable(Of Paczka))

    End Sub

    Public Overrides Sub UtworzPrzesylke(paczki As IEnumerable(Of PaczkaExtended))
        Dim sender = WyodrebnijAdresNadawcy()
        Dim flag = TruckerApiKurier.Create(paczki, sender, Konfiguracja.INPOST_KURIER_login, Konfiguracja.INPOST_KURIER_pass, Konfiguracja.INPOST_KURIER_url)

    End Sub
    Private Function WyodrebnijAdresNadawcy() As AdresNadawcy
        Dim senderAddress = New AdresNadawcy()
        senderAddress.NadawcaNazwa = Konfiguracja.NadawcaNazwa
        senderAddress.NadawcaUlica = Konfiguracja.NadawcaUlica
        senderAddress.NadawcaNrBudynku = Konfiguracja.NadawcaNrBudynku
        senderAddress.NadawcaNrLokalu = Konfiguracja.NadawcaNrLokalu
        senderAddress.NadawcaKodPocztowy = Konfiguracja.NadawcaKodPocztowy
        senderAddress.NadawcaMiejscowosc = Konfiguracja.NadawcaMiejscowosc
        Return senderAddress
    End Function

End Class
