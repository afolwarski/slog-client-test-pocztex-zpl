﻿Imports System.Data.SqlClient

Public Class PaczkaExtended
    Inherits Paczka

    'Public Property Id As Integer

    Public Property OdbiorcaNazwa As String = ""
    Public Property OdbiorcaUlica As String = ""
    Public Property OdbiorcaNrBudynku As String = ""
    Public Property OdbiorcaNrLokalu As String = ""
    Public Property OdbiorcaKodPocztowy As String = ""
    Public Property OdbiorcaMiejscowosc As String = ""
    Public Property OdbiorcaOsoba As String = ""
    Public Property OdbiorcaTelefon As String = ""
    Public Property OdbiorcaEmail As String = ""

    Public Property Typ As String = ""
    Public Property Dlugosc As Double? = Nothing
    Public Property Szerokosc As Double? = Nothing
    Public Property Wysokosc As Double? = Nothing
    Public Property Waga As Double? = Nothing

    Public Property Zawartosc As String = "Książki"

    Public Property WartoscZwrotuPobrania As Double? = Nothing

    Public Property UslugiDodatkowe As String() = Nothing

    'Public Property NrListu As String = Nothing
    'Public Property NrRef As String = Nothing
    'Public Property NrPaczki As String = Nothing
    'Public Property NrPaczkiWiodacej As String = Nothing
    'Public Property Etykieta As Etykieta = Nothing

    'Public Property DataNadania As DateTime? = Nothing

    Public Property Anuluj As Boolean = False

    Public Property Zrealizowane As Boolean = False

    Public Property Stanowisko As String = ""

    Public ReadOnly Property CzyNadana As Boolean
        Get
            Return Not String.IsNullOrEmpty(NrListu)
        End Get
    End Property

    '************************************************************************************************

    Public Shared Function PobierzZBazy(nrRef As String) As PaczkaExtended()
        Dim paczki As New List(Of PaczkaExtended)

        Try
            Using connection As New SqlConnection(My.Settings.SLOGConnectionString)
                connection.Open()

                Using cmd As New SqlCommand()
                    cmd.Connection = connection
                    cmd.CommandText =
                        "SELECT * FROM paczki " &
                        "WHERE " &
                            "NrRef = '" & nrRef & "' AND " &
                            "(Zrealizowane IS NULL OR Zrealizowane = 0 OR " &
                            "(ZadanieAnulowania = 1 AND (Anulowane IS NULL OR Anulowane = 0) AND " &
                            "DataNadaniaReal = CAST(GETDATE() AS DATE))) " &
                        "ORDER BY ID"

                    Using reader As SqlDataReader = cmd.ExecuteReader()
                        While reader.Read()
                            Dim paczka As New PaczkaExtended()

                            Try
                                paczka.Id = CType(reader("ID"), Integer)
                                If Not IsDBNull(reader("Spedytor")) Then paczka.Spedytor = reader("Spedytor").ToString()

                                If Not IsDBNull(reader("OdbNazwa")) Then paczka.OdbiorcaNazwa = reader("OdbNazwa").ToString()
                                If Not IsDBNull(reader("OdbMiasto")) Then paczka.OdbiorcaMiejscowosc = reader("OdbMiasto").ToString()
                                If Not IsDBNull(reader("OdbKodPocztowy")) Then paczka.OdbiorcaKodPocztowy = reader("OdbKodPocztowy").ToString()

                                If Not IsDBNull(reader("OdbAdres")) Then
                                    If Not String.IsNullOrEmpty(paczka.Spedytor) AndAlso paczka.Spedytor = "DHL" Then
                                        Dim adres As New Adres(reader("OdbAdres").ToString())

                                        paczka.OdbiorcaUlica = adres.Ulica
                                        paczka.OdbiorcaNrBudynku = adres.NrBudynku
                                        paczka.OdbiorcaNrLokalu = adres.NrLokalu
                                    Else
                                        paczka.OdbiorcaUlica = reader("OdbAdres").ToString()
                                    End If

                                    'Log.WriteInformation(String.Format("Ulica: {0}" & vbCrLf & "Nr budynku: {1}" & vbCrLf & "Nr lokalu: {2}", adres.Ulica, adres.NrBudynku, adres.NrLokalu))
                                End If

                                If Not IsDBNull(reader("OdbOsoba")) Then paczka.OdbiorcaOsoba = reader("OdbOsoba").ToString()
                                If Not IsDBNull(reader("OdbTelefon")) Then paczka.OdbiorcaTelefon = reader("OdbTelefon").ToString()
                                If Not IsDBNull(reader("OdbMail")) Then paczka.OdbiorcaEmail = reader("OdbMail").ToString()

                                If Not IsDBNull(reader("PaletaDl")) Then paczka.Dlugosc = CType(reader("PaletaDl"), Double)
                                If Not IsDBNull(reader("PaletaSzer")) Then paczka.Szerokosc = CType(reader("PaletaSzer"), Double)
                                If Not IsDBNull(reader("PaletaWys")) Then paczka.Wysokosc = CType(reader("PaletaWys"), Double)
                                If Not IsDBNull(reader("Waga")) Then paczka.Waga = CType(reader("Waga"), Double)

                                If Not IsDBNull(reader("RodzajPaczki")) Then paczka.Typ = reader("RodzajPaczki")
                                If Not IsDBNull(reader("KwotaPobrania")) Then paczka.WartoscZwrotuPobrania = CType(reader("KwotaPobrania"), Double)
                                If Not IsDBNull(reader("KodyUslugDod")) Then paczka.UslugiDodatkowe = reader("KodyUslugDod").ToString().Split(",")

                                If Not IsDBNull(reader("NrListu")) Then paczka.NrListu = reader("NrListu").ToString()
                                If Not IsDBNull(reader("NrRef")) Then paczka.NrRef = reader("NrRef").ToString()
                                If Not IsDBNull(reader("NrPaczki")) Then paczka.NrPaczki = reader("NrPaczki").ToString()
                                If Not IsDBNull(reader("NrPaczkiWiodacej")) Then paczka.NrPaczkiWiodacej = reader("NrPaczkiWiodacej").ToString()

                                If Not IsDBNull(reader("DataNadaniaWgSAP")) Then paczka.DataNadania = CType(reader("DataNadaniaWgSAP"), DateTime)

                                If Not IsDBNull(reader("ZadanieAnulowania")) Then paczka.Anuluj = CType(reader("ZadanieAnulowania"), Boolean)
                            Catch ex As Exception
                                Log.WriteError(ex.ToString())
                                paczka.UstawOpisBledu("[Baza danych] " & ex.Message, 5)
                            End Try

                            paczki.Add(paczka)
                        End While
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Log.WriteError(ex.ToString())
            Log.WriteToDB(Nothing, 5, ex.ToString())
        End Try

        Return paczki.ToArray()
    End Function

    Public Sub AktualizujDaneSpedycyjne()
        SQL_UpdateQuery(String.Format("UPDATE paczki SET DataNadaniaReal = '{0}', NrListu = '{1}', OpisBledu = NULL, Etykieta = '{2}' WHERE ID = {3}",
                                      DataNadania.Value.ToString("yyyy-MM-dd"), NrListu, Etykieta.ToString().Replace("'", "''"), Id))
    End Sub

    Public Sub OznaczJakoZrealizowane()
        If SQL_UpdateQuery(String.Format("UPDATE paczki SET Zrealizowane = 1, Status = 10, PIStatus = NULL, PIData = NULL, OpisBledu = NULL WHERE ID = {0}", Id)) Then Zrealizowane = True
    End Sub

    Public Shared Function PobierzDanePaczkiZreferencji(nrReferncji As String, nrListu As String) As PaczkaExtended
        Dim paczkaTemp As New PaczkaExtended

        Dim referencjaTemp As String()
        referencjaTemp = Split(nrReferncji, "/")

        Using connection As New SqlConnection(My.Settings.SLOGConnectionString)
            connection.Open()

            Using cmd As New SqlCommand()
                cmd.Connection = connection

                If referencjaTemp.Length = 1 Then

                    cmd.CommandText = String.Format("select top 1 * from paczki where NrRef = '{0}' ", referencjaTemp(0))

                ElseIf referencjaTemp.Length > 1 Then

                    cmd.CommandText = String.Format("select top 1 * from paczki where (NrRef = '{0}' and nrPaczki = {1}) ", referencjaTemp(0), referencjaTemp(1))

                End If

                If nrListu <> "" Then

                    cmd.CommandText = String.Format("select top 1 * from paczki where NrListu = '{0}' ", nrListu)

                End If

                Using reader As SqlDataReader = cmd.ExecuteReader()

                    If reader.Read() Then

                        If Not IsDBNull(reader.Item("spedytor")) Then paczkaTemp.Spedytor = reader.Item("spedytor").ToString

                        If Not IsDBNull(reader.Item("odbNazwa")) Then paczkaTemp.OdbiorcaNazwa = reader.Item("OdbNazwa").ToString

                        If Not IsDBNull(reader("OdbAdres")) Then paczkaTemp.OdbAdres = reader.Item("odbAdres").ToString

                        If Not IsDBNull(reader.Item("OdbKodPocztowy")) Then paczkaTemp.OdbiorcaKodPocztowy = reader.Item("OdbKodPocztowy").ToString
                        If Not IsDBNull(reader.Item("OdbMiasto")) Then paczkaTemp.OdbiorcaMiejscowosc = reader.Item("OdbMiasto").ToString
                        If Not IsDBNull(reader.Item("OdbOsoba")) Then paczkaTemp.OdbiorcaOsoba = reader.Item("OdbOsoba").ToString
                        If Not IsDBNull(reader.Item("OdbTelefon")) Then paczkaTemp.OdbiorcaTelefon = reader.Item("OdbTelefon").ToString
                        If Not IsDBNull(reader.Item("OdbMail")) Then paczkaTemp.OdbiorcaEmail = reader.Item("OdbMail").ToString

                        paczkaTemp.Zawartosc = "Książki"

                    End If

                End Using

            End Using

        End Using

        Return paczkaTemp

    End Function
End Class
