﻿Imports System.Net.Sockets
Imports System.Net
Imports System.IO
Imports System.Text
Imports Microsoft.Win32.SafeHandles
Imports System.Threading

Public Class Drukarka
    Declare Auto Function CreateFile Lib "kernel32.dll" (ByVal lpFileName As String, _
        ByVal dwDesiredAccess As FileAccess, ByVal dwShareMode As UInteger, _
        ByVal lpSecurityAttributes As IntPtr, ByVal dwCreationDisposition As FileMode, _
        ByVal dwFlagsAndAttributes As Integer, ByVal hTemplateFile As IntPtr) As SafeFileHandle

    Public Shared Sub WydrukujEtykietyPDF(guids() As String)
        Dim tEN As New pocztapolska.ElektronicznyNadawca
        Dim nc As New NetworkCredential

        nc.UserName = Konfiguracja.PP_login
        nc.Password = Konfiguracja.PP_pass

        Dim cc As New CredentialCache
        cc.Add(New Uri(Konfiguracja.PP_Api_Url), "Basic", nc)

        tEN.Credentials = cc

        Dim er = New pocztapolska.errorType() {}
        Dim etykietaPDF As Byte() = New Byte() {}

        etykietaPDF = tEN.getAddresLabelByGuidCompact(guids, 0, False, er)

        Dim pdfPath As String = "C:\pocztex\poczta_polska_group_" + DateTime.Now.ToString("yyyyddMM_HHmm") + ".pdf"
        System.IO.File.WriteAllBytes(pdfPath, etykietaPDF)
        Process.Start(pdfPath)
    End Sub

    Public Shared Sub WydrukujEtykiete(paczka As PaczkaExtended)
        'Log.WriteInformation("Drukowanie etykiety...")
        
        If Not paczka.Etykieta Is Nothing Or Not paczka.EtykietaNiestandardowa Is Nothing Then
            Try
                'WydrukujEtykieteNaDrukarceUdostepnionej(paczka)
                WydrukujEtykieteNaDrukarceSieciowej(paczka)
                paczka.OznaczJakoZrealizowane()
            Catch ex As Exception
                Log.WriteError(ex.ToString())
                paczka.UstawOpisBledu("[Drukarka] " & ex.Message, 5)
            End Try
        End If
    End Sub

    Private Shared Sub WydrukujEtykieteNaDrukarceUdostepnionej(paczka As PaczkaExtended)
        Using printer As SafeFileHandle = OpenSharedPrinter()
            Using s As Stream = New FileStream(printer, FileAccess.Write)
                Using sw As New StreamWriter(s, Encoding.ASCII)
                    If Not String.IsNullOrEmpty(paczka.EtykietaInPost) Then
                        sw.WriteLine(paczka.EtykietaInPost)
                    Else
                        sw.WriteLine(paczka.Etykieta)
                    End If
                    sw.Flush()
                End Using
            End Using
        End Using
    End Sub


    Private Shared Function OpenSharedPrinter() As SafeFileHandle
        Dim printer As SafeFileHandle = Nothing
        Dim i As Integer

        Const maxAttempts As Integer = 1

        For i = 1 To maxAttempts
            printer = CreateFile("\\neko13-0076\DrukarkaTermiczna", FileAccess.Write, 0, IntPtr.Zero, FileMode.Create, 0, IntPtr.Zero)

            If printer.IsInvalid Then
                printer.Dispose()

                If i < maxAttempts Then
                    Log.WriteError("Nie udało się połączyć z drukarką (próba " & i & ").")
                    Thread.Sleep(5000)
                End If
            Else
                Exit For
            End If
        Next

        If printer.IsInvalid Then Throw New Exception("Nie udało się połączyć z drukarką.")

        Return printer
    End Function

    Private Shared Sub WydrukujEtykieteNaDrukarceSieciowej(paczka As PaczkaExtended)
        Using socket As New Net.Sockets.Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP)
            socket.Connect(Konfiguracja.DrukarkaIP, Konfiguracja.DrukarkaPort)

            If paczka.Spedytor = "POCZTEX" Then
                paczka.EtykietaNiestandardowa = paczka.EtykietaNiestandardowa.Replace("^FWR", "").Replace("^FWN", "")
            ElseIf paczka.Spedytor = "DHL" Then
                paczka.Etykieta.CorrectFont()
            End If

            Using s As Stream = New NetworkStream(socket, FileAccess.Write)
                Using sw As New StreamWriter(s, Encoding.ASCII)
                    'sw.WriteLine("^PMY")

                    If Not String.IsNullOrEmpty(paczka.EtykietaNiestandardowa) Then
                        sw.WriteLine(paczka.EtykietaNiestandardowa)
                    Else
                        sw.WriteLine(paczka.Etykieta)
                    End If


                    sw.Flush()
                End Using
            End Using
        End Using
    End Sub
End Class
