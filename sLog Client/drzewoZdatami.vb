﻿Imports System.Data.SqlClient
Public Class drzewoZdatami
    Public miesiace As Dictionary(Of Integer, String)

    Public drzewo As TreeView
    Public Sub New(aktywneDrzewo As TreeView)
        Me.drzewo = aktywneDrzewo
        tworzSlownik()
    End Sub

    Public Sub tworzSlownik()
        miesiace = New Dictionary(Of Integer, String)
        miesiace.Add(1, "styczeń")
        miesiace.Add(2, "luty")
        miesiace.Add(3, "marzec")
        miesiace.Add(4, "kwiecień")
        miesiace.Add(5, "maj")
        miesiace.Add(6, "czerwiec")
        miesiace.Add(7, "lipiec")
        miesiace.Add(8, "sierpień")
        miesiace.Add(9, "wrzesień")
        miesiace.Add(10, "październik")
        miesiace.Add(11, "listopad")
        miesiace.Add(12, "grudzień")
    End Sub

    Public Function PobierzDaty() As List(Of String)

        Dim datyWysylek As New List(Of String)

        Using connection As New SqlConnection(My.Settings.SLOGConnectionString)
            connection.Open()

            Using cmd As New SqlCommand()
                cmd.Connection = connection

                cmd.CommandText = String.Format(" select distinct DataNadaniaWgSAP " & _
                                                " from paczki " & _
                                                " where DataNadaniaWgSAP Is Not null " & _
                                                " order by DataNadaniaWgSAP ")

                Using reader As SqlDataReader = cmd.ExecuteReader()


                    While reader.Read()

                        datyWysylek.Add(reader.Item("DataNadaniaWgSAP").ToString)


                    End While

                End Using

            End Using

        End Using

        Return datyWysylek

    End Function

    Public Sub dodajRok(rok As String)
        Dim node As New TreeNode

        node.Text = rok
        node.Name = rok

        drzewo.Nodes.Add(node)

    End Sub

    Public Sub DodajMiesiac(miesiac As String, rok As Integer)
        Dim node As New TreeNode

        node.Text = miesiac
        node.Name = miesiac

        drzewo.Nodes(rok).Nodes.Add(node)

    End Sub

    Public Sub DodajDzien(miesiac As Integer, rok As Integer, dzien As Integer)
        Dim node As New TreeNode

        node.Text = dzien
        node.Name = dzien

        drzewo.Nodes(rok).Nodes(miesiac).Nodes.Add(dzien)

    End Sub
    Public Function NazwaMiesiaca(numer As Integer) As String
        Return miesiace.Item(numer)
    End Function
    Public Function NumerMiesiaca(nazwa As String) As Integer
        Dim numer As Integer = 0

        For Each Val As KeyValuePair(Of Integer, String) In miesiace
            If Val.Value = nazwa Then
                numer = Val.Key
                Exit For
            End If
        Next
        Return numer
    End Function

  
End Class
