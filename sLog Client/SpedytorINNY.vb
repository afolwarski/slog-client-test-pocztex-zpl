﻿Imports System.IO
Imports System.Xml.Serialization
Imports NowaEra.Slog.InPost

Public Class SpedytorINNY
    Inherits Spedytor

    Public Overrides Sub AnulujPrzesylke(paczki As IEnumerable(Of PaczkaExtended))
        For Each paczka In paczki
            paczka.ZatrzymajPrzetwazanie("Niepoprawny spedytor.")
        Next

    End Sub

    Public Overrides Sub PobierzEtykiete(paczka As Paczka)

        paczka.ZatrzymajPrzetwazanie("Niepoprawny spedytor.")


    End Sub

    Public Overrides Sub PobierzEtykiety(paczki As IEnumerable(Of Paczka))
        For Each paczka In paczki
            paczka.ZatrzymajPrzetwazanie("Niepoprawny spedytor.")
        Next

    End Sub

    Public Overrides Sub UtworzPrzesylke(paczki As IEnumerable(Of PaczkaExtended))
        For Each paczka In paczki
            paczka.ZatrzymajPrzetwazanie("Niepoprawny spedytor.")
        Next

    End Sub
End Class
