﻿Imports sLog_Client.gls

Public Class SpedytorGLS
    Inherits Spedytor


    Public Overrides Sub UtworzPrzesylke(paczki As IEnumerable(Of PaczkaExtended))
        Dim etykieta As Etykieta = Nothing

        'Log.WriteInformation("Tworzenie przesyłki...")
        Dim paczka As PaczkaExtended = paczki.First(Function(p) String.IsNullOrEmpty(p.NrPaczkiWiodacej))

        Try
            Using glsService As New ADEplusWebAPI()

                Dim session As Session = glsService.adeLogin(Konfiguracja.GLS_AutoryzacjaLogin, Konfiguracja.GLS_AutoryzacjaHaslo)

                Dim consign As New Consign
                'consign.references = "achachacha" 'String.Format("{0}/{1:00000}", Integer.Parse(paczka.NrRef), Integer.Parse(paczka.NrPaczki))

                If paczka.DataNadania.HasValue Then consign.date = paczka.DataNadania.Value.ToString("yyyy-MM-dd")

                consign.pfc = Konfiguracja.PlatnoscCentrumKosztow

                If Not paczka.UslugiDodatkowe Is Nothing Then
                    consign.srv_bool = New ServicesBool()

                    For Each u In paczka.UslugiDodatkowe
                        Select Case u
                            Case "COD"
                                If paczka.WartoscZwrotuPobrania.HasValue Then
                                    consign.srv_bool.cod = 1
                                    consign.srv_bool.cod_amount = String.Format("{0:0.00}", paczka.WartoscZwrotuPobrania.Value)
                                End If
                            Case "UBEZP"
                            Case Else
                        End Select
                    Next
                End If

                consign.rname1 = paczka.OdbiorcaNazwa

                If Not String.IsNullOrEmpty(paczka.OdbiorcaOsoba) Then
                    consign.rname2 = paczka.OdbiorcaOsoba
                End If

                consign.rzipcode = paczka.OdbiorcaKodPocztowy
                consign.rcity = paczka.OdbiorcaMiejscowosc

                Dim ulica As String = paczka.OdbiorcaUlica

                If Not String.IsNullOrEmpty(paczka.OdbiorcaNrBudynku) Then
                    ulica &= " " & paczka.OdbiorcaNrBudynku
                End If

                If Not String.IsNullOrEmpty(paczka.OdbiorcaNrLokalu) Then
                    ulica &= " m. " & paczka.OdbiorcaNrLokalu
                End If

                consign.rstreet = ulica
                consign.rcountry = "PL"
                consign.rphone = paczka.OdbiorcaTelefon
                consign.rcontact = paczka.OdbiorcaOsoba

                Dim parcels As New List(Of Parcel)

                For Each paczka In paczki
                    Dim jednaPaczka As New Parcel()
                    jednaPaczka.reference = String.Format("{0}/{1:00000}", Integer.Parse(paczka.NrRef), Integer.Parse(paczka.NrPaczki))
                    jednaPaczka.number = paczka.NrPaczki

                    If paczka.Waga.HasValue Then
                        jednaPaczka.weight = String.Format("{0:0.00}", paczka.Waga.Value)
                    Else
                        jednaPaczka.weight = "0,01"
                    End If

                    parcels.Add(jednaPaczka)
                Next

                consign.parcels = parcels.ToArray()

                Dim przesylkaId As ConsignID = glsService.adePreparingBox_Insert(session.session, consign)

                Dim labels As DocLabels = glsService.adePreparingBox_GetConsignLabels(session.session, przesylkaId.id, Konfiguracja.GLS_TypEtykiety)

                Dim multiEtykieta As Etykieta = New Etykieta(labels.labels)
                Dim etykiety As Etykieta() = multiEtykieta.WyodrebnijEtykietyGLS()
                Dim i As Integer = 0

                'Dim multiEtykieta As Etykieta = New Etykieta(response.label.labelContent)
                'Dim etykiety As Etykieta() = multiEtykieta.WyodrebnijEtykiety()
                'Dim i As Integer = 0

                For Each paczka In paczki
                    paczka.NrListu = etykiety(i).PobierzIdentyfikatorPaczkiGLS()
                    etykiety(i).UstawPrzesuniecieGLS()
                    paczka.Etykieta = etykiety(i)
                    paczka.Etykieta.WstawOpisZNumeracjaGLS(i + 1, paczki.Count())

                    i += 1

                    paczka.AktualizujDaneSpedycyjne()
                Next

                glsService.adeLogout(session.session)
            End Using
        Catch soapEx As System.Web.Services.Protocols.SoapException
            For Each paczka In paczki
                paczka.UstawOpisBledu("[Spedytor] " & soapEx.Code.Name & " " & soapEx.Message, 2)
            Next
        Catch ex As Exception
            For Each paczka In paczki
                paczka.UstawOpisBledu("[Spedytor] " & ex.Message, 2)
            Next
        End Try
    End Sub

    Private Shared Function NajblizszyRoboczyDzien() As DateTime
        Dim dzien As DateTime = DateTime.Today

        If dzien.DayOfWeek = DayOfWeek.Saturday Then dzien = dzien.AddDays(2)
        If dzien.DayOfWeek = DayOfWeek.Sunday Then dzien = dzien.AddDays(1)

        Return dzien
    End Function

    Public Overrides Sub PobierzEtykiety(paczki As IEnumerable(Of Paczka))
        'Log.WriteInformation("Pobieranie etykiety...")

        Try
            Using glsService As New ADEplusWebAPI()
                Dim session As Session = glsService.adeLogin(Konfiguracja.GLS_AutoryzacjaLogin, Konfiguracja.GLS_AutoryzacjaHaslo)

                Dim labels As DocLabels = glsService.adePreparingBox_GetConsignLabels(session.session, paczki(0).NrListu, Konfiguracja.GLS_TypEtykiety)
                Dim multiEtykieta As Etykieta = New Etykieta(labels.labels)
                Dim etykiety As Etykieta() = multiEtykieta.WyodrebnijEtykietyGLS()
                Dim i As Integer = 0

                For Each paczka As PaczkaExtended In paczki
                    etykiety(i).UstawPrzesuniecieGLS()
                    etykiety(i).WstawOpisZNumeracjaGLS(i + 1, paczki.Count())
                    paczka.Etykieta = etykiety(i)

                    i += 1
                Next
            End Using
        Catch soapEx As System.Web.Services.Protocols.SoapException
            For Each paczka In paczki
                paczka.UstawOpisBledu("[Spedytor] " & soapEx.Code.Name & " " & soapEx.Message, 2)
            Next
        Catch ex As Exception
            For Each paczka As PaczkaExtended In paczki
                paczka.UstawOpisBledu("[Spedytor] " & ex.Message, 2)
            Next
        End Try
    End Sub

    Public Overrides Sub PobierzEtykiete(paczka As Paczka)
        Try
            Using glsService As New ADEplusWebAPI()
                Dim session As Session = glsService.adeLogin(Konfiguracja.GLS_AutoryzacjaLogin, Konfiguracja.GLS_AutoryzacjaHaslo)

                Dim labels As DocLabels = glsService.adePreparingBox_GetConsignLabels(session.session, paczka.NrListu, Konfiguracja.GLS_TypEtykiety)
                Dim multiEtykieta As Etykieta = New Etykieta(labels.labels)
                Dim etykiety As Etykieta() = multiEtykieta.WyodrebnijEtykietyGLS()
                Dim refPaczki As String = String.Format("{0}/{1:00000}", Integer.Parse(paczka.NrRef), Integer.Parse(paczka.NrPaczki))
                Dim i As Integer = 0

                For Each etykietka As Etykieta In etykiety
                    i += 1

                    Dim refEtykiety As String = etykietka.ToString().
                        Split(New String() {"^FO112,1052,^A0N,24^FR^FD"}, StringSplitOptions.None)(1).
                        Split(New String() {"^FS"}, StringSplitOptions.None)(0)

                    If refEtykiety = refPaczki Then
                        paczka.Etykieta = etykietka
                        paczka.Etykieta.UstawPrzesuniecieGLS()
                        paczka.Etykieta.WstawOpisZNumeracjaGLS(i, etykiety.Count())
                        Exit For
                    End If
                Next
            End Using
        Catch soapEx As System.Web.Services.Protocols.SoapException
            paczka.UstawOpisBledu("[Spedytor] " & soapEx.Code.Name & " " & soapEx.Message, 2)
        Catch ex As Exception
            paczka.UstawOpisBledu("[Spedytor] " & ex.Message, 2)
        End Try
    End Sub

    Public Overrides Sub AnulujPrzesylke(paczki As IEnumerable(Of PaczkaExtended))
        'Log.WriteInformation("Anulowanie przesyłki...")

        Try
            Using glsService As New ADEplusWebAPI()
                Dim session As Session = glsService.adeLogin(Konfiguracja.GLS_AutoryzacjaLogin, Konfiguracja.GLS_AutoryzacjaHaslo)

                glsService.adePreparingBox_DeleteConsign(session.session, paczki(0).NrListu)

                For Each paczka As PaczkaExtended In paczki
                    paczka.OznaczJakoAnulowane()
                Next
            End Using
        Catch soapEx As System.Web.Services.Protocols.SoapException
            For Each paczka In paczki
                paczka.UstawOpisBledu("[Spedytor] " & soapEx.Code.Name & " " & soapEx.Message, 2)
            Next
        Catch ex As Exception
            For Each paczka As PaczkaExtended In paczki
                paczka.UstawOpisBledu("[Spedytor] " & ex.Message, 2)
            Next
        End Try
    End Sub
End Class
