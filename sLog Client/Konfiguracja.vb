﻿Imports System.Data.SqlClient

Public Class Konfiguracja
    Public Shared Property InterwalCzasu_sek As Integer = 60

    Public Shared Property DrukowanieOpoznienie_milisek As Integer = 0

    Public Shared Property DrukarkaUdzialSciezka As String = ""

    Public Shared Property COD_nr_konta As String = ""

    Public Shared Property DrukarkaIP As String = ""
    Public Shared Property DrukarkaPort As Integer = 0

    Public Shared Property SpedytorWebApiAdres As String = ""
    Public Shared Property DHL_AutoryzacjaLogin As String = ""
    Public Shared Property DHL_AutoryzacjaHaslo As String = ""

    Public Shared Property NadawcaNazwa As String = ""
    Public Shared Property NadawcaUlica As String = ""
    Public Shared Property NadawcaNrBudynku As String = ""
    Public Shared Property NadawcaNrLokalu As String = ""
    Public Shared Property NadawcaKodPocztowy As String = ""
    Public Shared Property NadawcaMiejscowosc As String = ""

    Public Shared Property ZawartoscPaczki As String = ""

    Public Shared Property PlatnoscPlatnik As String = ""
    Public Shared Property PlatnoscNumerKlienta As String = ""
    Public Shared Property PlatnoscTyp As String = ""
    Public Shared Property PlatnoscCentrumKosztow As String = ""

    Public Shared Property DHL_WebApiTypZadania As String = ""
    Public Shared Property DHL_WebApiTypUslubi As String = ""
    Public Shared Property DHL_WebApiTypEtykiety As String = ""

    Public Shared Property DHL_NadanieWGodzinachStart As String = ""
    Public Shared Property DHL_NadanieWGodzinachKoniec As String = ""

    Public Shared Property WartoscUbezpieczenia As Double = 0
    Public Shared Property FormaZwrotuPobrania As String = ""

    Public Shared Property DHL_EtykietaPrzesuniecie_X As String = ""
    Public Shared Property GLS_EtykietaPrzesuniecie_X As String = ""
    Public Shared Property GLS_EtykietaPrzesuniecie_Y As String = ""

    Public Shared Property WysylkaMasowaMinimalnaLiczbaPaczek As Integer = 100

    Public Shared Property GLS_TypEtykiety As String
    Public Shared Property GLS_AutoryzacjaLogin As String
    Public Shared Property GLS_AutoryzacjaHaslo As String

    Public Shared Property WersjaProgramu As String = ""
    Public Shared Property InstalatorProgramu As String = ""

    Private Shared parametry As New Dictionary(Of String, String)

    Private Shared _nazwaKomputera As String = Nothing
    Public Shared Property INPOST_KURIER_login As String
    Public Shared Property INPOST_KURIER_pass As String
    Public Shared Property INPOST_KURIER_url As String

    Public Shared Property PP_login As String
    Public Shared Property PP_pass As String
    Public Shared Property PP_Api_Url As String

    Public Shared ReadOnly Property NazwaKomputera As String
        Get
            If String.IsNullOrEmpty(_nazwaKomputera) Then
                _nazwaKomputera = Environment.MachineName
            End If

            Return _nazwaKomputera
        End Get
    End Property

    Public Shared Sub Odswiez()
        PobierzParametry()
        AktualizujWlasciwosci()
    End Sub

    Public Shared Sub Odswiez(nazwaKomputera As String)
        _nazwaKomputera = nazwaKomputera

        PobierzParametry()
        AktualizujWlasciwosci()

        _nazwaKomputera = Environment.MachineName
    End Sub

    Private Shared Sub PobierzParametry()
        parametry.Clear()

        Try
            Using connection As New SqlConnection(My.Settings.SLOGConnectionString)
                connection.Open()

                Using cmd As New SqlCommand()
                    cmd.Connection = connection

                    cmd.CommandText = "SELECT * FROM konfiguracja"

                    Using reader As SqlDataReader = cmd.ExecuteReader()
                        While reader.Read()
                            parametry.Add(reader("Parametr").ToString(), reader("Wartosc").ToString())
                        End While
                    End Using

                    cmd.CommandText = String.Format("SELECT * FROM stanowiska WHERE Stanowisko = '{0}'", NazwaKomputera)

                    Using reader As SqlDataReader = cmd.ExecuteReader()
                        While reader.Read()
                            parametry.Add("DrukarkaIP", reader("DrukarkaIP").ToString())
                            parametry.Add("DrukarkaPort", reader("DrukarkaPort").ToString())
                            parametry.Add("WydrukEtykietyPrzesuniecie", reader("WydrukEtykietyPrzesuniecie").ToString())
                            parametry.Add("GLS_WydrukEtykietyPrzesuniecie_X", reader("GLS_WydrukEtykietyPrzesuniecie_X").ToString())
                            parametry.Add("GLS_WydrukEtykietyPrzesuniecie_Y", reader("GLS_WydrukEtykietyPrzesuniecie_Y").ToString())
                        End While
                    End Using

                End Using
            End Using
        Catch ex As Exception
            Log.WriteError(ex.ToString())
            Log.WriteToDB(Nothing, 5, ex.ToString())
        End Try
    End Sub

    Private Shared Sub AktualizujWlasciwosci()
        If parametry.ContainsKey("COD_nr_konta") Then COD_nr_konta = parametry("COD_nr_konta")
        If parametry.ContainsKey("InterwalCzasu_sek") Then Integer.TryParse(parametry("InterwalCzasu_sek"), InterwalCzasu_sek)

        If parametry.ContainsKey("DrukowanieOpoznienie_milisek") Then Integer.TryParse(parametry("DrukowanieOpoznienie_milisek"), DrukowanieOpoznienie_milisek)
        If parametry.ContainsKey("WysylkaMasowaMinimalnaLiczbaPaczek") Then Integer.TryParse(parametry("WysylkaMasowaMinimalnaLiczbaPaczek"), WysylkaMasowaMinimalnaLiczbaPaczek)

        If parametry.ContainsKey("DrukarkaIP") Then DrukarkaIP = parametry("DrukarkaIP")
        If parametry.ContainsKey("DrukarkaPort") Then DrukarkaPort = CType(parametry("DrukarkaPort"), Integer)

        If parametry.ContainsKey("SpedytorWebApiAdres") Then SpedytorWebApiAdres = parametry("SpedytorWebApiAdres")
        If parametry.ContainsKey("AutoryzacjaLogin") Then DHL_AutoryzacjaLogin = parametry("AutoryzacjaLogin")
        If parametry.ContainsKey("AutoryzacjaPass") Then DHL_AutoryzacjaHaslo = parametry("AutoryzacjaPass")

        If parametry.ContainsKey("WydrukEtykietyPrzesuniecie") Then DHL_EtykietaPrzesuniecie_X = parametry("WydrukEtykietyPrzesuniecie")
        If parametry.ContainsKey("GLS_WydrukEtykietyPrzesuniecie_X") Then GLS_EtykietaPrzesuniecie_X = parametry("GLS_WydrukEtykietyPrzesuniecie_X")
        If parametry.ContainsKey("GLS_WydrukEtykietyPrzesuniecie_Y") Then GLS_EtykietaPrzesuniecie_Y = parametry("GLS_WydrukEtykietyPrzesuniecie_Y")

        If parametry.ContainsKey("GLS_typ_etykiety") Then GLS_TypEtykiety = parametry("GLS_typ_etykiety")
        If parametry.ContainsKey("GLS_AutoryzacjaLogin") Then GLS_AutoryzacjaLogin = parametry("GLS_AutoryzacjaLogin")
        If parametry.ContainsKey("GLS_AutoryzacjaPass") Then GLS_AutoryzacjaHaslo = parametry("GLS_AutoryzacjaPass")

        If parametry.ContainsKey("NadawcaAdres") Then
            Dim adresElements As Dictionary(Of String, String) = WyodrebnijParametrySkladowe(parametry("NadawcaAdres"))

            If adresElements.ContainsKey("Nazwa") Then NadawcaNazwa = adresElements("Nazwa")
            If adresElements.ContainsKey("Ulica") Then NadawcaUlica = adresElements("Ulica")
            If adresElements.ContainsKey("NrBudynku") Then NadawcaNrBudynku = adresElements("NrBudynku")
            If adresElements.ContainsKey("NrLokalu") Then NadawcaNrLokalu = adresElements("NrLokalu")
            If adresElements.ContainsKey("KodPocztowy") Then NadawcaKodPocztowy = adresElements("KodPocztowy")
            If adresElements.ContainsKey("Miejscowosc") Then NadawcaMiejscowosc = adresElements("Miejscowosc")
        End If

        If parametry.ContainsKey("ZawartoscPaczkiDomyslnie") Then ZawartoscPaczki = parametry("ZawartoscPaczkiDomyslnie")

        If parametry.ContainsKey("Platnosc") Then
            Dim platnoscElements As Dictionary(Of String, String) = WyodrebnijParametrySkladowe(parametry("Platnosc"))

            If platnoscElements.ContainsKey("Platnik") Then PlatnoscPlatnik = platnoscElements("Platnik")
            If platnoscElements.ContainsKey("NumerKlienta") Then PlatnoscNumerKlienta = platnoscElements("NumerKlienta")
            If platnoscElements.ContainsKey("Typ") Then PlatnoscTyp = platnoscElements("Typ")
            If platnoscElements.ContainsKey("CentrumKosztow") Then PlatnoscCentrumKosztow = platnoscElements("CentrumKosztow")
        End If

        If parametry.ContainsKey("WebApi") Then
            Dim webApiElements As Dictionary(Of String, String) = WyodrebnijParametrySkladowe(parametry("WebApi"))

            If webApiElements.ContainsKey("TypŻądania") Then DHL_WebApiTypZadania = webApiElements("TypŻądania")
            If webApiElements.ContainsKey("TypUslugi") Then DHL_WebApiTypUslubi = webApiElements("TypUslugi")
            If webApiElements.ContainsKey("TypEtykiety") Then DHL_WebApiTypEtykiety = webApiElements("TypEtykiety")
        End If

        If parametry.ContainsKey("NadanieWGodzinach") Then
            Dim godzinyElements As Dictionary(Of String, String) = WyodrebnijParametrySkladowe(parametry("NadanieWGodzinach"))

            If godzinyElements.ContainsKey("Start") Then DHL_NadanieWGodzinachStart = godzinyElements("Start")
            If godzinyElements.ContainsKey("Koniec") Then DHL_NadanieWGodzinachKoniec = godzinyElements("Koniec")
        End If

        If parametry.ContainsKey("UslugiDodatkowe") Then
            Dim uslugiDodElements As Dictionary(Of String, String) = WyodrebnijParametrySkladowe(parametry("UslugiDodatkowe"))

            If uslugiDodElements.ContainsKey("WartoscUbezpieczeniaZl") Then Double.TryParse(uslugiDodElements("WartoscUbezpieczeniaZl"), WartoscUbezpieczenia)
            If uslugiDodElements.ContainsKey("FormaZwrotuPobrania") Then FormaZwrotuPobrania = uslugiDodElements("FormaZwrotuPobrania")
        End If

        If parametry.ContainsKey("SLogClient") Then
            Dim slogClientElements As Dictionary(Of String, String) = WyodrebnijParametrySkladowe(parametry("SLogClient"))

            If slogClientElements.ContainsKey("Wersja") Then WersjaProgramu = slogClientElements("Wersja")
            If slogClientElements.ContainsKey("Instalator") Then InstalatorProgramu = slogClientElements("Instalator")
        End If

        If parametry.ContainsKey("INPOST_KURIER_login") Then INPOST_KURIER_login = parametry("INPOST_KURIER_login")
        If parametry.ContainsKey("INPOST_KURIER_pass") Then INPOST_KURIER_pass = parametry("INPOST_KURIER_pass")
        If parametry.ContainsKey("INPOST_KURIER_url") Then INPOST_KURIER_url = parametry("INPOST_KURIER_url")

        If parametry.ContainsKey("PP_login") Then PP_login = parametry("PP_login")
        If parametry.ContainsKey("PP_pass") Then PP_pass = parametry("PP_pass")

        If parametry.ContainsKey("PP_Api_Url") Then PP_Api_Url = parametry("PP_Api_Url")


    End Sub

    Private Shared Function WyodrebnijParametrySkladowe(parametrGlowny As String) As Dictionary(Of String, String)
        Dim skladowe As New Dictionary(Of String, String)
        Dim parts As String() = parametrGlowny.Split("|")

        For Each part As String In parts
            Dim partKeyVal As String() = part.Trim().Split("=")

            If partKeyVal.Length = 2 Then skladowe.Add(partKeyVal(0), partKeyVal(1))
        Next

        Return skladowe
    End Function
End Class
