﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InpostLetters;
using Inpost;
using Novacode;
using System.Drawing;
using System.IO;
using Zen.Barcode;
using System.Diagnostics;

namespace test
{
    class Program
    {
        static void Main(string[] args)
        {
            //TestujKodyKreskowe();
            List<Paczka> paczki = new List<Paczka>();
            for (int i = 1; i < 33; i++)
            {
                Paczka p = new Paczka() { OdbAdres = "Ul. fajna " + i, OdbKodPocztowy = "11-12" + i, OdbMiasto = "Jakieś Miasto" + i, OdbOsoba = "Anna Maria" + i, OdbNazwa = "nazwa" + i, NrPaczki = "00001", NrRef = "222222222" + i };
                paczki.Add(p);
            }

            Letters.Generate(paczki);

        }

        public void TestujDLL_inpostLetters()
        {
            List<Paczka> paczki = new List<Paczka>();
            for (int i = 1; i < 33; i++)
            {
                Paczka p = new Paczka() { OdbAdres = "Ul. fajna " + i, OdbKodPocztowy = "11-12" + i, OdbMiasto = "Jakieś Miasto" + i, OdbOsoba = "Anna Maria" + i, NrPaczki = "00001", NrRef = "222222222" + i };
                paczki.Add(p);
            }

            Letters.Generate(paczki);
        }



        public static void TestujKodyKreskowe()
        {
            Guid guid = Guid.NewGuid();

            string filename = "barcode" + guid.ToString() + ".docx";

            var path2 = @"C:\barcodes\" + filename;


            var paraFormat = new Novacode.Formatting();
            paraFormat.FontFamily = new System.Drawing.FontFamily("Calibri");
            paraFormat.Size = 13D;

            var doc = DocX.Create(path2);

            doc.MarginTop = 0f;
            doc.MarginBottom = 0f;
            doc.MarginLeft = 0f;
            doc.MarginRight = 0f;

         
          
            Code128BarcodeDraw sto = BarcodeDrawFactory.Code128WithChecksum;
            
             System.Drawing.Size a = new Size(10,30);
             System.Drawing.Size r = new Size(10,30);

             var image = sto.Draw("test", sto.GetDefaultMetrics(60));
             image.Save("a.jpg");

            using (MemoryStream ms = new MemoryStream())
            {
                System.Drawing.Image myImg = System.Drawing.Image.FromFile(@"a.jpg");
                myImg.Save(ms, myImg.RawFormat);
                ms.Seek(0, SeekOrigin.Begin);
                Novacode.Image img = doc.AddImage(ms);

                Picture pic1 = img.CreatePicture(); 
                Paragraph par = doc.InsertParagraph("");
                par.Alignment = Alignment.center;
                par.AppendPicture(pic1);
            }
           
            
            
            
            //int currentElement = 0;
            //for (int i = 1; i < 6; i++)
            //{
            //    dynamic test = bdf1;
            //    if (i == 2) { test = bdf2; }
            //    if (i == 3) { test = bdf3; }
            //    if (i == 4) { test = bdf4; }
            //    if (i == 5) { test = bdf5; }
            //    if (i == 6) { test = bdf6; }

            //    var image = test.Draw("test1",test.GetDefaultMetrics(50));
            //    image.Save("a" + currentElement + ".jpg");

            //    using (MemoryStream ms = new MemoryStream())
            //    {
            //        System.Drawing.Image myImg = System.Drawing.Image.FromFile(@"a" + currentElement + ".jpg");
            //        myImg.Save(ms, myImg.RawFormat);
            //        ms.Seek(0, SeekOrigin.Begin);
            //        Novacode.Image img = doc.AddImage(ms);

            //        Picture pic1 = img.CreatePicture();
            //    }
            //    Paragraph par = doc.InsertParagraph("");
            //    par.Alignment = Alignment.center;
            //}

           

            doc.Save();


            Process.Start("WINWORD.EXE", path2);
        }
    }
}