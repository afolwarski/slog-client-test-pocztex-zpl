﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
//using System.Drawing.Drawing2D;
using System.Configuration;
//using System.Drawing.Imaging;
//using System.Drawing.Printing;
using System.Runtime.InteropServices;
using System.Xml.Linq;
using System.Net.Sockets;
using System.Diagnostics;
using System.Security;


namespace NowaEra.Slog.PocztaPolska
{
    public static class ElektronicznyNadawca
    {
        public static bool Create()
        {
            testConnection();
            return true;
        }

        private static SecureString ConvertToSecureString(string password)
        {
            if (password == null)
                throw new ArgumentNullException("password");

            var securePassword = new SecureString();

            foreach (char c in password)
                securePassword.AppendChar(c);

            securePassword.MakeReadOnly();
            return securePassword;
        }

        public static int SendReceiveTest2(Socket server, string pdfPath, string printerName)
        {

            byte[] msg = Encoding.UTF8.GetBytes(pdfPath + "|" + printerName);
            byte[] bytes = new byte[256];
            try
            {

                int byteCount = server.Send(msg, SocketFlags.None);
            }
            catch (SocketException e)
            {
                return (e.ErrorCode);
            }
            return 0;
        }

        private static CredentialCache SetCredentials(string login, string password, string url)
        {
            NetworkCredential nc = new NetworkCredential
            {
                UserName = login,
                Password = password
            };
            CredentialCache cc = new CredentialCache();
            cc.Add(new Uri(url), "Basic", nc);

            return cc;
        }

        public static bool RemoveShipmentFromBufor(IEnumerable<dynamic> paczki, dynamic config)
        {
            var paczka = paczki.FirstOrDefault();
            if (paczka == null)
                return false;
            try
            {
                enService.ElektronicznyNadawca tEN = new enService.ElektronicznyNadawca();
                tEN.Credentials = SetCredentials(config.login, config.pass, config.url);

                string[] guids = new string[1] { paczka.IDPrzesyłkiString };

                var result = tEN.clearEnvelopeByGuids(guids, 1, false);

                if (result != null)
                {
                    foreach (var r in result)
                    {
                        paczka.UstawOpisBledu("[Exception] " + r.errorDesc + "[errorNumber] " + r.errorNumber + "[Guid] " + r.guid, 2);
                    }
                    return false;
                }

            }
            catch (Exception e)
            {
                paczka.UstawOpisBledu("[Exception] " + e.Message, 7);
                return false;
            }

            foreach (var p in paczki)
            {

                p.OznaczJakoAnulowaneIDNiestandardowe();
            }

            return true;
        }

        public static bool Create(IEnumerable<dynamic> paczki, dynamic config)
        {
            try
            {
                var paczka = paczki.FirstOrDefault();
                if (paczka == null)
                    return false;

                //Socket MyTestSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                //IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Loopback, 43530);
                //MyTestSocket.Connect(ipEndPoint);


                enService.ElektronicznyNadawca tEN = new enService.ElektronicznyNadawca();

                tEN.Credentials = SetCredentials(config.login, config.pass, config.url);

                var er = new enService.errorType[] { };
                string main_guid = Guid.NewGuid().ToString("N").ToUpper();

                string TodayDate = DateTime.Now.ToShortDateString();

                var isExist = paczka.Check_PP_Log_exist();

                if (!isExist)
                {
                    tEN.clearEnvelope(1, false, out er);
                    paczka.Insert_PP_log(DateTime.Now.ToString(), main_guid, 0, 1, TodayDate);
                }

                List<enService.uslugaKurierskaType> przesyłki = new List<enService.uslugaKurierskaType>();
                enService.uslugaKurierskaType uslugaKurierska = new enService.uslugaKurierskaType();

                uslugaKurierska.termin = enService.terminKurierskaType.EKSPRES24;
                uslugaKurierska.terminSpecified = true;
                uslugaKurierska.masa = (int)(paczka.Waga * 1000);
                uslugaKurierska.masaSpecified = true;
                uslugaKurierska.guid = main_guid;
                uslugaKurierska.adres = new enService.adresType();

                string odbNazwa = paczka.OdbiorcaNazwa;
                string odbOs = paczka.OdbiorcaOsoba;

                uslugaKurierska.adres.nazwa = odbNazwa.Length >= 60 ? odbNazwa.Substring(0, 60) : odbNazwa;
                uslugaKurierska.adres.nazwa2 = odbOs.Length >= 60 ? odbOs.Substring(0, 60) : odbOs;

                uslugaKurierska.adres.ulica = paczka.OdbiorcaUlica;
                uslugaKurierska.numerPrzesylkiKlienta = paczka.NrRef;
                uslugaKurierska.opis = paczka.NrRef + "/" + paczka.NrPaczki;
                uslugaKurierska.adres.miejscowosc = paczka.OdbiorcaMiejscowosc;
                string kodPocztowy = paczka.OdbiorcaKodPocztowy.Replace("-", "");
                uslugaKurierska.adres.kodPocztowy = kodPocztowy;
                uslugaKurierska.adres.telefon = paczka.OdbiorcaTelefon;

                if (paczka.UslugiDodatkowe != null)
                {
                    foreach (string u in paczka.UslugiDodatkowe)
                    {
                        switch (u)
                        {
                            case "COD":
                                if (paczka.WartoscZwrotuPobrania > 0)
                                {
                                    enService.pobranieType pobranie = new enService.pobranieType();
                                    pobranie.kwotaPobrania = (int)(Math.Round(paczka.WartoscZwrotuPobrania * 100));
                                    pobranie.sposobPobrania = enService.sposobPobraniaType.RACHUNEK_BANKOWY;
                                    pobranie.kwotaPobraniaSpecified = true;
                                    pobranie.sposobPobraniaSpecified = true;
                                    pobranie.nrb = config.COD_nr_konta.Replace(" ", "");
                                    pobranie.tytulem = "FV";
                                    uslugaKurierska.pobranie = pobranie;
                                }
                                break;

                            case "UBEZP":
                                enService.ubezpieczenieType ubezp = new enService.ubezpieczenieType();
                                ubezp.rodzaj = enService.rodzajUbezpieczeniaType.STANDARD;
                                ubezp.kwota = 5000;
                                uslugaKurierska.ubezpieczenie = ubezp;

                                break;
                            case "DO1200":
                                var doreczenie = new enService.doreczenieUslugaKurierskaType();
                                doreczenie.oczekiwanaGodzinaDoreczenia = enService.oczekiwanaGodzinaDoreczeniaUslugiType.DO1200;
                                doreczenie.oczekiwanaGodzinaDoreczeniaSpecified = true;
                                uslugaKurierska.doreczenie = doreczenie;

                                break;
                            default:
                                break;
                        }
                    }
                }

                var itemss = new List<enService.subUslugaKurierskaType>();

                foreach (var p in paczki.Where(x => x.Id != paczka.Id))
                {
                    enService.subUslugaKurierskaType subs = new enService.subUslugaKurierskaType();

                    subs.guid = Guid.NewGuid().ToString("N").ToUpper();
                    subs.masa = (int)(p.Waga * 1000);
                    subs.numerPrzesylkiKlienta = paczka.NrRef;
                    subs.opis = p.NrRef + "/" + p.NrPaczki;
                    subs.masaSpecified = true;
                    subs.ubezpieczenie = uslugaKurierska.ubezpieczenie;

                    itemss.Add(subs);
                }

                uslugaKurierska.Items = itemss.ToArray();

                przesyłki.Add(uslugaKurierska);
                enService.addShipmentResponseItemType[] response = tEN.addShipment(przesyłki.ToArray(), 1, false);

                byte[] etykietaPDF = new byte[] { };

                var mainResponse = response.FirstOrDefault(x => x.guid == uslugaKurierska.guid);

                string[] guid = new string[1] { mainResponse.guid };

                var etykieta = tEN.getAddresLabelByGuid(guid, 1, false, out er);

                if (er.Count() > 0)
                {
                    foreach (var p in paczki)
                    {
                        p.UstawOpisBledu("[Spedytor] " + "[" + er[0].errorNumber + "] " + er[0].errorDesc, 2);
                    }
                    return false;
                }

                bool ifZpl = IfZpl(etykieta[0].pdfContent.Take(20).ToArray());
                string[] etykietyZPL = null;
                if (ifZpl)
                {
                    etykietyZPL = Encoding.Default.GetString(etykieta[0].pdfContent).Split(new string[] { @"^XA" }, StringSplitOptions.None);
                }

                int i = 0;
                foreach (var p in paczki)
                {
                    if (ifZpl)
                    {
                        p.EtykietaNiestandardowa = ScaleLabel(etykietyZPL[0] + @"^XA" + etykietyZPL[i + 1]).Replace(@"^FWR", "").Replace(@"^FWN", "");
                    }
                    else
                    {
                        string pdfPath = @"C:\pocztex\poczta_polska_" + mainResponse.guid + @".pdf";
                        etykietaPDF = etykieta[i].pdfContent;
                        System.IO.File.WriteAllBytes(pdfPath, etykietaPDF);
                        p.EtykietaNiestandardowa = pdfPath;
                    }

                    p.NrListu = response[i].numerNadania;
                    p.IDPrzesyłkiString = response[i].guid;
                    p.AktualizujDaneSpedycyjneEtykietaNiestandardowa();
                    p.OznaczJakoZrealizowane();
                    i++;
                }
            }
            catch (Exception e)
            {

                foreach (var p in paczki)
                {
                    p.UstawOpisBledu("[Exception] " + e.Message, 7);
                }
                return false;
            }

            return true;
        }

        private static void testConnection()
        {
            enService.ElektronicznyNadawca tEN = new enService.ElektronicznyNadawca();
            tEN.Credentials = SetCredentials(@"j.bylinowski@nowaera.pl", "Nowaera2017", @"https://en-testwebapi.poczta-polska.pl/websrv/en.wsdl");
            string test = tEN.hello("test");
        }


        private static bool IfZpl(byte[] input)
        {
            string output = System.Text.Encoding.Default.GetString(input);
            return output.Contains(@"^XA");
        }

        private static string ScaleLabel(string label)
        {
            var lsc = new LabelScaleController();
            return lsc.ScaleZpl(label);
        }
    }
}
