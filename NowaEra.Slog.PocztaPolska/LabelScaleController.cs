﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace NowaEra.Slog.PocztaPolska
{
    public class LabelScaleController
    {

        private IFormatProvider culture;
        private NumberStyles style;

        public LabelScaleController()
        {
            culture = CultureInfo.CreateSpecificCulture("en-GB");
            style = NumberStyles.Number | NumberStyles.AllowCurrencySymbol;
        }

        public string ScaleZpl(string rawCommands)
        {
            double scaleFactor = (double)203 / (double)300;

            var sections = rawCommands.Split('^');
            var cmds = new string[] { "FO", "A0", "A@", "LL", "LH", "GB", "FB", "BY", "B3", "FT", "CF" };

            for (int i = 0; i < cmds.Length; i++)
            {
                for (int j = 0; j < sections.Length; j++)
                {
                    if (sections[j].IndexOf(cmds[i]) == 0)
                    {
                        sections[j] = ScaleSection(cmds[i], sections[j], scaleFactor);
                    }
                }
            }

            return string.Join("\n^", sections);

        }

        private string ScaleSection(string cmd, string section, double scaleFactor)
        {
            section = section.Substring(cmd.Length, section.Length - cmd.Length);
            var parts = section.Split(',');
            for (int p = 0; p < parts.Length; p++)
            {
                if (IsNumber(parts[p]))
                {
                    parts[p] = Math.Round(scaleFactor * Convert.ToDouble(parts[p], culture)).ToString();
                }
            }
            return cmd + string.Join(",", parts);
        }

        private bool IsNumber(string value)
        {
            double output;
            bool x = Double.TryParse(value, style, culture, out output);
            return x;
        }
    }
}
