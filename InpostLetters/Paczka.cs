﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InpostLetters
{
    public class Paczka
    {
        public string OdbNazwa { get; set; }
        public string OdbOsoba { get; set; }
        public string OdbAdres { get; set; }
        public string OdbMiasto { get; set; }
        public string OdbKodPocztowy { get; set; }
        public string NrRef { get; set; }
        public string NrPaczki { get; set; }
    }
}
