﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Novacode;
using System.IO;
using Zen.Barcode;
using System.Diagnostics;
using System.Threading;

namespace Inpost
{
    public static class Letters
    {

        public static void cleanFolder()
        { 
            System.IO.DirectoryInfo di = new DirectoryInfo(@"C:\listy\");

            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete(); 
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true); 
            }
        }

        private static void CreateFolderIfNoExist()
        {
            if (!Directory.Exists(@"C:\listy\"))
                Directory.CreateDirectory(@"C:\listy\");
        }

        //private static void CloseWordProcesIfOpen()
        //{
        //    Process[] processes = Process.GetProcessesByName("WINWORD.EXE");
        //    if (processes.Length > 0) 
        //    {
        //        foreach (Process p in Process.GetProcessesByName("WINWORD.EXE"))
        //        {
        //            p.CloseMainWindow();
        //            p.Close();
        //            //p.Kill();
        //        }
        //    }
            
        //}

        public static bool KillProcess(string name)
        {
            //here we're going to get a list of all running processes on
            //the computer
            foreach (Process clsProcess in Process.GetProcesses())
            {
                if (Process.GetCurrentProcess().Id == clsProcess.Id)
                    continue;
                //now we're going to see if any of the running processes
                //match the currently running processes. Be sure to not
                //add the .exe to the name you provide, i.e: NOTEPAD,
                //not NOTEPAD.EXE or false is always returned even if
                //notepad is running.
                //Remember, if you have the process running more than once, 
                //say IE open 4 times the loop thr way it is now will close all 4,
                //if you want it to just close the first one it finds
                //then add a return; after the Kill
                if (clsProcess.ProcessName.Contains(name))
                {
                    clsProcess.Kill();
                    return true;
                }
            }
            //otherwise we return a false
            return false;
        }

        public static void Generate(IEnumerable<dynamic> paczki)
        {
            KillProcess("WINWORD");
            CreateFolderIfNoExist();

            Thread.Sleep(300);
            cleanFolder();
            

            Guid guid = Guid.NewGuid();

            string filename = "listy" + guid.ToString() + ".docx";           

            var path2 = @"C:\listy\" + filename; 

            var headLineFormat = new Novacode.Formatting();
            headLineFormat.FontFamily = new System.Drawing.FontFamily("Arial Black");
            headLineFormat.Size = 15D;
            headLineFormat.Position = 20;
            headLineFormat.FontColor = System.Drawing.Color.SlateBlue;
            headLineFormat.UnderlineStyle = UnderlineStyle.singleLine;


            var paraFormat = new Novacode.Formatting();
            paraFormat.FontFamily = new System.Drawing.FontFamily("Calibri");
            paraFormat.Size = 13D;

            var doc = DocX.Create(path2);

            doc.MarginTop = 0f;
            doc.MarginBottom = 0f;
            doc.MarginLeft = 0f;
            doc.MarginRight = 0f;

            float RowHeight = doc.PageHeight / 4;
            double columnWidth = (doc.PageWidth / 2);
            Table table = doc.AddTable(4, 2);
            table.Alignment = Alignment.center;
            table.Rows[0].Height = 275;
            table.Rows[1].Height = 275;
            table.Rows[2].Height = 275;
            table.Rows[3].Height = 275;

            foreach (Row r in table.Rows)
            {
                r.Cells[0].Width = 400;
                r.Cells[1].Width = 400;
            }           
                 
            Border tt = new Border(Novacode.BorderStyle.Tcbs_dotted, BorderSize.one, 1, Color.Black);
            Border ll = new Border(Novacode.BorderStyle.Tcbs_dotted, BorderSize.one, 1, Color.Black);
            Border rr = new Border(Novacode.BorderStyle.Tcbs_dotted, BorderSize.one, 1, Color.Black);
            Border bb = new Border(Novacode.BorderStyle.Tcbs_dotted, BorderSize.one, 1, Color.Black);
            Border border_none = new Border(Novacode.BorderStyle.Tcbs_none, BorderSize.one, 1, Color.White);

            foreach (Row r in table.Rows)
            {
                foreach (Cell c in r.Cells)
                {
                    c.SetBorder(TableCellBorderType.Top, border_none);
                    c.SetBorder(TableCellBorderType.Left, border_none);
                    c.SetBorder(TableCellBorderType.Right, border_none);
                    c.SetBorder(TableCellBorderType.Bottom, border_none);
                }
            }

            int row = 0;
            int col = 0;
            int modulo = 8;
            int currentElement = 0;

            foreach (var p in paczki)
            {
                Code128BarcodeDraw bdf = BarcodeDrawFactory.Code128WithChecksum;             
                var image = bdf.Draw(p.NrRef+"/"+p.NrPaczki, bdf.GetDefaultMetrics(40));

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    image.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Bmp);

                    memoryStream.Seek(0, SeekOrigin.Begin);

                    Novacode.Image img = doc.AddImage(memoryStream);

                    Picture pic1 = img.CreatePicture();

                    currentElement++;
                    Novacode.Image img2 = null;
                   
                    using (MemoryStream memoryStream2 = new MemoryStream())
                    {
                        //System.Drawing.Image myImg2 = System.Drawing.Image.FromFile(@"ne.jpg");
                        System.Drawing.Image myImg2 = System.Drawing.Image.FromFile(@"ne.jpg");
                        myImg2.Save(memoryStream2, myImg2.RawFormat);
                        memoryStream2.Seek(0, SeekOrigin.Begin);
                        img2 = doc.AddImage(memoryStream2);
                      
                    }

                    Picture pic2 = img2.CreatePicture();
                    pic2.Height = 63;
                    pic2.Width = 350;
                    

                   
                    Paragraph par0 = table.Rows[row].Cells[col].InsertParagraph("");
                    par0.Alignment = Alignment.left;
                    par0.AppendPicture(pic2).Alignment = Alignment.center;
                    table.Rows[row].Cells[col].InsertParagraph(String.Format("Nadano dnia: {0}        ",  DateTime.Now.ToShortDateString())).FontSize(7).Alignment = Alignment.right;
                    

                    table.Rows[row].Cells[col].InsertParagraph(p.OdbOsoba).Bold().FontSize(10).Alignment = Alignment.center;
                    table.Rows[row].Cells[col].InsertParagraph(p.OdbNazwa).Bold().FontSize(10).Alignment = Alignment.center;
                    table.Rows[row].Cells[col].InsertParagraph("").FontSize(6);
                    table.Rows[row].Cells[col].InsertParagraph(p.OdbAdres).Bold().FontSize(9).Alignment = Alignment.center;
                    table.Rows[row].Cells[col].InsertParagraph(p.OdbKodPocztowy + "  " + p.OdbMiasto).Bold().FontSize(9).Alignment = Alignment.center;
                                        
                 //   table.Rows[row].Cells[col].InsertParagraph(p.NrRef+"/"+p.NrPaczki).Bold().Alignment = Alignment.center;
                    table.Rows[row].Cells[col].InsertParagraph("");
                    Paragraph par = table.Rows[row].Cells[col].InsertParagraph("");
                    par.Alignment = Alignment.center;
                    par.AppendPicture(pic1).Alignment = Alignment.center;
                    table.Rows[row].Cells[col].InsertParagraph(p.NrRef + "/" + p.NrPaczki).Bold().FontSize(7).Alignment = Alignment.center;
                }


                if (currentElement % modulo == 0 && currentElement < paczki.Count())
                {
                    doc.InsertTable(table);  
                    table = doc.AddTable(4, 2);

                    foreach (Row r in table.Rows)
                    {
                        r.Cells[0].Width = 400;
                        r.Cells[1].Width = 400;
                    }

                    row = 0;
                    col = 0;

                    foreach (Row r in table.Rows)
                    {
                        foreach (Cell c in r.Cells)
                        {
                            c.SetBorder(TableCellBorderType.Top, border_none);
                            c.SetBorder(TableCellBorderType.Left, border_none);
                            c.SetBorder(TableCellBorderType.Right, border_none);
                            c.SetBorder(TableCellBorderType.Bottom, border_none);
                        }
                    }

                    table.Rows[0].Height = 275;
                    table.Rows[1].Height = 275;
                    table.Rows[2].Height = 275;
                    table.Rows[3].Height = 275;
                    table.Alignment = Alignment.center;
                }
                else
                {
                    if (col == 1)
                    {
                        if (row < 3)
                        {
                            row++;
                        }
                        col = 0;
                    }
                    else
                    {
                        col = 1;
                    }
                }
            }

            doc.InsertTable(table);
            doc.Save();
           
            Process.Start("WINWORD.EXE", path2);
        }
    }
}
